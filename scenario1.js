fs = require('fs');
file = './debug.log';
debugOuptut = '';
fs.closeSync(fs.openSync(file, 'w'));

// add debug header / footers using functions to make code a bit cleaner?
// standardize debug output


// enable more verbose output
scoringDebug = false;
scoringFBDebug = false;
scoringSpyDebug = false;
scoringSPDebug = false;
scoringSPGDebug = false;
//virDebug = false;
scoringURLDebug = false;
scoringCustomURLDebug = false;
scoringCustomDataDebug = false;
scoringDataDebug = false;
scoringVulnDebug = false;
scoringVulnExceptionDebug = false;
scoringSpywareSinkholenDebug = false;
scoringWFConfigDebug = false;
scoringMgmtPassComplexDebug = false;
scoringDeviceDynamicUpdateDebug = false;
scoringDeviceBannerDebug = false;
scoringCTDConfigDebug = false;
scoringCustomImgDebug = false;
scoringAdminRoleDebug = true;
mgmtUsersDebug = true;
IPNetmaskObjectDebug = false;
secPoliciesDebug = false;
dosPoliciesDebug = false;
schedObjDebug = false;
overrideAppDebug = false;
appFilterDebug = false;
debug_zones = false;
debug_physInt = false;
logForwardingDebug = false;
qosPoliciesDebug = false;
qosInterfaceDebug = false;
qosProfileDebug = false;
scoringCertsDebug = false;
secDecryptDebug = false;
netProfilesDebug = false;
debug_dosProfile = false;
customThreatDebug = false;

// Point varables
const [ pointsL1, pointsL2, pointsL3 ] = [ 5, 20, 50 ];
const [ hintPointsL1, hintPointsL2, hintPointsL3 ] = [-1, -5, -15];

// App-ID
let L1AppIDUnwantedObjectComplete = false;
let L2AppIDUnknownObjectComplete = false;
let L3AppIDSSLObjectComplete = false;
let L3AppIDSSHObjectComplete = false;
let L3AppIDFilterObjectName;
let L3AppIDAppFilterObjectComplete = false;

// File Blocking variables
let L1FBname; // keep track which profile name the correct rule is attached to
let L2FBmultiencodefbname; // keep track which profile name the correct rule is attached to
let L2FBprofileNameMatch; // L1 FB and L2 FB should be in the same FB profile name
let L3FBcorrectfbname; // keep track which profile name the continue all is attached to
let L3FBDriveByObjectComplete; // drive-by downloads, includes security policy config

// Spyware variables
let L1SpywarecorrectSpyname; // keep track of spyware profile name to be checked if it is used in the SPG
let L1SpywareCorrectResetClientName;
let L1SpywareprofileNameMatch = false;
let L2SpywareSinkholeName;
let L3SpywareExtendedPCAPComplete = false;

// Antivirus variables
let L1aAVCorrectavname;
let L1bAVCorrectavname;
let L1AVCorrectname;
let L1AVprofileNameMatch = false;

// URL variables
let L1aComplete = false;
let L1aBlockURLCorrectname;
let L1aAlertURLCorrectname;
let L1aCorrectname;
let L1aURLNameMatch = false;
let L1bComplete = false;
let L1bContinueURLCorrectname;
let L1bAlertURLCorrectname;
let L1bCorrectname;
let L1bURLNameMatch = false;
let L2URLCorrectname;
let L2URLContainerComplete = false;
let L3URLCorrectname;
let L3URLCustomURLComplete = false;
let L3URLCustomURLContinue;
let L3URLCustomURLBlock;

// Data Filtering variables
let L2DataAlertCorrectName;
let L2DataCCCorrectName;
let L2DataCorrectAlertProfileName;
let L2DataCorrectCCProfileName;
let L3DataPatternCorrectName;
let L3DataPatternCorrectProfileName;
let L2DataAlertObjectiveComplete = false;
let L2DataCCObjectiveComplete = false;
let L3DataPatternComplete = false;

// Security Policies variabls
let L2BlockSinkholeIPObjectName; // track address object the IP could be in
let L2BlockSinkholeIPObjectComplete = false;
let L2BlockBadIPsObjectComplete = false;
let L2SchedFacebookObjectName;
let L2SchedFacebookObjectComplete = false;
let L3PolicyQoSObjectComplete = false;

// Scoring variables - will likely need to make this a player array, or return value into a player array or some kind
let L1FBcomplete = false; // keep track if L1 complete
let L2FBcomplete = false; // keep track if L2 complete
let L3FBcomplete = false; // keep track if L3 complete

// Vulnerability variables
let L1VulCorrectResetClientName;
let L1VulCorrectDefaultClientName;
let L1VulnprofileClientNameMatch = false;
let L1VulCorrectClientName;
let L1VulCorrectResetServerName;
let L1VulCorrectDefaultServerName;
let L1VulCorrectServerName;
let L1VulnprofileServerNameMatch = false;
let L2VulCorrectExceptionName;
let L3VulnerabilityCustomObjectComplete = false;

// Security Profile Group variables
let L1SecurityProfileGroupName;
let L1SecurityProfileGroupComplete = false;
let L3SecurityProfileGroupComplete = false;

//Management Config
let L1minimumPasswordComplixityComplete = false;

// Device Config
let L1DeviceConfigWildFireConfigComplete = false;
let L1DeviceConfigDynamicUpdateComplete = false;
let L1DeviceConfigBannerComplete = false;
let L1DeviceConfigCTDComplete = false;
let L1DeviceConfigCustomLoginImgComplete = false;
let L1DeviceConfigCustomUIImgComplete = false;

// Shared Config
let L3SharedConfigAdminRoleComplete = false;
let L3SharedConfigCorrectRoleName;
let L3SharedConfigCorrectUserWithRole = false;

// Zones
let L1ZoneInternalObjectComplete = false;
let L1ZoneExternalObjectComplete = false;
let L1ZoneVPNObjectComplete = false;
let L1ZoneObjectComplete = false;
let L1physicalInterfaceObjectComplete = false;

// Interface
let L1InternalInterfaceObjectComplete = false;
let L1ExternalInterfaceObjectComplete = false; 
let L1InterfaceObjectComplete = false;
let L1VirtualRouterObjectComplete = false;

// User-ID
let L1UserIDZoneObjectComplete = false;
let L1UserIDInternalObjectComplete = false;
let L1UserIDVPNObjectComplete = false;
let L2UserIDIgnoreObjectComplete = false;

// Log Forwarding
let L1LogFwdObjectName;
let L1LogFwdObjectComplete = false;
let L2LogFwdObjectName;
let L2LogFwdObjectComplete = false;
let L3LogFwdObjectComplete = false;

// QoS
let L1QoSProfileObjectName;
let L1QoSInterfaceComplete = false;
let L1QoSPolicyComplete = false;
let L1QoSObjectComplete = false;

// Decrypt
let L1DecryptCertsObjectComplete = false;
let L1DecryptNoDecryptObjectComplete = false;
let L1DecryptPolicyObjectComplete = false;
let L1DecryptObjectComplete = false;
let L1DecryptSSHPolicyObjectComplete = false;
let L1noDecryptPos;
let L1decryptPos;
let L2DecryptForwardContentObjectComplete = false;

// Zone PP
let L1ZonePacketBuffObjectComplete = false;
let L2ZoneProfileObjectName;
let L2ZonePPObjectComplete = false;

// DoS 
let L2DosProfileObjectName;
let L2DoSPolicyObjectComplete = false;
let L2DosPolicyRuleComplete = false;
let L2DosObjectName = false;

//******************************* Track Complete Per Player *************************************
// this section has objects that keep track for which players successfully completed the tasks

// File Blocking
let L1FileBlockingComplete = [];
let L2FileBlockingComplete = [];
let L3FileBlockingComplete = [];
let L3FBDriveByComplete = [];

// Spyware
let L1SpywareComplete = [];
let L2SpywareComplete = [];
let L3SpywareComplete = [];

// Antivirus
let L1AntivirusComplete = [];

// Vulnerability
let L1VulnerabilityClientComplete = [];
let L1VulnerabilityServerComplete = [];
let L2VulnerabilityExceptionComplete = [];
let L3VulnerabilityCustomComplete = [];

// URL Filtering
let L1aURLComplete = [];
let L1bURLComplete = [];
let L2URLComplete = [];
let L3URLComplete = [];

// Data Filtering
let L2DataAlertComplete = [];
let L2DataCCComplete = [];
let L3DataDBPatternComplete = [];

// Security Profile Groups
let L1SPGComplete = [];
let L3SPGComplete = [];

// Device Config
let L1DeviceConfigWFConfigComplete = [];
let L1DeviceConfigDynUpdateComplete = [];
let L1DeviceConfigLoginBannerComplete = [];
let L1DeviceConfigCTDQBypassComplete = [];
let L1DeviceConfigCustomLoginLogoComplete = [];
let L1DeviceConfigCustomUILogoComplete = [];

// Management Config
let L1minPassComplixityComplete = [];

// Shared Config
let L3SharedAdminRoleComplete = [];

// Policies
let L2BlockSinkholeIPComplete = [];
let L2BlockBadIPsComplete = [];
let L2FacebookScheduleComplete = [];
let L3PolicyQoSComplete = [];

// App-ID
let L1AppIDUnwantedComplete = [];
let L2AppIDUnknownComplete = [];
let L3AppIDAppFilterComplete = [];

// Zones
let L1ZoneComplete = [];

// Interfaces
let L1InterfaceComplete = [];

// User-ID
let L1UserIDZoneComplete = [];
let L2UserIDIgnoreComplete = [];

// Log Forwarding
let L1LogFwdComplete = [];
let L2LogFwdComplete = [];
let L3LogFwdComplete = [];

// QoS
let L1QoSComplete = [];

// Decrypt
let L1DecryptComplete = [];
let L1DecryptCertComplete = [];
let L1DecryptSSHComplete = [];
let L2DecryptForwardContentComplete = [];

// Zone Protection Profile
let L1ZonePacketBuffComplete = [];
let L2ZonePPComplete = [];

// DoS
let L2DoSPolicyComplete = [];


//************************************** Scenarios **********************************************

//**************************************** Zones ************************************************
// L1 - (COMPLETE) create internal, external and VPN zones
// L1 - (COMPLETE) create interfaces

//************************************** Antivirus **********************************************
// L1 - (COMPLETE) reset-both - all items for AV & WF

//************************************ File Blocking ********************************************
// L1 - (COMPLETE) L1FB - FB - alert all + added to SPG
// L2 - (COMPLETE) L2FB - block multi-level encoding, add to FB policy that contains L1
// L3 - (COMPLETE) L3FB - stop drive by downloads. continue on files for only unknown category -- need to correlate with security rule

//*************************************** Spyware ***********************************************
// L1 - (COMPLETE) reset client - low to critical, default on inf
// L2 - (COMPLETE) enable sinkholing
// L3 - (COMPLETE) Extended Packet Capture Length (packets)? Device > Setup > Content-ID

//************************************ Vulnerability ********************************************
// L1- (COMPLETE) reset client - block med-critical and default on low/info (currently using default profile). Update security group 
// L1- (COMPLETE) reset server - (both block med-crit, default on low/info)
// L2- (COMPLETE) whitelist evasion sigs only one the reset-client profile (54319 & )
// L3- (COMPLETE) custom vuln for http 500 response code codes

//***************************************** URL *************************************************
// L1a- (COMPLETE) block malicous. Trick is that you should be able to create each profile 
// L1b- (COMPLETE) continue on malicious for helpdesk (potentially combine with )
// L2- (COMPLETE) don’t log just container pages (reword to make more obscure)
// L3- (COMPLETE) custom category or multiple categories / risk

//************************************** Data Filtering ******************************************
// L2- (COMPLETE) alert on confidential
// L2- (COMPLETE) credit card pattern alert only on certain threshold (alert needs to be completed first)
// L3- (COMPLETE) DB pattern like at black hat

//******************************* Security Profile Groups ****************************************
// L1- (COMPLETE) create a group for client-reset (IPS, C2, AV, etc)
// L3- (COMPLETE) Security groups - default profile 

//************************************* Device Config ********************************************
// L1- (COMPLETE) change WF file sizes to max
// L1- (COMPLETE) password complexity 
// L1- (COMPLETE) dynamic update times
// L1- (COMPLETE) update company logo
// L1- (COMPLETE) banner 
// L2- (COMPLETE) CTD queue exhaustion ****** need to validate default config ********
// L3- (COMPLETE) create a api only account, no commit

//*************************************** Policies ***********************************************
// L2- (COMPLETE) Block risky IPs
// L2- (COMPLETE) block sinkholing address
// L2- (COMPLETE) scheduled policy - facebook
// L3- (COMPLETE) mark QoS traffic

//*************************************** App-ID ***********************************************
// L1- (COMPLETE) create rule to block bittorrent, tor
// L1- create permit any any out on all ports. This should be at the bottom.
// L2- (COMPLETE) unknown-tcp and metasploit, block
// L3- (COMPLETE) app filters - block encrypted tunnel risk 3-5 without affecting ssl

//************************************** User-ID ***********************************************
//L1- enable group mapping for help desk to get a different response page
//L1- (COMPLETE) enable user-ID for VPN and internal zones
//L2- (COMPLETE) ignore svc account user name

//*********************************** Log Forwarding *******************************************
//L1- (COMPLETE) create a profile with Panorama check-box, log traffic, threat, url, WF
//L2- (COMPLETE) log certain severities
//L3- (COMPLETE) create a log forwarding profile called default 

//***************************************** QoS ************************************************
//L2- (COMPLETE) new iPhone update came out and your pipes are getting crushed. Create a policy to limit to x Kbps

//*************************************** Decrypt **********************************************
//L1- (COMPLETE) configure certs
//L1- (COMPLETE) create a policy to decrypt everything except financial, government, healthcare 
//L1- (COMPLETE) decrypt ssh
//L2- (COMPLETE) forward decrypted content

//*************************************** Zone PP **********************************************
//L1- (COMPLETE) enable packet buffer protection under internal zone
//L2- (COMPLETE) best practices: tcp with data, port scans, recon, flood parameters 

//***************************************** DoS ************************************************
//L2- (COMPLETE)  for web server / classified 

// re-init variables
exports.init = function() { // reset values so they get checked again on next commit.
    L1FBname = void 0; 
    L2FBmultiencodefbname = void 0;
    L1SpywarecorrectSpyname = void 0; 
    L1SpywareCorrectResetClientName = void 0;
    L2SpywareSinkholeName = void 0;
    L1aAVCorrectavname = void 0;
    L1bAVCorrectavname = void 0;
    L1AVCorrectname = void 0;
    L1VulCorrectResetClientName = void 0;
    L1VulCorrectDefaultClientName = void 0;
    L1VulCorrectClientName = void 0;
    L1VulCorrectResetServerName = void 0;
    L1VulCorrectDefaultServerName = void 0;
    L1VulCorrectServerName = void 0;
    L2VulCorrectExceptionName = void 0;
    L1aBlockURLCorrectname = void 0;
    L1aAlertURLCorrectname = void 0;
    L1bCoontinueURLCorrectname = void 0;
    L1bAlertURLCorrectname = void 0;
    L1aCorrectname = void 0;
    L1bCorrectname = void 0;
    L1aComplete = false;
    L1bComplete = false;
    L2URLCorrectname = void 0;
    L3URLCorrectname = void 0;
    L3URLCustomURLContinue = void 0;
    L3URLCustomURLBlock = void 0;
    L1SecurityProfileGroupName = void 0;
    L3SharedConfigCorrectRoleName = void 0;
    L2DataAlertCorrectName = void 0;
    L2DataCCCorrectName = void 0;
    L2DataCorrectAlertProfileName = void 0;
    L2DataCorrectCCProfileName = void 0;
    L3DataPatternCorrectName = void 0;
    L3DataPatternCorrectProfileName = void 0;
    L2BlockSinkholeIPObjectName = void 0;
    L2SchedFacebookObjectName = void 0;
    L3AppIDFilterObjectName = void 0;
    L1ZoneVPNObjectComplete = false;
    L1ZoneInternalObjectComplete = false;
    L1ZoneExternalObjectComplete = false;
    L1ZoneObjectComplete = false;
    L1UserIDZoneObjectComplete = false;
    L1UserIDInternalObjectComplete = false;
    L1UserIDVPNObjectComplete = false;
    L1InterfaceObjectComplete = false;
    L1InternalInterfaceObjectComplete = false;
    L1ExternalInterfaceObjectComplete = false;
    L1VirtualRouterObjectComplete = false;
    L1physicalInterfaceObjectComplete = false;
    L1LogFwdObjectName = void 0;
    L1QoSProfileObjectName = void 0;
    L1QoSPolicyComplete = false;
    L1QoSInterfaceComplete = false;
    L1noDecryptPos = void 0;
    L1decryptPos = void 0;
    L2ZoneProfileObjectName = void 0;
    L2DosProfileObjectName = void 0;
    L2DosPolicyRuleComplete = false;
    L2DosObjectName = void 0;
}

// Tasks for player's dashboards
exports.tasks = function() {

    let tasks = [];

    // [category of taskID, task, task description, points, task hint]
    tasks.push(['q1', 'Zones', 'You\'re turning up a new firewall. Why not start with configuring some zones? Create the following L3 zones (case sensitive): external, internal and vpn.', pointsL1]);
    tasks.push(['q2', 'Zones', 'Configure two Layer-3 interfaces, with the Internet facing interface on E1/1 and the LAN interface on E1/2. Configure DHCP on the internet facing interface and 192.168.0.254/24 on the LAN interface. Use the default Virtual Router and add the external and internal zones to their respective interfaces.', pointsL1]);
    tasks.push(['q3', 'Device Config', 'You ran a Best Practices Assessment with your local VAR or favourite Palo Alto SE. You noticed that by default, the firewall will forward packets without inspection if the TCP, UDP and App-ID packet queues are full and will also allow partial HTTP responses, which is not considered best practices. Configure the firewall to drop packets when these queues become full and to drop HTTP partial responses.', pointsL1]);
    tasks.push(['q4', 'Device Config', 'Change the Main UI image in the firewall with your chosen avatar image representing your corp. Click on the Game Images link in the top menu bar to download the image you would like to use.', pointsL1]);
    tasks.push(['q5', 'Device Config', 'Change the Login screen logo with your chosen avatar image representing your corp. Click on the Game Images link in the top menu bar to download the image you would like to use.', pointsL1]);
    tasks.push(['q6', 'Device Config', 'Add a login banner message before you log in, warning unauthorized users that they should disconnect now and only authorized personnel are permitted. Your legal team will be reviewing your work.', pointsL1]);
    tasks.push(['q7', 'Device Config', 'Configure Dynamic Updates with the following schedules: AV - DL & install every hour, App/Threat - DL & install every 30 min, WildFire - DL & install every minute. This should really only be worth half a point.', pointsL1]);
    tasks.push(['q8', 'Device Config', 'Going through the Best Practices assessment, it indicates that you didn\'t configure a password complexity policy. Go do that. Make sure the password length is at least 8 characters long and contains at least one upper and one lower. Also block user name inclusion.', pointsL1]);
    tasks.push(['q9', 'Device Config', 'You have been working on your coding skillz and want to start playing with the firewall API. Let\'s create an API Role that ONLY has access to the API, and only for the following capabilities: Report, Log, perform Operational Requests and User-ID Agent updates and NOTHING else. Create an administrator account that has this role attached to it.', pointsL3]);
    tasks.push(['q10', 'Security Policies', 'Configure a Security Policy to block all outbound communication to the IP for the Palo Alto Networks Sinkhole FQDN across all ports, all apps, from any source zone to external.', pointsL2]);
    tasks.push(['q11', 'Security Policies', 'Configure a Security Policy to block all outbound communication to the 3 pre-defined Extended Dynamic Lists from Palo Alto Networks, across all ports from any source zone and IP to the external zone.', pointsL2]);
    tasks.push(['q12', 'Security Policies', 'Maaaaaaaannnn.. one day you look at ACC and Facebook is the #1 app used in the company! Of course you\'re too busy for social media, so you decide to create a security policy to only allow facebook between 11:30 and 1pm for everyone. LOL! Set source zone to internal and destination to external. Use the "facebook" App-ID which includes all sub-functions and use the app-default service.', pointsL2]);
    tasks.push(['q13', 'DoS', 'Your web server is being attacked AGAIN. Set up a DoS rule from external to internal, type Zone, source IP of any, dest IP of 56.2.217.65 on service "service-https" with the action of protect and Classified as the option, using SYN Cookies instead of RED. You did some analytics and the following TCP SYN flood rates should be acceptable: 1000/sec Alarm rate, 1500/sec Activate rate, 3000/sec Max rate and a 300 sec Block Duration. We only want to block the offending source IP, nothing else.', pointsL2]);
    tasks.push(['q14', 'App-ID', 'Why are we allowing bittorrent and tor traffic on our network?? As if people are using torrents to download only Linux ISOs. Block both of them now on all ports from any source IP on the internal zone to any destination IP out the external zone.', pointsL1]);
    tasks.push(['q15', 'App-ID', 'Unknown apps showing up in the logs? Da heck? After some research and testing, you find out that meterpreter (reverse shell) shows up as unknown tcp traffic in the logs. You did your due diligence and found that none of these unknown apps look to be business related. Time to block these risky apps! Block all unknown tcp, udp and peer to peer traffic from all internal IPs to any external IPs. Make sure they are blocked across all ports.', pointsL2]);
    tasks.push(['q16', 'App-ID', 'You blocked facebook and other unwanted apps, but now users are tunneling their data out through encrypted channels to get around your heavy hand. Block all encrypted tunnel apps with risks 4 and 5 from all internal IPs to any external IPs across all ports.', pointsL3]);
    tasks.push(['q17', 'Log Forwarding', 'Create a log forwarding profile to send traffic, threat, url, data and wildfire logs to Panorama. *Yawn*.', pointsL1]);
    tasks.push(['q18', 'Log Forwarding', 'Looks like the Governance/Compliance team is bored today. They want you to send all medium - critical threat logs to their syslog server. Sure. Feel free to use any IP you want for the syslog server - what do you care? Add this to the existing threat rule in the log forwarding profile you completed in the previous task (pre-req).', pointsL2]);
    tasks.push(['q19', 'Log Forwarding', 'GRC team is back at you again. Looks like you\'ve been forgetting to add the log forwarding profile to all security rules. Easy oversight - I understand. Configure the profile so that it will automatically be applied to all new rules moving forward. Cool, huh! You need to complete the prior Log Forwarding goals to complete this task. If you don\'t know the answer to this question, don\'t take the hint.... you\'ve been warned.', pointsL3]);
    tasks.push(['q20', 'User-ID', 'Enable User-ID on the internal and vpn zones. So easy, my mom can do it. By now your zones should have been configured (required).', pointsL1]);
    tasks.push(['q21', 'User-ID', 'You\'re getting complaints from GRC again that a service account is interfering with correct User-ID mappings. Looks like this this service account is performing a "logon" operation and is overwriting usernames of actual people logged into the computer. Ignore the following service account from future User-ID mappings: acme.local\\svc_account', pointsL2]);
    tasks.push(['q22', 'File Blocking', 'Your boss asked if you can run a file report on the firewall since there is suspicion that the latest sales person uploaded all their client lists to their personal cloud account. It\'s and NGFW and can do everything right? Well, you forgot to alert on all file types travsering the firewall. Uh-oh... you sure you really know what you\'re doing? Create a file blocking profile to alert on all file types in both directions.', pointsL1]);
    tasks.push(['q23', 'File Blocking', 'You attended the latest Fuel user group and learned that the firewall can look into up to 4 levels of encoding (ie. files downloaded over compressed HTTP within a zip, within a zip, within a zip.) However remote, you don\'t want files coming through that can bypass inspection. The previous file blocking task is required to be completed first. To the previously created file blocking profile, add another rule to block file types that have more than 4 levels of encoding.', pointsL2]);
    tasks.push(['q24', 'File Blocking', 'Your Palo Alto SE showed you a cool way to block drive-by downloads for only the unknown URL category which is higher risk. Wish you had known about this earlier to help stop scripting attacks. You will need to set a "continue" action for all file types going through the web-browsing application on its app-default port, from all internal IPs to all external IPs but only affect the unknown URL category.', pointsL3]);
    tasks.push(['q25', 'WildFire', 'You can adjust the wildfire file sizes that can be sent up to the cloud? (of course you knew that). Adjust the file sizes to support the maximums for all file types.', pointsL1]);
    tasks.push(['q26', 'Antivirus', 'Some people like to create an "alert-all" profile on an initial turn-up and then try to harden later. Not you. You want block all viruses right away. Create an AV profile and set action to reset-both to all decoders for AV and WF.', pointsL1]);
    tasks.push(['q27', 'Spyware', 'Create a spyware profile and Block Low-Crit and set action to Default for Informational severities. Just use 2 rules to accomplish this.', pointsL1]);
    tasks.push(['q28', 'Spyware', 'Enable sinkholing for the Palo Alto DNS security subscription in the spyware profile! Will need to change all your DNS resolvers in your whole company to point to 208.67.222.222 and 208.67.220.220. Ugh... That\'s going to be hard to do for all the IoT and static IP devices out there. Just kidding, you don\'t have to change any DNS resolvers with the PA DNS security license. LOLz. This should take you 15 seconds to roll out!', pointsL2]);
    tasks.push(['q29', 'Spyware', 'GRC team again!!! *wining voice* "we want extended pcaps for all spyware activity! Can you do that for us?". Yes, yes you can. Change the default extended packet capture from 5 packets to 10 packets. That should be good enough.', pointsL3]);
    tasks.push(['q30', 'QoS', 'Apple just released a new IOS. You know what that means - saturated WAN links. Set up a QoS policy to send all "apple-update" traffic to a class 8 queue, set the maximum to 5Mbps for that queue and priority to low. Don\'t forget to enable it on the *internal* interface. Remember, we queue and police traffic only in the egress direction, right? This should affect all IPs internally communicating to any external IPs. The app-default ports is what you want.', pointsL2]);
    tasks.push(['q31', 'QoS', 'You just rolled out a VOIP solution, but because of other non-work related bandwidth being consumed (*sigh*), it is (of course) affecting VOIP traffic and now it\'s affecting your life. Your upstream carrier is going to be handling the QoS, but you need to mark the packets properly. Mark "rtp" and "sip" applications with IP DSCP - ef on their default ports from any internal IPs to any external IPs.', pointsL3]);
    tasks.push(['q32', 'Decryption', 'The moment you have been waiting for. You finally get to configure decryption! Generate the certs you need for Forward Proxy capabilities. Make sure you have the trust and untrust cert(s)!', pointsL1]);
    tasks.push(['q33', 'Decryption', 'SSH tunneling? Not in my house! Decrypt all SSH traffic from internal to external across all ports.', pointsL1]);
    tasks.push(['q34', 'Decryption', 'Next, let\'s decrypt traffic from all internal IPs to all external IPs across all ports for all URL categories, EXCEPT for URL categories: financial-services, government, health-and-medicine. You need to have your certs generated properly in the previous step before this will work! This will need to be accomplished using two different decryption policies.', pointsL2]);
    tasks.push(['q35', 'Decryption', 'You learn something new every day. By default, the NGFW doesn\'t forward decrypted content to Wildfire. Go enable the forwarding of decrypted files to Wildfire to catch any 0-day malware attempting to come through.', pointsL2]);
    tasks.push(['q36', 'Zone Protection', 'It only has to happen once! Some rogue or misconfigured device creates so many connections per second it affects the firewall. Enable Packet Buffer protection on the internal and external zones.', pointsL1]);
    tasks.push(['q37', 'Zone Protection', 'You\'re starting to get the hang of this! Enable a zone protection profile for TCP, Host and UDP sweeps with a 2, 10 and 2 second interval respectively, block action and a threshold of 100 events. We also need to block: Strict and loose source routing, Unknown and malformed IP options. Don\'t forget Mismatched overlapping TCP segments, split handshakes, TCP SYN with data, TCP SYNACK with data and TCP timestamps. Place this profile on the external zone.', pointsL2]);
    tasks.push(['q38', 'Vulnerability', 'Create a vulnerability profile specifically for your clients. Set action to reset-client for Crit-Med and default action on Low-Inf severities. Why reset-client? When a client visits a malicious resource, you want to silent drop to the attacker as to not give them any indication it was blocked but cleanly kill the connection to your user.', pointsL1]);
    tasks.push(['q39', 'Vulnerability', 'Create another vulnerability profile for your servers. You don\'t want to be sending TCP resets back to the attacker to let them know you are blocking their attacks, do you? Configure this profile with an action of reset-server on Crit-Med and a default action on Low-Inf severities.', pointsL1]);
    tasks.push(['q40', 'Vulnerability', 'Some informational Vuln sigs are very noisy, especially the "Suspicious HTTP Response Found" one, which really only works if you are using DNS proxy, which in this case we\'re not. Create an exception for this vuln sig to reduce the noise on the "client" vuln profile created earlier (pre-req).', pointsL2]);
    tasks.push(['q41', 'Vulnerability', 'Your website has been a bit flaky lately - it\'s the network\'s fault! (j/k). Your web admin asked if you can help detect if the web server starts sending HTTP 500 response codes back to people which indicates there is a problem on the server. Think you can handle this? ... You\'ll get there one day though. Maybe you can phone a friend to help you configure a custom vuln sig to alert on all http-500 error response codes.', pointsL3]);
    tasks.push(['q42', 'URL', 'Create a URL filtering profile to block the following categories and alert on the rest: command-and-control, copyright-infringement, dynamic-dns, extremism, hacking, malware, parked, phishing, proxy-avoidance-and-anonymizers, unknown. You can do this without having to click on each drop-down menu to set. Free Hint on how!', pointsL1]);
    tasks.push(['q43', 'URL', 'Create another profile for your help desk and other IT groups using the same URL categories from the previous URL task, but use continue as an action instead of block. Should only take you 20 seconds to do, or you\'re doing it wrong! Free Hint on how.', pointsL1]);
    tasks.push(['q44', 'URL', 'Log only the container page? That means the firewall isn’t logging related web links during the session, such as advertisements and content links. Pft. You want full URL visibility and record everything. Disable the log container only option. You only need to make this change on the URL filtering block profile in the first URL challenge (pre-req).', pointsL2]);
    tasks.push(['q45', 'URL', 'You\'ve received some intel that there is some malware being propagated through CDNs. Create a custom URL category match selecting content-delivery-networks and high-risk. In the URL Block  profile (pre-req), set the action to block for this new custom category. Set the action to Continue in the Continue profile (pre-req).', pointsL3]);
    tasks.push(['q46', 'Data Filtering', 'Data-filtering? What the heck is that. Configure a regex "Confidential" on all file-types in the upload direction, with an alert threshold of 1 and a block of 5. Set severity to medium, let\'s not get too carried away.', pointsL2]);
    tasks.push(['q47', 'Data Filtering', 'Configure a data-filtering object to detect credit card numbers. Add to the "Confidential" profile created previously (pre-req) and set to alert on 1 and block on 5 Credit Card numbers over all file-types in both directions. This is pretty serious, make sure the severity is set to high.', pointsL2]);
    tasks.push(['q48', 'Data Filtering', 'Your SE gave you the idea of implanting canary strings into meta-data within fake sensitive documents or in fake tables within legitimate databases so that when these strings are detected by the firewall, you know someone is accessing data that they shouldn\'t.  What a great idea! Look for the following regex: SSBsb3ZlIFBhbG8gQWx0byBOZXR3b3JrcyE= and set alert action to 1 and block block to 1. You should never see this come across the firewall unless data is being exfiltrated (that\'s bad). Set this severity to Critical. This object should be added to the previously created data filtering profile as a new rule. Both Confidential and Credit Card number data filtering tasks need to be completed first (pre-req).', pointsL3]);
    tasks.push(['q49', 'Security Profile Groups', 'Let\'s make your life easier. Create a security profile group with all the correct security profiles attached to it. Use the "client" vulnerability profile, the "block" URL filtering profile, the "alert all" file blocking profile and the other correct profiles for AV, Spyware, Data Filtering and the default Wildfire profile.', pointsL1]);
    tasks.push(['q50', 'Security Profile Groups', 'Just like the log forwarding, you\'ve been forgetting to add the security profile group to all rules! One foot out the door, this is a career limiting mistake! Name the security profile group so that will automatically be applied to all security rules moving forward. You need to complete the previous Security Profile Group task (pre-req) before you can complete this.', pointsL3]);

    return tasks;

}

exports.hints = function() {

    let hints = [];

    // [category of taskID, task, task description, points, task hint]
    hints.push(['q1', 'Zones', '']);
    hints.push(['q2', 'Zones', '']);
    hints.push(['q3', 'Device Config', '']);
    hints.push(['q4', 'Device Config', '']);
    hints.push(['q5', 'Device Config', '']);
    hints.push(['q6', 'Device Config', 'Login banner config is located under Device > Setup > Management > General Settings widget', hintPointsL1]);
    hints.push(['q7', 'Device Config', '']);
    hints.push(['q8', 'Device Config', 'Did you actually enable it? Make sure the Password Complexity is enabled under Device > Settings > Management > Minimum Password Complexity widget.', hintPointsL1]);
    hints.push(['q9', 'Device Config', 'Look under Device > Admin roles. Make sure all other access has been revoked for GUI access and SSH access as well.', hintPointsL3]);
    hints.push(['q10', 'Security Policies', 'Find out the IP of the FQDN sinkhole.paloaltonetworks.com by performing an nslookup on it. Make sure outbound communications are blocked to this IP.', hintPointsL2]);
    hints.push(['q11', 'Security Policies', 'You can find these predefined lists under Objects > Extended Dynamic Lists. If they are not present, make sure the AV dynamic update is installed.', hintPointsL2]);
    hints.push(['q12', 'Security Policies', 'Don\'t forget the schedule on the rule!', hintPointsL2]);
    hints.push(['q13', 'DoS', 'Check your DoS profile under Objects and make sure SYN Cookies are selected. In your classified settings within the DoS Policy, the Address field should be set to source-ip-only.', hintPointsL2]);
    hints.push(['q14', 'App-ID', 'Only block "tor" and "bittorrent" App-IDs. There should only be 2 App-IDs in the policy.', hintPointsL1]);
    hints.push(['q15', 'App-ID', 'Pay attention to the Service attached to this rule.', hintPointsL2]);
    hints.push(['q16', 'App-ID', 'You\'ll need to create an app filter for this one.', hintPointsL3]);
    hints.push(['q17', 'Log Forwarding', 'When creating your Log Forwarding Profile, you need to add multiple log types. There should be one for each of the required log types. Make sure the Panorama checkbox is checked for each one!', hintPointsL1]);
    hints.push(['q18', 'Log Forwarding', 'There are a couple ways to create the filter correctly. If you are using a compounding argument of severity eq high, severity eq medium, etc, you need to be using "or" operators instead of "and". You can\'t have a log that is medium and high and critical severity all at the same time!', hintPointsL2]);
    hints.push(['q19', 'Log Forwarding', 'The name that you give the profile is the key to this answer. Good luck. Can\'t tell you more or it would give you the answer.', hintPointsL3]);
    hints.push(['q20', 'User-ID', '']);
    hints.push(['q21', 'User-ID', 'This exception can be created under the Device > User-Identification config area.', hintPointsL2]);
    hints.push(['q22', 'File Blocking', '']);
    hints.push(['q23', 'File Blocking', 'There\'s a special file type you need to use to accomplis this.', hintPointsL2]);
    hints.push(['q24', 'File Blocking', 'You need to combine the URL profile in the security policy in conjunction with the file bocking profile.', hintPointsL3]);
    hints.push(['q25', 'WildFire', 'Don\'t know what the maximum file sizes are? Well, first of all, they are under the Device > Setup > Wildfire tab. Second, delete all the existing values and in doing so, it will reveal the accepted range of file sizes.', hintPointsL1]);
    hints.push(['q26', 'Antivirus', '']);
    hints.push(['q27', 'Spyware', '']);
    hints.push(['q28', 'Spyware', 'Your solution is simple. Just enable it in the spyware profile!', hintPointsL2]);
    hints.push(['q29', 'Spyware', 'There is an option in the profiles to change how the data is packet captured. Check it out!', hintPointsL3]);
    hints.push(['q30', 'QoS', 'Create the QoS policy, the QoS profile and make sure to also attach it to the physical interface.', hintPointsL2]);
    hints.push(['q31', 'QoS', 'You need to make this change under the security policy!', hintPointsL3]);
    hints.push(['q32', 'Decryption', '']);
    hints.push(['q33', 'Decryption', '']);
    hints.push(['q34', 'Decryption', 'Create a decryption policy with the "do not decrypt" URL categories" from all internal IPs to all external IPs across all ports. Create a second policy below that using "Any" for URL categories with a forward-proxy action. Make sure the Service is set to Any.', hintPointsL2]);
    hints.push(['q35', 'Decryption', 'Can\'t make this too easy. But it is a single check box somewhere under the Device tab :)', hintPointsL2]);
    hints.push(['q36', 'Zone Protection', '']);
    hints.push(['q37', 'Zone Protection', 'When creating your ZPP profile, the settings can be found under: Reconnaissance Protection, Packet Based Attack Protection with IP Drop and TCP Drop sub-tabs. After the profile is created, open the external zone and select this profile from the drop-down menu.', hintPointsL2]);
    hints.push(['q38', 'Vulnerability', '']);
    hints.push(['q39', 'Vulnerability', '']);
    hints.push(['q40', 'Vulnerability', 'Check off the box to show all signatures and search for http evasion. When you check off the box for this signature, it creates an exception for it. Found more than one signature in the list? There is only one with a severity of informational. That\'s the one you want.', hintPointsL2]);
    hints.push(['q41', 'Vulnerability', 'Create a standard custom vuln signature. What we\'re looking for is an http-rsp-code value of 500. "rsp" indicates the HTTP response coming from the server.', hintPointsL3]);
    hints.push(['q42', 'URL', 'Hover your mouse over the Site Access column in the URL filtering object and a little arrow appears. Click on that and choose Set All Actions to alert. Then change the few categories manually to block. You just saved yourself 15 minutes!', 0]);
    hints.push(['q43', 'URL', 'Clone the previously made URL category and then manually change the block actions to continue. To speed this up further, sort the Site Access column by clicking on "Site Access". Check off each box that has the action of block, hover your mouse over the Site Access column header, click on the arrow that appears and choose Set Selected Actions > Continue.', 0]);

    //tasks.push(['q46', 'Data Filtering', 'Data-filtering? What the heck is that. Configure a regex "Confidential" on all file-types in the upload direction, with an alert threshold of 1 and a block of 5. Set severity to medium, let\'s not get too carried away.', pointsL2]);
    //tasks.push(['q47', 'Data Filtering', 'Configure your data-filtering to alert on 1 and block on 5 Credit Card numbers over all file-types in both directions. This is pretty serious, make sure the severity is set to high.', pointsL2]);
    //tasks.push(['q48', 'Data Filtering', 'Your SE gave you the idea of implanting canary strings to data points and alerting off of them. What a great idea! Look for the following regex: SSBsb3ZlIFBhbG8gQWx0byBOZXR3b3JrcyE= and set alert to 1 and block to 1. You should never see this come across the firewall unless data is being exfilterated (that\'s bad). Set this severity to Critical.', pointsL3]);

    //tasks.push(['q50', 'Security Profile Groups', 'Just like the log forwarding, you\'ve been forgetting to add the security profile group to all rules! One foot out the door. Name the security profile group so that will automatically be applied to all security rules moving forward.', pointsL3]);

    hints.push(['q44', 'URL', 'Can\'t find the option? Look harder. It\'s right there in the URL filtering profile, just not on the Categories sub-tab.', hintPointsL2]);
    hints.push(['q45', 'URL', 'In your custom URL category object, make sure you have the right type selected. It should be "Category Match".', hintPointsL3]);
    hints.push(['q46', 'Data Filtering', 'Set the pattern type to Regular Expression. "Confidential" is case sensitive for this task.', hintPointsL2]);
    hints.push(['q47', 'Data Filtering', 'Set the pattern type to Predefined Pattern and "Credit Card Numbers" is chosen for the Name field. Under the Confidential profile created earlier (pre-req), add a new rule using this object.', hintPointsL2]);
    hints.push(['q48', 'Data Filtering', '']);
    hints.push(['q49', 'Security Profile Groups', '']);
    hints.push(['q50', 'Security Profile Groups', '']);

    return hints;

}


//Perform completion logic
exports.completionLogic = function(fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);

    let tasks = exports.tasks();
    let completedTasks = [];
    let removedTasks = [];
    let messages = [];
    let playerScore = 0;

    // a lot of the checks used the same completion logic, so a generic function was created to minimze code
    function genericCheck(objComplete, playerComplete, descrArr, points) {
        let score = 0;
        if (objComplete === true && playerComplete !== true) {
            console.log(`${fwName} scores ${points} point(s): ${descrArr[0]} - ${descrArr[1]}`);
            messages.push(`${fwName} scores ${points} point(s): ${descrArr[0]} - ${descrArr[1]}`);
            score += points; // L1 FB complete
            playerComplete = true;
        } else if (playerComplete === true && objComplete === false) {
            console.log(`${fwName} LOST ${points} point(s): ${descrArr[0]} - ${descrArr[1]}`);
            messages.push(`${fwName} LOST ${points} point(s): ${descrArr[0]} - ${descrArr[1]}`);
            score -= points;
            playerComplete = false; // reset and change back to false
        }
        objComplete = false;

        return [score, objComplete, playerComplete];
    }

    function updateCompletedTasks(score, taskID) {
        if (score > 0) 
            completedTasks.push(taskID); // ioPlayer.emit completed for strikethrough text
        else if (score < 0) 
            removedTasks.push(taskID) // ioPlayer.emit removed strikethrough for screwed up tasks
        playerScore += score;
    }

    // Check for L1 Zone
    [scoreResult, L1ZoneObjectComplete, L1ZoneComplete[playerNum]] = genericCheck(L1ZoneObjectComplete, L1ZoneComplete[playerNum], ['Zones', 'created zones'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q1');

    // Check for L1 Interface
    [scoreResult, L1InterfaceObjectComplete, L1InterfaceComplete[playerNum]] = genericCheck(L1InterfaceObjectComplete, L1InterfaceComplete[playerNum], ['Zones', 'created interfaces'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q2');

    // Device Config
    [scoreResult, L1DeviceConfigCTDComplete, L1DeviceConfigCTDQBypassComplete[playerNum]] = genericCheck(L1DeviceConfigCTDComplete, L1DeviceConfigCTDQBypassComplete[playerNum], ['Device Config', 'CTD Q Bypass'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q3');

    [scoreResult, L1DeviceConfigCustomUIImgComplete, L1DeviceConfigCustomUILogoComplete[playerNum]] = genericCheck(L1DeviceConfigCustomUIImgComplete, L1DeviceConfigCustomUILogoComplete[playerNum], ['Device Config', 'Custom Main UI Logo'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q4');

    [scoreResult, L1DeviceConfigCustomLoginImgComplete, L1DeviceConfigCustomLoginLogoComplete[playerNum]] = genericCheck(L1DeviceConfigCustomLoginImgComplete, L1DeviceConfigCustomLoginLogoComplete[playerNum], ['Device Config', 'Custom Login UI Logo'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q5');

    [scoreResult, L1DeviceConfigBannerComplete, L1DeviceConfigLoginBannerComplete[playerNum]] = genericCheck(L1DeviceConfigBannerComplete, L1DeviceConfigLoginBannerComplete[playerNum], ['Device Config', 'Banner'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q6');

    [scoreResult, L1DeviceConfigDynamicUpdateComplete, L1DeviceConfigDynUpdateComplete[playerNum]] = genericCheck(L1DeviceConfigDynamicUpdateComplete, L1DeviceConfigDynUpdateComplete[playerNum], ['Device Config', 'Dynamic updates'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q7');

    [scoreResult, L1minimumPasswordComplixityComplete, L1minPassComplixityComplete[playerNum]] = genericCheck(L1minimumPasswordComplixityComplete, L1minPassComplixityComplete[playerNum], ['Device Config', 'Pass Complexity'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q8');

    [scoreResult, L3SharedConfigCorrectUserWithRole, L3SharedAdminRoleComplete[playerNum]] = genericCheck(L3SharedConfigCorrectUserWithRole, L3SharedAdminRoleComplete[playerNum], ['Device Config', 'Admin API Role'], pointsL3);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q9');

    // Security Policies

    // Check for L2 Security Policy - Block Sinkhole IP
    [scoreResult, L2BlockSinkholeIPObjectComplete, L2BlockSinkholeIPComplete[playerNum]] = genericCheck(L2BlockSinkholeIPObjectComplete, L2BlockSinkholeIPComplete[playerNum], ['Security Policies', 'Block Sinkhole IP'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q10');

    // Check for L2 Security Policy - Block back IP lists
    [scoreResult, L2BlockBadIPsObjectComplete, L2BlockBadIPsComplete[playerNum]] = genericCheck(L2BlockBadIPsObjectComplete, L2BlockBadIPsComplete[playerNum], ['Security Policies', 'Block Bad IP lists'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q11');

    // Check for L2 Security Policy - Facebook Schedule
    [scoreResult, L2SchedFacebookObjectComplete, L2FacebookScheduleComplete[playerNum]] = genericCheck(L2SchedFacebookObjectComplete, L2FacebookScheduleComplete[playerNum], ['Security Policies', 'Facebook Schedule'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q12');

    // Check for L2 DoS Protection
    [scoreResult, L2DoSPolicyObjectComplete, L2DoSPolicyComplete[playerNum]] = genericCheck(L2DoSPolicyObjectComplete, L2DoSPolicyComplete[playerNum], ['DoS Protection', 'DoS Policy'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q13');

    // Check for L1 App-ID - uwnwated apps
    [scoreResult, L1AppIDUnwantedObjectComplete, L1AppIDUnwantedComplete[playerNum]] = genericCheck(L1AppIDUnwantedObjectComplete, L1AppIDUnwantedComplete[playerNum], ['App-ID', 'Unwanted Apps'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q14');

    // Check for L2 App-ID - unknown apps
    [scoreResult, L2AppIDUnknownObjectComplete, L2AppIDUnknownComplete[playerNum]] = genericCheck(L2AppIDUnknownObjectComplete, L2AppIDUnknownComplete[playerNum], ['App-ID', 'Unknown Apps'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q15');

    // Check for L3 App-ID encrypted tunnel apps
    [scoreResult, L3AppIDAppFilterObjectComplete, L3AppIDAppFilterComplete[playerNum]] = genericCheck(L3AppIDAppFilterObjectComplete, L3AppIDAppFilterComplete[playerNum], ['App-ID', 'Encrypted Tunnel'], pointsL3);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q16');

    // Check for L1 Log Forwarding profile
    [scoreResult, L1LogFwdObjectComplete, L1LogFwdComplete[playerNum]] = genericCheck(L1LogFwdObjectComplete, L1LogFwdComplete[playerNum], ['Log Forwarding', 'profile created'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q17');

    // Check for L2 Log Forwarding Syslog on severity
    scoreResult = 0;
    if (L2LogFwdObjectComplete === true && L1LogFwdComplete[playerNum] === true && L2LogFwdComplete[playerNum] !== true) {
        console.log(`${fwName} scores ${pointsL2} point(s): Log Forwarding - syslog severity`);
        messages.push(`${fwName} scores ${pointsL2} point(s): Log Forwarding - syslog severity`);
        scoreResult = pointsL2; 
        L2LogFwdComplete[playerNum] = true;
    } else if (L2LogFwdComplete[playerNum] === true && L2LogFwdObjectComplete === false) {
        console.log(`${fwName} LOST ${pointsL2} point(s): Log Forwarding - syslog severity`);
        messages.push(`${fwName} LOST ${pointsL2} point(s): Log Forwarding - syslog severity`);
        scoreResult = -(pointsL2);
        L2LogFwdComplete[playerNum] = false; // reset and change back to false
    }
    L2LogFwdObjectComplete = false;
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q18');

    // Check for L3 Log Forwarding default name
    scoreResult = 0;
    if (L3LogFwdObjectComplete === true && L1LogFwdComplete[playerNum] === true && L2LogFwdComplete[playerNum] === true && L3LogFwdComplete[playerNum] !== true) {
        console.log(`${fwName} scores ${pointsL3} point(s): Log Forwarding - default name`);
        messages.push(`${fwName} scores ${pointsL3} point(s): Log Forwarding - default name`);
        scoreResult = pointsL3; 
        L3LogFwdComplete[playerNum] = true;
    } else if (L3LogFwdComplete[playerNum] === true && L3LogFwdObjectComplete === false) {
        console.log(`${fwName} LOST ${pointsL3} point(s): Log Forwarding - default name`);
        messages.push(`${fwName} LOST ${pointsL3} point(s): Log Forwarding - default name`);
        scoreResult = -(pointsL3);
        L3LogFwdComplete[playerNum] = false; // reset and change back to false
    }
    L3LogFwdObjectComplete = false;
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q19');


    // Check for L1 User-ID
    [scoreResult, L1UserIDZoneObjectComplete, L1UserIDZoneComplete[playerNum]] = genericCheck(L1UserIDZoneObjectComplete, L1UserIDZoneComplete[playerNum], ['User-ID', 'enabled on zones'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q20');

    // Check for L2 User-ID ignore SVC Account
    [scoreResult, L2UserIDIgnoreObjectComplete, L2UserIDIgnoreComplete[playerNum]] = genericCheck(L2UserIDIgnoreObjectComplete, L2UserIDIgnoreComplete[playerNum], ['User-ID', 'ignore SVC account'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q21');

    // check for L1 File Blocking
    scoreResult = 0;
    if (typeof(L1FBname) !== 'undefined' && L1FileBlockingComplete[playerNum] !== true) {
        console.log(`${fwName} scores ${pointsL1} point(s): File Blocking - Alert All`);
        messages.push(`${fwName} scores ${pointsL1} point(s): File Blocking - Alert All`);
        scoreResult = pointsL1; // L1 FB complete
        L1FileBlockingComplete[playerNum] = true;
    } else if (L1FileBlockingComplete[playerNum] === true && typeof(L1FBname) === 'undefined') {
        console.log(`${fwName} LOST ${pointsL1} point(s): File Blocking - Alert All`);
        messages.push(`${fwName} LOST ${pointsL1} point(s): File Blocking - Alert All`);
        scoreResult = -(pointsL1);
        L1FileBlockingComplete[playerNum] = false; // reset and change back to false
    }
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q22');

    // Check for L2 File Blocking (L1 should be complete first)
    scoreResult = 0;
    if (L2FileBlockingComplete[playerNum] !== true && L2FBprofileNameMatch && typeof(L2FBmultiencodefbname) !== 'undefined') {
        console.log(`${fwName} scores ${pointsL2} point(s): File Blocking - Multi-lvl Encode`);
        messages.push(`${fwName} scores ${pointsL2} point(s): File Blocking - Multi-lvl Encode`);
        L2FBprofileNameMatch = false; // reset so it gets processed next time
        scoreResult = pointsL2; // L2 FB complete, award points
        L2FileBlockingComplete[playerNum] = true; // set to complete for player
    } else if (L2FileBlockingComplete[playerNum] === true && L2FBprofileNameMatch !== true && typeof(L2FBmultiencodefbname) === 'undefined') { // check if they screwed it up later
        console.log(`${fwName} LOST ${pointsL2} point(s): File Blocking - Multi-lvl Encode`);
        messages.push(`${fwName} LOST ${pointsL2} point(s): File Blocking - Multi-lvl Encode`);
        scoreResult = -(pointsL2);
        L2FileBlockingComplete[playerNum] = false; // reset and change back to false
    } else if (L1FileBlockingComplete[playerNum] !== true && L2FileBlockingComplete[playerNum] === true ) {
        // this condition should not happen and lose points
        console.log(`${fwName} LOST ${pointsL2} point(s): File Blocking - Multi-lvl Encode`);
        messages.push(`${fwName} LOST ${pointsL2} point(s): File Blocking - Multi-lvl Encode`);
        scoreResult = -(pointsL2);
        L2FileBlockingComplete[playerNum] = false; // reset and change back to false
    }
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q23');

    // Check for L3 File Blocking - drive-by downloads
    [scoreResult, L3FBDriveByObjectComplete, L3FBDriveByComplete[playerNum]] = genericCheck(L3FBDriveByObjectComplete, L3FBDriveByComplete[playerNum], ['File Blocking', 'Drive-by Downloads'], pointsL3);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q24');

    // Wildfire 
    [scoreResult, L1DeviceConfigWildFireConfigComplete, L1DeviceConfigWFConfigComplete[playerNum]] = genericCheck(L1DeviceConfigWildFireConfigComplete, L1DeviceConfigWFConfigComplete[playerNum], ['Wildfire', 'Max file sizes'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q25');

    // Check for L1-A AV
    scoreResult = 0;
    if (L1AVprofileNameMatch && L1AntivirusComplete[playerNum] !== true) {
        console.log(`${fwName} scores ${pointsL1} point(s): AV - reset-both All`);
        messages.push(`${fwName} scores ${pointsL1} point(s): AV - reset-both All`);
        scoreResult = pointsL1; // L1 Spyware complete, award points
        L1AntivirusComplete[playerNum] = true; // set to complete for player so we can revoke if needed later
    } else if (L1AntivirusComplete[playerNum] === true && (typeof(L1aAVCorrectavname) === 'undefined' || typeof(L1bAVCorrectavname) === 'undefined')) {
        console.log(`${fwName} LOST ${pointsL1} point(s): AV - reset-both All`);
        messages.push(`${fwName} LOST ${pointsL1} point(s): AV - reset-both All`);
        L1AntivirusComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL1);
    }
    L1AVprofileNameMatch = false;
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q26');

    // Check for L1 Spyware
    scoreResult = 0;
    if (L1SpywareprofileNameMatch && L1SpywareComplete[playerNum] !== true) {
        console.log(`${fwName} scores ${pointsL1} point(s): Spyware - Block Low-Crit/Default Inf`);
        messages.push(`${fwName} scores ${pointsL1} point(s): Spyware - Block Low-Crit/Default Inf`);
        scoreResult = pointsL1; // L1 Spyware complete, award points
        L1SpywareComplete[playerNum] = true; // set to complete for player so we can revoke if needed later
    } else if (L1SpywareComplete[playerNum] === true && (typeof(L1SpywareCorrectResetClientName) === 'undefined' || typeof(L1SpywarecorrectSpyname) === 'undefined')) {
        console.log(`${fwName} LOST ${pointsL1} point(s): Spyware - Block Low-Crit/Default Inf`);
        messages.push(`${fwName} LOST ${pointsL1} point(s): Spyware - Block Low-Crit/Default Inf`);
        L1SpywareComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL1);
    }
    L1SpywareprofileNameMatch = false; // reset so this gets re-processed every time to remove points if a mistake is made.
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q27');

    // check for L2 Spyware
    scoreResult = 0;
    if (L2SpywareSinkholeName && L2SpywareComplete[playerNum] !== true) {
        console.log(`${fwName} scores ${pointsL2} point(s): Spyware - Sinkholing`);
        messages.push(`${fwName} scores ${pointsL2} point(s): Spyware - Sinkholing`);
        scoreResult = pointsL2; // L2 Spyware complete, award points
        L2SpywareComplete[playerNum] = true; // set to complete for player so we can revoke if needed later
    } else if (L2SpywareComplete[playerNum] === true && typeof(L2SpywareSinkholeName) === 'undefined') {
        console.log(`${fwName} LOST ${pointsL2} point(s): Spyware - Sinkholing)`);
        messages.push(`${fwName} LOST ${pointsL2} point(s): Spyware - Sinkholing)`);
        L2SpywareComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL2);
    }
    L2SpywareSinkholeName = false;
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q28');

    // Check for L3 Spyware - Content-ID extended PCAP
    [scoreResult, L3SpywareExtendedPCAPComplete, L3SpywareComplete[playerNum]] = genericCheck(L3SpywareExtendedPCAPComplete, L3SpywareComplete[playerNum], ['Spyware', 'Extended PCAP'], pointsL3);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q29');

    // Check for L2 QoS Policy
    [scoreResult, L1QoSObjectComplete, L1QoSComplete[playerNum]] = genericCheck(L1QoSObjectComplete, L1QoSComplete[playerNum], ['QoS', 'Policy apple-update'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q30');

    // Check for L3 Security Policy - Facebook Schedule
    [scoreResult, L3PolicyQoSObjectComplete, L3PolicyQoSComplete[playerNum]] = genericCheck(L3PolicyQoSObjectComplete, L3PolicyQoSComplete[playerNum], ['Security Policies', 'VOIP QoS'], pointsL3);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q31');

    // Check for L1 Decrypt Certs
    [scoreResult, L1DecryptCertsObjectComplete, L1DecryptCertComplete[playerNum]] = genericCheck(L1DecryptCertsObjectComplete, L1DecryptCertComplete[playerNum], ['Decrypt', 'Forward Trust/Untrust Certs'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q32');

    // Check for L1 SSH Decrypt Policy
    [scoreResult, L1DecryptSSHPolicyObjectComplete, L1DecryptSSHComplete[playerNum]] = genericCheck(L1DecryptSSHPolicyObjectComplete, L1DecryptSSHComplete[playerNum], ['Decrypt', 'SSH Proxy Policy'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q33');

    // Check for L2 Decrypt Policies
    [scoreResult, L1DecryptObjectComplete, L1DecryptComplete[playerNum]] = genericCheck(L1DecryptObjectComplete, L1DecryptComplete[playerNum], ['Decrypt', 'Forward Proxy Policy'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q34');

    // Check for L2 Decrypt Forward Decrypted Content
    [scoreResult, L2DecryptForwardContentObjectComplete, L2DecryptForwardContentComplete[playerNum]] = genericCheck(L2DecryptForwardContentObjectComplete, L2DecryptForwardContentComplete[playerNum], ['Decrypt', 'Forward Decrytped Content'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q35');

    // Check for L1 Zone Packet Buff
    [scoreResult, L1ZonePacketBuffObjectComplete, L1ZonePacketBuffComplete[playerNum]] = genericCheck(L1ZonePacketBuffObjectComplete, L1ZonePacketBuffComplete[playerNum], ['Zone Protection', 'Packet Buffer'], pointsL1);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q36');

    // Check for L2 Zone Protection Protile
    [scoreResult, L2ZonePPObjectComplete, L2ZonePPComplete[playerNum]] = genericCheck(L2ZonePPObjectComplete, L2ZonePPComplete[playerNum], ['Zone Protection', 'ZonePP'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q37');

    // Check for L1 Vulnerability Client
    scoreResult = 0;
    if (L1VulnprofileClientNameMatch && L1VulnerabilityClientComplete[playerNum] !== true) {
        console.log(`${fwName} scores ${pointsL1} point(s): Vulnerability - Reset-Client Med-Crit/Default Inf-Low`);
        messages.push(`${fwName} scores ${pointsL1} point(s): Vulnerability - Reset-Client Med-Crit/Default Inf-Low`);
        scoreResult = pointsL1; // L1 Vuln client complete, award points
        L1VulnerabilityClientComplete[playerNum] = true; // set to complete for player so we can revoke if needed later
    } else if (L1VulnerabilityClientComplete[playerNum] === true && (typeof(L1VulCorrectResetClientName) === 'undefined' || typeof(L1VulCorrectDefaultClientName) === 'undefined')) {
        console.log(`${fwName} LOST ${pointsL1} point(s): Vulnerability -  Reset-Client Med-Crit/Default Inf-Low`);
        messages.push(`${fwName} LOST ${pointsL1} point(s): Vulnerability -  Reset-Client Med-Crit/Default Inf-Low`);
        L1VulnerabilityClientComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL1);
    } 
    L1VulnprofileClientNameMatch = false;
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q38');

    // Check for L1 Vulnerability Server
    scoreResult = 0;
    if (L1VulnprofileServerNameMatch && L1VulnerabilityServerComplete[playerNum] !== true) {
        console.log(`${fwName} scores ${pointsL1} point(s): Vulnerability - Reset-Server Med-Crit/Default Inf-Low`);
        messages.push(`${fwName} scores ${pointsL1} point(s): Vulnerability - Reset-Server Med-Crit/Default Inf-Low`);
        scoreResult = pointsL1; // L1 Spyware complete, award points
        L1VulnerabilityServerComplete[playerNum] = true; // set to complete for player so we can revoke if needed later
    } else if (L1VulnerabilityServerComplete[playerNum] === true && (typeof(L1VulCorrectResetServerName) === 'undefined' || typeof(L1VulCorrectDefaultServerName) === 'undefined')) {
        console.log(`${fwName} LOST ${pointsL1} point(s): Vulnerability - Reset-Server Med-Crit/Default Inf-Low`);
        messages.push(`${fwName} LOST ${pointsL1} point(s): Vulnerability - Reset-Server Med-Crit/Default Inf-Low`);
        L1VulnerabilityServerComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL1);
    }
    L1VulnprofileServerNameMatch = false;
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q39');

    // Check for L2 Vuln Exception. L1 Client Reset needs to be completed first.
    scoreResult = 0;
    if (L1VulnerabilityClientComplete[playerNum] === true && L2VulnerabilityExceptionComplete[playerNum] !== true && L1VulCorrectResetClientName === L2VulCorrectExceptionName) {
        console.log(`${fwName} scores ${pointsL2} point(s): Vulnerability - HTTP Evasion Exception`);
        messages.push(`${fwName} scores ${pointsL2} point(s): Vulnerability - HTTP Evasion Exception`);
        L2VulnerabilityExceptionComplete[playerNum] = true;
        scoreResult = pointsL2 // L2 Vuln exception complete, award points
    } else if (L1VulnerabilityClientComplete[playerNum] === true && L2VulnerabilityExceptionComplete[playerNum] === true && typeof(L2VulCorrectExceptionName) === 'undefined') {
        console.log(`${fwName} LOST ${pointsL2} point(s): Vulnerability - HTTP Evasion Exception`);
        messages.push(`${fwName} LOST ${pointsL2} point(s): Vulnerability - HTTP Evasion Exception`);
        L2VulnerabilityExceptionComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL2);
    } else if (L1VulnerabilityClientComplete[playerNum] !== true && L2VulnerabilityExceptionComplete[playerNum] === true ) {
        // this condition is not valid and points should be removed
        console.log(`${fwName} LOST ${pointsL2}: Vulnerability - HTTP Evasion Exception`);
        messages.push(`${fwName} LOST ${pointsL2} point(s): Vulnerability - HTTP Evasion Exception`);
        L2VulnerabilityExceptionComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL2);
    }
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q40');

    // Check for L3 Vuln Custom Sig
    [scoreResult, L3VulnerabilityCustomObjectComplete, L3VulnerabilityCustomComplete[playerNum]] = genericCheck(L3VulnerabilityCustomObjectComplete, L3VulnerabilityCustomComplete[playerNum], ['Vulnerability', 'Custom sig'], pointsL3);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q41');

    // Check for L1a URL Filtering
    scoreResult = 0;
    if (L1aURLNameMatch && L1aURLComplete[playerNum] !== true && L1aBlockURLCorrectname === L1aAlertURLCorrectname) {
        console.log(`${fwName} scores ${pointsL1} point(s): URL - Block Risky Categories`);
        messages.push(`${fwName} scores ${pointsL1} point(s): URL - Block Risky Categories`);
        scoreResult = pointsL1; // L1 Spyware complete, award points
        L1aURLComplete[playerNum] = true; // set to complete for player so we can revoke if needed later
    } else if (L1aURLComplete[playerNum] === true && L1aURLNameMatch === false && L1aBlockURLCorrectname !== L1aAlertURLCorrectname) {
        console.log(`${fwName} LOST ${pointsL1} point(s): URL - Block Risky Categories`);
        messages.push(`${fwName} LOST ${pointsL1} point(s): URL - Block Risky Categories`);
        L1aURLComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL1); // reset so it gets re-processed every time
    }
    L1aURLNameMatch = false;
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q42');

    // Check for L1b URL Filtering
    scoreResult = 0;
    if (L1bURLNameMatch && L1bURLComplete[playerNum] !== true && L1bContinueURLCorrectname === L1bAlertURLCorrectname) {
        console.log(`${fwName} scores ${pointsL1} point(s): URL - Continue Risky Categories`);
        messages.push(`${fwName} scores ${pointsL1} point(s): URL - Continue Risky Categories`);
        scoreResult = pointsL1; // L1 Spyware complete, award points
        L1bURLComplete[playerNum] = true; // set to complete for player so we can revoke if needed later
    } else if (L1bURLComplete[playerNum] === true && L1bURLNameMatch === false && L1bContinueURLCorrectname !== L1bAlertURLCorrectname)        {
        console.log(`${fwName} LOST ${pointsL1} point(s): URL - Continue Risky Categories`);
        messages.push(`${fwName} LOST ${pointsL1} point(s): URL - Continue Risky Categories`);
        L1bURLComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL1); // reset so it gets re-processed every time
    }
    L1bURLNameMatch = false;
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q43');

    // Check for L2 URL Container Page
    scoreResult = 0;
    if (L2URLContainerComplete && L2URLComplete[playerNum] !== true && typeof(L2URLCorrectname) !== 'undefined' && L2URLCorrectname === L1aAlertURLCorrectname) {
        console.log(`${fwName} scores ${pointsL2} point(s): URL - Container Page`);
        messages.push(`${fwName} scores ${pointsL2} point(s): URL - Container Page`);
        scoreResult = pointsL2; // L1 Spyware complete, award points
        L2URLComplete[playerNum] = true; // set to complete for player so we can revoke if needed later
    } else if (L2URLComplete[playerNum] === true && L2URLContainerComplete === false && typeof(L2URLCorrectname) === 'undefined') {
        console.log(`${fwName} LOST ${pointsL2} point(s): URL - Container Page`);
        messages.push(`${fwName} LOST ${pointsL2} point(s): URL - Container Page`);
        L2URLComplete[playerNum] = false; // player lost, set back to false
        scoreResult = -(pointsL2); // reset so it gets re-processed every time
    }
    L2URLContainerComplete = false; // reset so this gets re-processed every time to remove points if a mistake is made.
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q44');

    // Check for L3 Custom Category 
    scoreResult = 0;
    if (L3URLCustomURLComplete && L3URLComplete[playerNum] !== true && typeof( L3URLCustomURLContinue) !== 'undefined' && typeof(L3URLCustomURLBlock) !== 'undefined') {
        console.log(`${fwName} scores ${pointsL3} point(s): URL - Custom Category`);
        messages.push(`${fwName} scores ${pointsL3} point(s): URL - Custom Category`);
        scoreResult = pointsL3; // L1 Spyware complete, award points
        L3URLComplete[playerNum] = true; // set to complete for player so we can revoke if needed later
    } else if (L3URLComplete[playerNum] === true && L3URLCustomURLComplete === false && (typeof(L3URLCustomURLContinue) === 'undefined' || typeof(L3URLCustomURLBlock) === 'undefined' )) {
        console.log(`${fwName} LOST ${pointsL3} point(s): URL - Custom Category`);
        messages.push(`${fwName} LOST ${pointsL3} point(s): URL - Custom Category`);
        L3URLComplete[playerNum] = false; // player lost, set back to false
        //L2URLContainerComplete = false;
        scoreResult = -(pointsL3); // reset so it gets re-processed every time
    }
    L3URLCustomURLComplete = false; // reset so this gets re-processed every time to remove points if a mistake is made.
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q45');

    // Check for L2 Data filtering Confidential Alert
    [scoreResult, L2DataAlertObjectiveComplete, L2DataAlertComplete[playerNum]] = genericCheck(L2DataAlertObjectiveComplete, L2DataAlertComplete[playerNum], ['Data Filtering', 'Confidential'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q46');

    // Check for L2 Data filtering Confidential Alert
    [scoreResult, L2DataCCObjectiveComplete, L2DataCCComplete[playerNum]] = genericCheck(L2DataCCObjectiveComplete, L2DataCCComplete[playerNum], ['Data Filtering', 'Credit Card'], pointsL2);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q47');

    // Check for L3 Data filtering DB
    [scoreResult, L3DataPatternComplete, L3DataDBPatternComplete[playerNum]] = genericCheck(L3DataPatternComplete, L3DataDBPatternComplete[playerNum], ['Data Filtering', 'DB'], pointsL3);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q48');

    //if (L1SecurityProfileGroupComplete && L1SPGComplete[playerNum] !== true && L1SecurityProfileGroupName === spgProfileName) {
    // ******** THIS NEEDS TO BE VALIDATED
    scoreResult = 0;
    if (L1SecurityProfileGroupComplete && L1SPGComplete[playerNum] !== true) {
        console.log(`${fwName} scores ${pointsL1} point(s): Security Profile Groups - Profile complete`);
        messages.push(`${fwName} scores ${pointsL1} point(s): Security Profile Groups - Profile complete`);
        scoreResult = pointsL1;
        L1SPGComplete[playerNum] = true;
    } else if (L1SPGComplete[playerNum] === true && L1SecurityProfileGroupComplete === false && typeof(L1SecurityProfileGroupName) === 'undefined') {
        console.log(`${fwName} LOST ${pointsL1} point(s): Security Profile Groups - Profile complete`);
        messages.push(`${fwName} LOST ${pointsL1} point(s): Security Profile Groups - Profile complete`);
        L1SPGComplete[playerNum] = false;
        scoreResult = -(pointsL1);
    }
    L1SecurityProfileGroupComplete = false;
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q49');

    // Check for L3 Security Profile Group - default name
    [scoreResult, L3SecurityProfileGroupComplete, L3SPGComplete[playerNum]] = genericCheck(L3SecurityProfileGroupComplete, L3SPGComplete[playerNum], ['Security Profile Groups', 'Profile default name'], pointsL3);
    if (scoreResult != 0) updateCompletedTasks(scoreResult, 'q50');

    //  
    return ([messages, playerScore, completedTasks, removedTasks]);
}

//Virtual Routers
exports.scoringVirtualRouters = function(arr, fwName) {
    // function expects an array in: eg. [ { vr: vrName, interfaces: interfaces } ]
    // the calling function only sends over if there are interfaces attached to the VR.

    if (debug_vr) {
        debugOuptut = `???????????????????????? Virtual Routers ????????????????????????\n`; 
        debugOuptut += `\n     ********** Pre Variables **********\n`;
        //debugOuptut += ;
    }

    for (i in arr) {
        if (arr[i].interfaces.length === 2 && arr[i].interfaces.includes('ethernet1/1') && arr[i].interfaces.includes('ethernet1/2')) {
            L1VirtualRouterObjectComplete = true;
            if (debug_vr) debugOuptut += `\n     length = 2 and includes ethernet1/1 and ethernet1/2`;
        }

    }

    fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
        if (err) throw err;
    });

}

// Physical Interfaces
exports.scoringPhysicalInterfaces = function(arr, fwName) {

    let externalInt = false;
    let internalInt = false;

    if (debug_physInt) {
        debugOuptut = `???????????????????????? Physical Interfaces ????????????????????????\n`; 
        debugOuptut += `\n     ********** Pre Variables **********\n`;
        debugOuptut += `\n     external int = ${externalInt}`;
        debugOuptut += `\n     internal int = ${internalInt}`;
        debugOuptut += `\n     L1physicalInterfaceObjectComplete = ${L1physicalInterfaceObjectComplete}`;
    }

    for (i in arr) {
        if (arr[i].interfaceName === 'ethernet1/1' && arr[i].ip === 'dhcp') {
            externalInt = true;
        }
        if (arr[i].interfaceName === 'ethernet1/2' && arr[i].ip === '192.168.0.254/24') {
            internalInt = true;
        }
    }

    if (externalInt && internalInt)
        L1physicalInterfaceObjectComplete = true;

    if (debug_physInt) {
        debugOuptut = `???????????????????????? Physical Interfaces ????????????????????????\n`; 
        debugOuptut += `\n     ********** Post Variables **********\n`;
        debugOuptut += `\n     external int = ${externalInt}`;
        debugOuptut += `\n     internal int = ${internalInt}`;
        debugOuptut += `\n     L1physicalInterfaceObjectComplete = ${L1physicalInterfaceObjectComplete}`;

        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
    }
}


//QoS - Profiles
exports.scoringQoSProfiles = function(class1, class2, class3, class4, class5, class6, class7, class8, name, fwName) {
    if (qosProfileDebug == true) {
        debugOuptut = `???????????????????????? QoS Profiles ????????????????????????\n`; 
        debugOuptut += `\n     ********** Pre Variables **********\n`;
        debugOuptut += class1, class2, class3, class4, class5, class6, class7, class8, name, fwName;
        debugOuptut += `\nL1QoSProfileObjectName = ${L1QoSProfileObjectName}\n`;
    }

    if (class8.length === 3 && class8[0] == 5 && class8[1] == 0 && class8[2] == 'low') {
        if (qosProfileDebug == true) debugOuptut += `name ${name} is correct\n`;
        L1QoSProfileObjectName = name;
    }
}

//QoS - Interfaces
exports.scoringQoSInterface = function(intName, defaultClearTextProfile, fwName) {
    if (qosInterfaceDebug == true) {
        debugOuptut = `???????????????????????? QoS Interface ????????????????????????\n`; 
        debugOuptut += `\n     ********** Pre Variables **********\n`;
        debugOuptut += intName, defaultClearTextProfile, fwName;
        debugOuptut += `\nL1QoSProfileObjectName = ${L1QoSProfileObjectName}\n`;
    }

    if (intName === 'ethernet1/2' && defaultClearTextProfile == L1QoSProfileObjectName)
        L1QoSInterfaceComplete = true;

    if (qosInterfaceDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Network Profiles
exports.processZoneProfiles = function(arr, fwName) {
    let [name, flood, scan, looseSourceRouting, unknownOption, malformedOption, tcpSplitHandshake, overlappingTCPSegment, tcpTimestamp] = [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8]];

    let [tcpScanComplete, hostSweepComplete, udpScanComplete] = [false, false, false];

    if (netProfilesDebug) {
        debugOuptut = `???????????????????????? Net Profiles ????????????????????????\n`; 
        debugOuptut += `\n     ********** Pre Variables **********\n`;
        debugOuptut += `\n     name = ${name}\n     flood = ${flood}\n     scan = ${scan}\n     looseSourceRouting = ${looseSourceRouting}\n     unknownOption = ${unknownOption}\n     malformedOption = ${malformedOption}\n     tcpSplitHandshake = ${tcpSplitHandshake}\n     overlappingTCPSegment = ${overlappingTCPSegment}\n     tcpTimestamp = ${tcpTimestamp}\n`;
    }

    if (Object.prototype.toString.call(scan) === '[object Array]')
        for (arr in scan) {
            if (scan[arr]['$'].name === '8001' && Object.keys(scan[arr].action) == 'block' && scan[arr].interval == 2 && scan[arr].threshold == 100) 
                tcpScanComplete = true;
            if (scan[arr]['$'].name == '8002' && Object.keys(scan[arr].action) == 'block' && scan[arr].interval == 10 && scan[arr].threshold == 100) 
                hostSweepComplete = true;
            if (scan[arr]['$'].name === '8003' && Object.keys(scan[arr].action) == 'block' && scan[arr].interval == 2 && scan[arr].threshold == 100) 
                udpScanComplete = true;
        }

    if (looseSourceRouting === 'yes' && unknownOption === 'yes' && malformedOption === 'yes' && tcpSplitHandshake === 'yes' && overlappingTCPSegment === 'yes' && tcpTimestamp === 'yes' && tcpScanComplete && hostSweepComplete && udpScanComplete)
        L2ZoneProfileObjectName = name;

    if (netProfilesDebug) {
        debugOuptut += `scan\n`;
        debugOuptut += scan;
        debugOuptut += '\n';
        debugOuptut += `\n     ********** Post Variables **********\n`;
        debugOuptut += `     L2ZoneProfileObjectName = ${L2ZoneProfileObjectName}\n`;
    }

    if (netProfilesDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Policies - QoS Policies
exports.scoringQoSPolicies = function(arr, fwName) {

    let [ruleName, toZone, fromZone, srcIP, dstIP, user, category, app, service, action, tag, schedule, dscp, disabled] = [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9], arr[10], arr[11], arr[12], arr[13]];

    if (qosPoliciesDebug == true) {
        debugOuptut = `???????????????????????? QoS Policies ????????????????????????\n`; 
        debugOuptut += `\n     ********** Pre Variables **********\n`;
        //debugOuptut += `     L2BlockSinkholeIPObjectComplete = ${L2BlockSinkholeIPObjectComplete}\n`;
        //debugOuptut += `     L2SchedFacebookObjectComplete = ${L2SchedFacebookObjectComplete}\n`;
        debugOuptut += `\n     ruleName = ${ruleName}\n     toZone = ${toZone}\n     fromZone = ${fromZone}\n     srcIP = ${srcIP}\n     dstIP = ${dstIP}\n     user = ${user}\n     category = ${category}\n     app = ${app}\n     service = ${service}\n     action = ${action}\n     tag = ${tag}\n     schedule = ${schedule}\n`;
    }

    if (disabled !== 'yes') {
        if (toZone === 'external' && fromZone === 'internal' && srcIP === 'any' && dstIP === 'any' && user === 'any' && category === 'any' && app === 'apple-update' && service === 'application-default' && action == 8) {
            if (qosPoliciesDebug == true) debugOuptut += `Policy is correct\n`;
            L1QoSPolicyComplete = true;
        }

        if (L1QoSPolicyComplete && L1QoSInterfaceComplete)
            L1QoSObjectComplete = true;
    }

    if (qosPoliciesDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Policies - Security Policies
exports.scoringSecurityPolicies = function(arr, fwName) {

    let [ruleName, toZone, fromZone, srcIP, dstIP, user, category, app, service, hip, action, tag, schedule, qos, profileType, profileSettings, disabled] = [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9], arr[10], arr[11], arr[12], arr[13], arr[14], arr[15], arr[16]];

    if (secPoliciesDebug) {
        debugOuptut = `???????????????????????? Security Policies ????????????????????????\n`; 
        debugOuptut += `\n     ********** Pre Variables **********\n`;
        debugOuptut += `     L2BlockSinkholeIPObjectComplete = ${L2BlockSinkholeIPObjectComplete}\n`;
        debugOuptut += `     L2SchedFacebookObjectComplete = ${L2SchedFacebookObjectComplete}\n`;
        debugOuptut += `     L2BlockSinkholeIPObjectName = ${L2BlockSinkholeIPObjectName}\n`;
        debugOuptut += `\n     ruleName = ${ruleName}\n     toZone = ${toZone}\n     fromZone = ${fromZone}\n     srcIP = ${srcIP}\n     dstIP = ${dstIP}\n     user = ${user}\n     category = ${category}\n     app = ${app}\n     service = ${service}\n     hip = ${hip}\n     action = ${action}\n     tag = ${tag}\n     profileType = ${profileType}\n     profileSettings = ${profileSettings}\n     schedule = ${schedule}\n     qos = ${qos}\n     disabled = ${disabled}\n`;
    }

    if (disabled !== 'yes') { // check if rule is disabled

        // L2 block sinkhole IP (72.5.65.111)
        if ((dstIP === '72.5.65.111' || dstIP === '72.5.65.111/32') || (typeof(L2BlockSinkholeIPObjectName) !== 'undefined' && L2BlockSinkholeIPObjectName == dstIP)) 
            if (toZone === 'external' && fromZone === 'any' && srcIP === 'any' && user === 'any' && category === 'any' && app === 'any' && service === 'any' && hip === 'any' && (action === 'drop' || action === 'deny')) {
                L2BlockSinkholeIPObjectComplete = true;
                if (secPoliciesDebug) {
                    debugOuptut += `\n     L2BlockSinkholeIPObjectComplete = ${L2BlockSinkholeIPObjectComplete}\n`;
                    debugOuptut += `\n     L2BlockSinkholeIPObjectName = ${L2BlockSinkholeIPObjectName}\n`;
                }
            }

        // L2 - Block bad IP lists
        if (Object.prototype.toString.call(dstIP) === '[object Array]') 
            if (dstIP.length == 3 && dstIP.includes('panw-highrisk-ip-list') && dstIP.includes('panw-known-ip-list') && dstIP.includes('panw-bulletproof-ip-list'))
                if (toZone === 'external' && fromZone === 'any' && user === 'any' && category === 'any' && app === 'any' && service === 'any' && hip === 'any' && (action === 'drop' || action === 'deny'))
                    L2BlockBadIPsObjectComplete = true;

        // L2 - Facebook schedule
        if (Object.prototype.toString.call(app) === '[object String]' && app === 'facebook' && typeof(schedule) !== 'undefined') 
            if (toZone === 'external' && fromZone === 'internal' && dstIP === 'any' && user === 'any' && category === 'any' && service === 'application-default' && hip === 'any' && schedule == `${L2SchedFacebookObjectName}` && action === 'allow') 
                L2SchedFacebookObjectComplete = true;

        // L3 - QoS marking
        if (Object.prototype.toString.call(app) === '[object Array]' && app.length == '2' && app.includes('sip') && app.includes('rtp') && typeof(qos) !== 'undefined') 
            if (typeof(qos.marking) !== 'undefined' && Object.keys(qos.marking).includes(`ip-dscp`)) 
                if (toZone === 'external' && fromZone === 'internal' && dstIP === 'any' && user === 'any' && category === 'any' && service === 'application-default' && hip === 'any' && qos.marking['ip-dscp'] === 'ef') 
                    L3PolicyQoSObjectComplete = true;

        // L3 - file blocking drive-by downloads
        if (profileType === 'profiles' && profileSettings !== 'none' && typeof(profileSettings['file-blocking']) !== 'undefined')
            if (toZone === 'external' && fromZone === 'internal' && dstIP === 'any' && srcIP === 'any' && user === 'any' && category === 'unknown' && service === 'application-default' && hip === 'any' && Object.prototype.toString.call(app) === '[object String]' && app === 'web-browsing' && action === 'allow' && profileSettings['file-blocking'].member === L3FBcorrectfbname)
                L3FBDriveByObjectComplete = true;

        // L1 - App-ID block unwanted apps
        if (Object.prototype.toString.call(app) === '[object Array]' && app.length == 2 && app.includes('bittorrent') && app.includes('tor'))
            if (toZone === 'external' && fromZone === 'internal' && dstIP === 'any' && srcIP === 'any' && user === 'any' && category === 'any' && service === 'any' && hip === 'any' && (action === 'drop' || action === 'deny'))
                L1AppIDUnwantedObjectComplete = true;

        // L1 - App-ID block unwanted apps
        if (Object.prototype.toString.call(app) === '[object Array]' && app.length == 3 && app.includes('unknown-tcp') && app.includes('unknown-udp') && app.includes('unknown-p2p'))
            if (toZone === 'external' && fromZone === 'internal' && dstIP === 'any' && srcIP === 'any' && user === 'any' && category === 'any' && service === 'any' && hip === 'any' && (action === 'drop' || action === 'deny')) 
                L2AppIDUnknownObjectComplete = true;

        // L3 - App-ID block encrypted tunnel
        if (toZone === 'external' && fromZone === 'internal' && dstIP === 'any' && srcIP === 'any' && app === L3AppIDFilterObjectName && user === 'any' && category === 'any' && service === 'any' && hip === 'any' && (action === 'drop' || action === 'deny'))
            L3AppIDAppFilterObjectComplete = true;
    }


    if (secPoliciesDebug) {
        debugOuptut += `\n     ********** Post Variables **********\n`;
        debugOuptut += `     L2BlockSinkholeIPObjectComplete = ${L2BlockSinkholeIPObjectComplete}\n`;
        debugOuptut += `     L2SchedFacebookObjectComplete = ${L2SchedFacebookObjectComplete}\n`;
        debugOuptut += `     L2BlockSinkholeIPObjectName = ${L2BlockSinkholeIPObjectName}\n`;
        debugOuptut += `\n???????????????????????????????????????????????????????????????????\n`;
    }

    if (secPoliciesDebug) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Policies - DoS Policies
exports.scoringDoSPolicies = function(arr, fwName) {
    //ruleName, tag, toZone, fromZone, srcIP, dstIP, user, service, protection, action, position

    let [ruleName, tag, toZone, fromZone, srcIP, dstIP, user, service, action, position, classificationCriteria, classificationProfile, disabled] = [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9], arr[10], arr[11], arr[12]];

    if (dosPoliciesDebug == true) {
        debugOuptut = `???????????????????????? DoS Policies ????????????????????????\n`; 
        debugOuptut += `\n     ********** Pre Variables **********\n`;
        debugOuptut += `\n     ruleName = ${ruleName}\n     toZone = ${toZone}\n     fromZone = ${fromZone}\n     srcIP = ${srcIP}\n     dstIP = ${dstIP}\n     user = ${user}\n     service = ${service}\n     action = ${action}\n     tag = ${tag}\n      classificationCriteria = ${classificationCriteria}\n      classificationProfile = ${classificationProfile}\n      position = ${position}\n`;
    }

    if (disabled !== 'yes') {
        if (toZone === 'internal' && fromZone === 'external' && srcIP === 'any' && user === 'any' && service === 'service-https' && action === 'protect' && classificationProfile == L2DosProfileObjectName && classificationCriteria === 'source-ip-only' && (dstIP === '56.2.217.65' || dstIP === '56.2.217.65/32' || (typeof(L2DosObjectName) !== 'undefined' && L2DosObjectName == dstIP)))
            L2DoSPolicyObjectComplete = true;
    }

    if (dosPoliciesDebug == true) {
        debugOuptut += `\n     ********** Post Variables **********\n`;
        debugOuptut += `     L2DosProfileObjectName = ${L2DosProfileObjectName}\n`;
        debugOuptut += `\n???????????????????????????????????????????????????????????????????\n`;
    }

    if (dosPoliciesDebug == true) 
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Policies - Decrypt Policies
exports.scoringDecryptPolicies = function(arr, fwName) {

    let [ruleName, toZone, fromZone, srcIP, dstIP, user, category, service, action, tag, type, position, disabled] = [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9], arr[10], arr[11], arr[12]];

    if (secDecryptDebug == true) {
        debugOuptut = `???????????????????????? Decrypt Policies ????????????????????????\n`; 
        debugOuptut += `\n     ********** Pre Variables **********\n`;
        debugOuptut += `\n     ruleName = ${ruleName}\n     toZone = ${toZone}\n     fromZone = ${fromZone}\n     srcIP = ${srcIP}\n     dstIP = ${dstIP}\n     user = ${user}\n     category = ${category}\n     category.length = ${category.length}\n     service = ${service}\n     action = ${action}\n     tag = ${tag}\n     position = ${position}\n     type = ${type}\n     L1noDecryptPos = ${L1noDecryptPos}\n     L1decryptPos = ${L1decryptPos}\n     disabled = ${disabled}\n`;
    }

    if (disabled !== 'yes') {

        if (toZone === 'external' && fromZone === 'internal' && srcIP === 'any' && dstIP === 'any' && user === 'any' && category.length === 3 && category.includes('financial-services') && category.includes('health-and-medicine') && category.includes('government') && service === 'any' && action === 'no-decrypt') {
            if (secDecryptDebug == true) debugOuptut += `no-decrypt policy is correct\n`;
            L1noDecryptPos = position;
            L1DecryptNoDecryptObjectComplete = true;
        }

        if (toZone === 'external' && fromZone === 'internal' && srcIP === 'any' && dstIP === 'any' && user === 'any' && category == 'any' && service === 'any' && action === 'decrypt' && type == 'ssl-forward-proxy') {
            if (secDecryptDebug == true) debugOuptut += 'decrypt policy is correct\n';
            L1DecryptPolicyObjectComplete = true;
            L1decryptPos = position;
        }

        if (toZone === 'external' && fromZone === 'internal' && srcIP === 'any' && dstIP === 'any' && user === 'any' && category == 'any' && service === 'any' && action === 'decrypt' && type == 'ssh-proxy') {
            if (secDecryptDebug == true) debugOuptut += 'decrypt SSH policy is correct\n';
            L1DecryptSSHPolicyObjectComplete = true;
        }

        if (L1DecryptNoDecryptObjectComplete && L1DecryptPolicyObjectComplete && L1DecryptCertsObjectComplete && L1noDecryptPos < L1decryptPos)
            L1DecryptObjectComplete = true;
    }

    debugOuptut += `\n     ********** Post Variables **********\n`;
    debugOuptut += `\n     ruleName = ${ruleName}\n     toZone = ${toZone}\n     fromZone = ${fromZone}\n     srcIP = ${srcIP}\n     dstIP = ${dstIP}\n     user = ${user}\n     category = ${category}\n     category.length = ${category.length}\n     service = ${service}\n     action = ${action}\n     tag = ${tag}\n     position = ${position}\n     type = ${type}\n     L1noDecryptPos = ${L1noDecryptPos}\n     L1decryptPos = ${L1decryptPos}\n`;

    if (secDecryptDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Log-forwarding objects *** L2 " or " will not work if they don't use spaces
exports.scoringLogForwarding = function(trafficName, trafficFilter, trafficPanorama, threatName, threatFilter, threatPanorama, threatSyslog, dataName, dataFilter, dataPanorama, urlName, urlFilter, urlPanorama, wildfireName, wildfireFilter, wildfirePanorama, profileName, fwName) {

    let [trafficComplete, threatComplete, dataComplete, urlComplete, wildfireComplete] = [false, false, false, false, false];

    if (logForwardingDebug == true) {
        debugOuptut = `???????????????????????? Log Forwarding ????????????????????????\n`; 
        debugOuptut += `scoringLogForwarding\n`;
        debugOuptut += `name = ${profileName}\n`;
        debugOuptut += `trafficName, trafficFilter, trafficPanorama\n`;
        debugOuptut += trafficName, trafficFilter, trafficPanorama;
        debugOuptut += `\nthreatName, threatFilter, threatPanorama, threatSyslog\n`;
        debugOuptut += threatName, threatFilter, threatPanorama, threatSyslog;
        debugOuptut += `\ndataName, dataFilter, dataPanorama\n`;
        debugOuptut += dataName, dataFilter, dataPanorama;
        debugOuptut += `\nurlName, urlFilter, urlPanorama\n`;
        debugOuptut += urlName, urlFilter, urlPanorama;
        debugOuptut += `\nwildfireName, wildfireFilter, wildfirePanorama\n`;
        debugOuptut += wildfireName, wildfireFilter, wildfirePanorama;
    }

    for (let arr in trafficFilter)
        if (trafficFilter[arr] === 'All Logs' && trafficPanorama[arr] === true)
            trafficComplete = true;
    for (let arr in threatFilter) {
        if (threatFilter[arr] === 'All Logs' && threatPanorama[arr] === true) {
            threatComplete = true;
        } else if (threatFilter[arr] === '(severity geq medium)') {
            L2LogFwdObjectComplete = true;
            threatComplete = true;
        } else if (threatFilter[arr].includes('severity') && threatFilter[arr].includes('or')) { // L2 log FWD check
            let severityCheck = threatFilter[arr].split(" or ");
            if (severityCheck.length === 3 && severityCheck.includes('(severity eq critical)') && severityCheck.includes('(severity eq high)') && severityCheck.includes('(severity eq medium)')) {
                if (threatSyslog[arr] !== '') {
                    debugOuptut += `\nsyslog contains something\n`;
                    L2LogFwdObjectComplete = true;
                    threatComplete = true;
                }
            }
            debugOuptut += `severityCheck = `;
            debugOuptut += severityCheck;
        }
    }

    for (let arr in dataFilter)
        if (dataFilter[arr] === 'All Logs' && dataPanorama[arr] === true)
            dataComplete = true;
    for (let arr in urlFilter)
        if (urlFilter[arr] === 'All Logs' && urlPanorama[arr] === true)
            urlComplete = true;
    for (let arr in wildfireFilter)
        if (wildfireFilter[arr] === 'All Logs' && wildfirePanorama[arr] === true)
            wildfireComplete = true;

    if (logForwardingDebug) debugOuptut += `\ntrafficComplete = ${trafficComplete}, threatComplete = ${threatComplete}, dataComplete = ${dataComplete}, urlComplete = ${urlComplete},   wildfireComplete = ${wildfireComplete}, threat syslog = ${threatSyslog}\n`;

    if (trafficComplete && threatComplete && dataComplete && urlComplete && wildfireComplete) {
        L1LogFwdObjectName = profileName;
        L1LogFwdObjectComplete = true;
    }

    if (L1LogFwdObjectComplete && L2LogFwdObjectComplete && profileName === 'default')
        L3LogFwdObjectComplete = true;

    debugOuptut += `L3LogFwdObjectComplete = ${L3LogFwdObjectComplete}\n`;

    if (logForwardingDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//App-filter objects
exports.scoringAppFilters = function(appFiltersArr, fwName) {

    let [filterName, subCategory, risk] = [appFiltersArr[0], appFiltersArr[1], appFiltersArr[2]];

    //L3 - App-ID app-filters
    if (subCategory.length === 1 && subCategory[0] === 'encrypted-tunnel' && risk.length == 2 && risk.includes('4') && risk.includes(`5`))
        L3AppIDFilterObjectName = filterName;

    if (appFilterDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Zones
exports.scoringZones = function(name, type, interface, userIDEnable, zonePP, packetBuff, fwName) {
    // var interface is an array

    if (debug_zones == true) {
        debugOuptut = `???????? Zones ????????\n`;
        debugOuptut += `name = ${name}\ntype = ${type}\nuserIDEnable = ${userIDEnable}\ninterface = ${interface}\npacketBuff = ${zonePP}\nzonePP = ${packetBuff}\n`;
    }   

    if (name === 'internal' && type === 'layer3') {
        if (debug_zones == true) debugOuptut += `internal zone correct\n`;
        L1ZoneInternalObjectComplete = true;
        if (userIDEnable)
            L1UserIDInternalObjectComplete = true;
        if (interface.length === 1 && interface[0] === 'ethernet1/2')
            L1InternalInterfaceObjectComplete = true;
        if (packetBuff === 'yes')
            L1ZonePacketBuffObjectComplete = true;
    }

    if (name === 'external' && type === 'layer3') {
        if (debug_zones == true) debugOuptut += `external zone correct\n`;
        L1ZoneExternalObjectComplete = true;
        if (interface.length === 1 && interface[0] === 'ethernet1/1')
            L1ExternalInterfaceObjectComplete = true;
        if (zonePP == L2ZoneProfileObjectName)
            L2ZonePPObjectComplete = true;
    }

    if (name === 'vpn' && type === 'layer3') {
        if (debug_zones == true) debugOuptut += `vpn zone correct\n`;
        L1ZoneVPNObjectComplete = true;
        if (userIDEnable)
            L1UserIDVPNObjectComplete = true;
    }

    // Zone L1 checking
    if (L1ZoneVPNObjectComplete && L1ZoneExternalObjectComplete && L1ZoneInternalObjectComplete)
        L1ZoneObjectComplete = true;

    // User-ID L1 checking
    if (L1UserIDInternalObjectComplete && L1UserIDVPNObjectComplete)
        L1UserIDZoneObjectComplete = true;

    // Interface L1 checking
    if (L1InternalInterfaceObjectComplete && L1ExternalInterfaceObjectComplete && L1VirtualRouterObjectComplete && L1physicalInterfaceObjectComplete)
        L1InterfaceObjectComplete = true;

    if (debug_zones == true) {
        debugOuptut += `???????????????????????????????????\n`;
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
    }
}

//User-ID
exports.scoringUserID = function(ignoreUser, fwName) {
    if (ignoreUser === 'acme.local\\svc_account')
        L2UserIDIgnoreObjectComplete = true;
}

//Schedule Objects
exports.scoringSchedule = function(schedRecur, recurDaily, recur, schedName) {
    // schedRecur true = recurring and false = non-recurring
    // recurDaily true = daily recurrence and false = weekly recurrance
    // recur is the time or date of recurrence

    if (schedObjDebug == true) {
        debugOuptut = `???????? Schedule Object ????????\n`;
        debugOuptut += `schedRecur = ${schedRecur}\nrecurDaily = ${recurDaily}\nrecur = ${recur}\nschedName = ${schedName}\n`;
    }    

    if (schedRecur && recurDaily) {
        if (recur === '11:30-13:00') {
            L2SchedFacebookObjectName = schedName;
        }
    }

    if (schedObjDebug == true) {
        debugOuptut += `???????????????????????????????????\n`;
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
    }

}

//Address Objects - IP Netmask &&&&& NEEDS TO BE CHANGED TO HANDLE MULTIPLE TYPES
exports.scoringIPNetmask = function(ipNetmask, addressObjectName) {
    if (IPNetmaskObjectDebug) {
        debugOuptut = `???????? IP Netmask Object ????????\n`;
        debugOuptut += `ipNetmask = `;
        debugOuptut += ipNetmask;
        debugOuptut += `addressObjectName = `;
        debugOuptut += addressObjectName;
    }    

    // L2 - Block sinkhole IP
    if (ipNetmask === '72.5.65.111' || ipNetmask === '72.5.65.111/32' || ipNetmask === 'sinkhole.paloaltonetworks.com') {
        L2BlockSinkholeIPObjectName = addressObjectName;
        if (IPNetmaskObjectDebug) {
            debugOuptut += `\nSinkhole address successfully configured.\n`;
            debugOuptut += `L2BlockSinkholeIPObjectName = ${L2BlockSinkholeIPObjectName}\n`;
        }
    }

    // L2 - DoS object
    if (ipNetmask === '56.2.217.65' || ipNetmask === '56.2.217.65/32') {
        L2DosObjectName = addressObjectName;
        if (IPNetmaskObjectDebug) {
            debugOuptut += `\nDoS address successfully configured.\n`;
            debugOuptut += `L2DosObjectName = ${L2DosObjectName}\n`;
        }
    }

    if (IPNetmaskObjectDebug) {
        debugOuptut += `???????????????????????????????????\n`;
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
    }
}

//Shared Config - Override Apps
exports.scoringOverrideApps = function(appName, appRisk, fwName) {

    if (appName === 'ssl' && appRisk == '3')
        L3AppIDSSLObjectComplete = true;

    if (appName === 'ssh' && appRisk === '3')
        L3AppIDSSHObjectComplete = true;

    if (overrideAppDebug == true)
        debugOuptut = `\nappName = ${appName}, appRisk = ${appRisk}\nL3AppIDSSLObjectComplete = ${L3AppIDSSLObjectComplete}\nL3AppIDSSHObjectComplete = ${L3AppIDSSHObjectComplete}`;

    if (overrideAppDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Shared Config - Admin Roles
exports.scoringAdminRoles = function(adminRole, fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);
    let adminRoleName = adminRole.entry['$'].name;

    let webUI = false;
    let api = false;
    let cli = true; // this shoudn't exist, if it does it means it is configured, which is wrong.
    let x; // just used as a shortener below

    if (scoringAdminRoleDebug) {
        debugOuptut =   `????????????????????????????????????????\n`; 
        debugOuptut += `          Device > Admin Roles\n`;
        debugOuptut += `?????????????????????????????????????????\n`;
        debugOuptut += `Checking if L3SharedConfigAdminRoleComplete. \n`;
    }

    if (typeof(adminRole.entry.role) !== 'undefined') {
        if (scoringAdminRoleDebug) {
            debugOuptut += `\nadminRole.entry.role\n`;
            debugOuptut += adminRole.entry.role;
            debugOuptut += `\n`;
        }
        if (typeof(adminRole.entry.role.device) !== 'undefined')
            // at this level, if you remove all Web UI, first element in array is an empty string. Otherwise an object.
            debugOuptut += typeof(adminRole.entry.role.device);
        for (let obj in adminRole.entry.role.device) { // there can be 3 things coming back: webui, xmlapi and cli
            if (scoringAdminRoleDebug) debugOuptut += `\nobj = ${obj}\n`;

            if (obj === 'webui' && Object.prototype.toString.call(adminRole.entry.role.device[obj]) === '[object String]' && webUI !== true) {
                webUI = true; // thing should be a string
                if (scoringAdminRoleDebug) debugOuptut += `webui is a string (correct)\n`;
            }
            if (obj === 'xmlapi' && Object.prototype.toString.call(adminRole.entry.role.device[obj]) === '[object Object]') {
                api = true; // thing should be a string
                if (scoringAdminRoleDebug) debugOuptut += `xmlapi is an object (correct)\n`;
                x = adminRole.entry.role.device[obj];
                if (x.report === 'enable' && x.log === 'enable' && x.op === 'enable' && x['user-id'] === 'enable')
                    if (typeof(x.config) === 'undefined' && typeof(x.commit) === 'undefined' && typeof(x.import) === 'undefined' && typeof(x.export) === 'undefined') {
                        L3SharedConfigCorrectRoleName = adminRoleName; // set this to the correct name
                        if (scoringAdminRoleDebug) debugOuptut += `L3SharedConfigCorrectRoleName = ${L3SharedConfigCorrectRoleName}\n`;
                    }
            }
            if (obj === 'cli' && Object.prototype.toString.call(adminRole.entry.role.device[obj]) === '[object String]') {
                cli = false; // thing should be a string
                if (scoringAdminRoleDebug) debugOuptut += `cli is a string (wrong)\n`;
            }
        }
        if (cli)
            if (scoringAdminRoleDebug) debugOuptut += `cli does not exist (correct)\n`;

        // CLI should not be set (true) and webUI should be a string (not set, true) and L3SharedConfigCorrectRoleName should be set
        if (cli && webUI && L3SharedConfigCorrectRoleName === adminRoleName)
            L3SharedConfigAdminRoleComplete = true; // mark as complete and will be cross referenced to be attached to a proper account
        else 
            L3SharedConfigAdminRoleComplete = false;

        if (scoringAdminRoleDebug) debugOuptut += `cli = ${cli} & webUI = ${webUI} & L3SharedConfigAdminRoleComplete = ${L3SharedConfigAdminRoleComplete}\n`;

    }

    if (scoringAdminRoleDebug) {
        debugOuptut +=   `????????????????????????????????????????\n`; 
        debugOuptut += `END          Device > Admin Roles\n`;
        debugOuptut += `?????????????????????????????????????????\n`;
    }

    if (scoringAdminRoleDebug) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Shared Config - Custom Threat Sig
exports.processCustomThreatSig = function(threatAction, threatSigType, threatOrderFree, threatValue, threatContext, fwName) {

    if (customThreatDebug) {
        debugOuptut = `?????????????? Custom Threat Sig ?????????????\n`; 
        debugOuptut += `     threatAction = ${threatAction}\n     threatSigType = ${threatSigType}\n     threatOrderFree = ${threatOrderFree}\n     threatValue = ${threatValue}\n     threatContext = ${threatContext}\n`;
        //debugOuptut += `\n`; 
        debugOuptut += `????????????????????????????????????????????\n`;
    }

    if (threatAction === 'alert' && threatSigType === 'standard' && threatValue == 500 && threatContext === 'http-rsp-code') {
        debugOuptut += `     Custom threat configured correctly\n `;
        L3VulnerabilityCustomObjectComplete = true;
    }

    if (customThreatDebug) 
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Device Config -  Certs
exports.scoringCerts = function (certName, ca, privateKey, forwardTrustName, forwardUntrustName, fwName) {

    if (scoringCertsDebug) {
        debugOuptut = `?????????????? Device Certs ?????????????\n`; 
        debugOuptut += `     certName = ${certName}\n     ca = ${ca}\n     privateKey = ${privateKey}\n     forwardTrustName = ${forwardTrustName}\n     forwardUntrustName = ${forwardUntrustName}\n`;
        //debugOuptut += `\n`; 
        debugOuptut += `???????????????????????????????????????\n`;
    }

    // if forwardTrust is configured, it has to be a CA
    if (typeof(forwardTrustName) !== 'undefined' && typeof(forwardUntrustName) !== 'undefined')
        if (forwardTrustName !== '' && forwardUntrustName !== '')
            L1DecryptCertsObjectComplete = true;

    if (scoringCertsDebug) 
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Device Config - Custom Logos
exports.scoringCustomLogo = function(obj, fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);
    let score = 0;

    let [customLogoMainUI, customLogoLoginUI] = [obj[0], obj[1]];

    if (customLogoMainUI.length != 0)
        L1DeviceConfigCustomUIImgComplete = true;
    else 
        L1DeviceConfigCustomUIImgComplete = false;

    if (customLogoLoginUI.length != 0) 
        L1DeviceConfigCustomLoginImgComplete = true;
    else 
        L1DeviceConfigCustomLoginImgComplete = false;
}

//Device Config - CTD Queue exhaustion config
exports.scoringCTDConfig = function (obj, fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);
    let score = 0;

    let [tcpBypassExceedQ, udpBypassExceedQ, allowHTTPRange, appIDexceedQ, extendedCapture] = [obj[0], obj[1], obj[2], obj[3], obj[4]];

    if (tcpBypassExceedQ === 'no' && udpBypassExceedQ === 'no' && allowHTTPRange === 'no' && (appIDexceedQ === 'no' || appIDexceedQ == '')) {
        L1DeviceConfigCTDComplete = true;
        if (scoringCTDConfigDebug) debugOuptut = `L1DeviceConfigCTDComplete = ${L1DeviceConfigCTDComplete} (if)\n`;
    }
    else {
        L1DeviceConfigCTDComplete = false;
        if (scoringCTDConfigDebug) debugOuptut = `L1DeviceConfigCTDComplete = ${L1DeviceConfigCTDComplete} (else)\n`;
    }

    if (scoringCTDConfigDebug) debugOuptut += `tcpBypassExceedQ = ${tcpBypassExceedQ}, udpBypassExceedQ = ${udpBypassExceedQ}, allowHTTPRange = ${allowHTTPRange}, appIDexceedQ = ${appIDexceedQ}, extendedCapture = ${extendedCapture}\n`;

    if (extendedCapture == '10') {
        L3SpywareExtendedPCAPComplete = true;
        if (scoringCTDConfigDebug) debugOuptut = `L3SpywareExtendedPCAPComplete = ${L3SpywareExtendedPCAPComplete}\n`;
    } else {
        L3SpywareExtendedPCAPComplete = false;
        if (scoringCTDConfigDebug) debugOuptut = `L3SpywareExtendedPCAPComplete = ${L3SpywareExtendedPCAPComplete} (else)\n`;

    }

    if (scoringCTDConfigDebug == true) 
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Device Config - Banner Config Scoring
exports.scoringBannerConfig = function (obj, fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);
    let score = 0;

    // Just make sure it exists. Not checking what it is.
    if(obj.length != 0)
        L1DeviceConfigBannerComplete = true;
    else 
        L1DeviceConfigBannerComplete = false;

    if (scoringDeviceBannerDebug == true) 
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Device Config - WF Config Scoring
exports.scoringDeviceConfigWFConfig = function (arr, fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);
    let score = 0;

    let [pe, apk, pdf, msoffice, jar, flash, macosx, archive, linux, script] = [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9]];

    if (scoringWFConfigDebug == true) {
        debugOuptut = `??????????????? WF Config ??????????????\n`; 
        debugOuptut += `     pe = ${pe}\n     apk = ${apk}\n     pdf = ${pdf}\n     msoffice = ${msoffice}\n     jar = ${jar}\n     flash = ${flash}\n     macosx = ${macosx}\n     archive = ${archive}\n     linux = ${linux}\n     script = ${script}`;
        debugOuptut += `\n`; 
        debugOuptut += `???????????????????????????????????????\n`;
    }

    //********************************************************************************************
    // L1 - Max WF Sizes 
    //********************************************************************************************

    if(pe == 50 && apk == 50 && pdf == 51200 && msoffice == 51200 && jar == 20 && flash == 10 && macosx == 50 && archive == 50 && linux == 50 && script == 4096)
        L1DeviceConfigWildFireConfigComplete = true;
    else 
        L1DeviceConfigWildFireConfigComplete = false;

    if (scoringWFConfigDebug == true) debugOuptut += `???????????? WF Config End ?????????????\n\n`;

    if (scoringWFConfigDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Device Config - Forward decrypted content
exports.processForwardDecryptContent = function (decryptedContent, fwName) {
    if (decryptedContent === 'yes')
        L2DecryptForwardContentObjectComplete = true;
}

//Management Config - Password Complexity Scoring
exports.scoringMgmtPassComplexityConfig = function (obj, fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);
    let score = 0;

    let [enabled, blockUserInclusion, minLength, minUpper, minLower] = [obj[0], obj[1], obj[2], obj[3], obj[4]];

    if (scoringMgmtPassComplexDebug == true) {
        debugOuptut = `????????????? Pass Complexity ????????????\n`; 
        debugOuptut += `     enabled = ${enabled}\n     blockUserInclusion = ${blockUserInclusion}\n     minLength = ${minLength}\n     minUpper = ${minUpper}\n     minLower = ${minLower}\n}`; 
        debugOuptut += `???????????????????????????????????????\n`;
    }

    //********************************************************************************************
    // L1 - enabled = yes, block inclusion = yes, min length = 8, min upper = 1, min lower = 1 
    //********************************************************************************************

    if(enabled === 'yes' && blockUserInclusion === 'yes' && minLength == 8 && minUpper == 1 && minLower == 1)
        L1minimumPasswordComplixityComplete = true;
    else 
        L1minimumPasswordComplixityComplete = false;

    if (scoringMgmtPassComplexDebug == true) debugOuptut += `??????????? Pass Complexity End ????????????\n\n`;

    if (scoringMgmtPassComplexDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}
exports.scoringMgmtUsers = function (userConfig, fwName) {
    // this needs to be called after the admin roles for correlation. Shared config (roles) needs to be processed first.
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);
    let score = 0;

    if (mgmtUsersDebug) {
        debugOuptut =   `????????????????????????????????????????\n`; 
        debugOuptut += `          Device > Administrators\n`;
        debugOuptut += `?????????????????????????????????????????\n`;
        debugOuptut += `Checking if L3SharedConfigAdminRoleComplete. \n`;
    }

    if (mgmtUsersDebug) debugOuptut += `L3SharedConfigAdminRoleComplete = ${L3SharedConfigAdminRoleComplete}\n`;
    if (L3SharedConfigAdminRoleComplete) { // no point in processing users if the role is not config'd properly (pre-req)
        if (Object.prototype.toString.call(userConfig) === '[object Array]') {
            if (mgmtUsersDebug) debugOuptut += `userConfig is an Array\n`;
            for (let obj in userConfig) {
                if (typeof(userConfig[obj].permissions) !== 'undefined')
                    for (let obj2 in userConfig[obj].permissions) { // crazy hierarchy... should only be 1 thing in here
                        for (let prop in userConfig[obj].permissions[obj2]) { // should only be 1 thing in here... 
                            if (typeof(userConfig[obj].permissions[obj2][prop].profile) !== 'undefined')
                                if (userConfig[obj].permissions[obj2][prop].profile === L3SharedConfigCorrectRoleName) {
                                    L3SharedConfigCorrectUserWithRole = true;
                                }
                        }
                    }
            }
        } else {
            if (mgmtUsersDebug) debugOuptut += `userConfig is an Object\n`;
            console.log(Date(), userConfig);
            if (typeof(userConfig.permissions) !== 'undefined')
                for (let obj2 in userConfig.permissions) { // crazy hierarchy... should only be 1 thing in here
                    for (let prop in userConfig.permissions[obj2]) { // should only be 1 thing in here... 
                        if (typeof(userConfig.permissions[obj2][prop].profile) !== 'undefined')
                            if (userConfig.permissions[obj2][prop].profile === L3SharedConfigCorrectRoleName) {
                                L3SharedConfigCorrectUserWithRole = true;
                            }
                    }
                }

        }
    } else {
        L3SharedConfigCorrectUserWithRole = false;
    }

    if (mgmtUsersDebug == true) debugOuptut += `L3SharedConfigCorrectUserWithRole = ${L3SharedConfigCorrectUserWithRole}\n`;


    if (mgmtUsersDebug) {
        debugOuptut +=   `????????????????????????????????????????\n`; 
        debugOuptut +=  `END      Device > Administrators\n`;
        debugOuptut +=  `?????????????????????????????????????????\n`;
    }

    if (mgmtUsersDebug == true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Management Config - Dynamic Updates
exports.scoringDynamicUpdateConfig = function (obj, fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);
    let score = 0;

    let [threatInterval, threatInstall, avInterval, avInstall, wfInterval, wfInstall] = [obj[0], obj[1], obj[2], obj[3], obj[4], obj[5]];

    if (scoringDeviceDynamicUpdateDebug) {
        debugOuptut = `??????? Dynamic Update Complexity ??????\n`; 
        debugOuptut += `     threatInterval = ${threatInterval}\n     threatInstall = ${threatInstall}\n     avInterval = ${avInterval}\n     avInstall = ${avInstall}\n     wfInterval = ${wfInterval}\n     wfInstall = ${wfInstall}\n`; 
    }

    //********************************************************************************************
    // L1 - WildFire = "every minute" && DL/Install, Antivirus = hourly && DL/Install, 
    // Apps/Threats = hourly && DL/Install, Apps & Threats - New APP-ID Threshold (48 hrs)
    //********************************************************************************************

    if(threatInterval === 'every-30-mins' && threatInstall === 'download-and-install' && avInterval === 'hourly' && avInstall === 'download-and-install' && wfInterval === 'every-min' && wfInstall === 'download-and-install')
        L1DeviceConfigDynamicUpdateComplete = true;
    else 
        L1DeviceConfigDynamicUpdateComplete = false;

    if (scoringDeviceDynamicUpdateDebug == true) debugOuptut += `??????????? Dynamic Update End ????????????\n\n`;

    if (scoringDeviceDynamicUpdateDebug) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Profile - Antivirus Scoring
exports.scoringAntivirus = function (obj, avProfileName, fwName) {
    let ftp = obj.find(o => o['$'].name === 'ftp');
    let http = obj.find(o => o['$'].name === 'http');
    let http2 = obj.find(o => o['$'].name === 'http2');
    let imap = obj.find(o => o['$'].name === 'imap');
    let pop3 = obj.find(o => o['$'].name === 'pop3');
    let smb = obj.find(o => o['$'].name === 'smb');
    let smtp = obj.find(o => o['$'].name === 'smtp');

    if (virDebug) {
        debugOuptut = `?????????????? Antivirus Begin ??????????????\n`; 
        debugOuptut += JSON.stringify(obj);
        debugOuptut += `\n`;
        debugOuptut += `     Profile name: ${avProfileName}\n`;
        debugOuptut += `     L1aAVCorrectavname: ${L1aAVCorrectavname}\n`;
        debugOuptut += `     L1bAVCorrectavname: ${L1bAVCorrectavname}\n`;
        debugOuptut += `     L1AVprofileNameMatch: ${L1AVprofileNameMatch}\n`;
        debugOuptut += `???????????????????????????????????????\n`;
    }

    //********************************************************************************************
    // L1 - reset-both - all items for AV
    //********************************************************************************************
    if ((ftp.action === 'default' || ftp.action === 'reset-both') && (http.action === 'default' || http.action === 'reset-both') && (http2.action === 'default' || http2.action === 'reset-both') && (smb.action === 'default' || smb.action === 'reset-both') && (smtp.action === 'reset-both') && (pop3.action === 'reset-both') && (imap.action === 'reset-both')) {
        L1aAVCorrectavname = avProfileName;
    }  

    //********************************************************************************************
    // L1 - reset-both - all items for WF
    //********************************************************************************************
    if ((ftp['wildfire-action'] === 'default' || ftp['wildfire-action'] === 'reset-both') && (http['wildfire-action'] === 'default' || http['wildfire-action'] === 'reset-both') && (http2['wildfire-action'] === 'default' || http2['wildfire-action'] === 'reset-both') && (smb['wildfire-action'] === 'default' || smb['wildfire-action'] === 'reset-both') && (smtp['wildfire-action'] === 'reset-both') && (pop3['wildfire-action'] === 'reset-both') && (imap['wildfire-action'] === 'reset-both'))
        L1bAVCorrectavname = avProfileName;

    if (typeof(L1aAVCorrectavname) !== 'undefined') 
        if (L1aAVCorrectavname === L1bAVCorrectavname) {
            L1AVprofileNameMatch = true;
            L1AVCorrectname = L1aAVCorrectavname;
        }

    if (virDebug) {
        debugOuptut = `?????????????? Antivirus End ??????????????\n`; 
        debugOuptut += `     Profile name: ${avProfileName}\n`;
        debugOuptut += `     L1aAVCorrectavname: ${L1aAVCorrectavname}\n`;
        debugOuptut += `     L1bAVCorrectavname: ${L1bAVCorrectavname}\n`;
        debugOuptut += `     L1AVprofileNameMatch: ${L1AVprofileNameMatch}\n`;
        debugOuptut += `???????????????????????????????????????\n`;
    }

    if (virDebug) debugOuptut += `??????????????? AV End ????????????????\n\n`;

    if (virDebug) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Profile - File Blocking Scoring
exports.scoringFileBlocking = function (obj, fbProfileName, fwName) {
    let [name, apps, fileTypes, direction, action] = [obj[0], obj[1], obj[2], obj[3], obj[4]];

    if (scoringFBDebug == true) {
        debugOuptut = `???????????? File-Blocking ????????????\n`;  
        debugOuptut += JSON.stringify(obj);
        debugOuptut += `\n`;
        debugOuptut += `     Profile name: ${fbProfileName}\n`;
        debugOuptut += `     name, apps, fileTypes, direction, action: ${name, apps, fileTypes, direction, action}\n`;
        debugOuptut += `     apps array length: ${apps.length}\n`;
        debugOuptut += `???????????????????????????????????????\n\n`;
        debugOuptut += `     ?????? Variable States ?????\n`;
        debugOuptut += `     L1FBname: ${L1FBname}\n`;
        debugOuptut += `     L2FBmultiencodefbname: ${L2FBmultiencodefbname}\n`;
        debugOuptut += `     L3FBcorrectfbname set to ${L3FBcorrectfbname}\n`;
        debugOuptut += `     L2FBprofileNameMatch set to ${L2FBprofileNameMatch}\n`;
        debugOuptut += `     ????????????????????????????\n`;

    }

    //********************************************************************************************
    //Scenario L1: fb alert on all file types
    //********************************************************************************************f
    for (var arr in apps) {
        if (apps[arr] === 'any' && fileTypes[arr] === 'any' && direction === 'both' && action === 'alert') { // L1FB
            L1FBname = fbProfileName;
            if (scoringFBDebug) {
                debugOuptut += `     ?????? Alert Profile Section ?????\n`;
                debugOuptut += `     Alert all on profile\n`;
                debugOuptut += `     apps[arr] = ${apps[arr]} : fileTypes[arr] = ${fileTypes[arr]} : direction = ${direction} : action = ${action}\n`;
                debugOuptut += `     L1FBname set to ${fbProfileName}\n\n`;
            }
        }

        if (apps[arr] === 'any' && fileTypes[arr] === 'Multi-Level-Encoding' && direction === 'both' && action === 'block') { // L2FB
            L2FBmultiencodefbname = fbProfileName; // record names so that L1 and L2 should match, which indicates they are in the same profile
            if (scoringFBDebug) {
                debugOuptut += `     ?????? Multi-Level Section ?????\n`;
                debugOuptut += `     Multi-level on profile\n`;
                debugOuptut += `     apps[arr] = ${apps[arr]} : fileTypes[arr] = ${fileTypes[arr]} : direction = ${direction} : action = ${action}\n`;
                debugOuptut += `     L2FBmultiencodefbname set to ${fbProfileName}\n`;
            }
        }

        if (scoringFBDebug) debugOuptut =+ `     ?????? Check L1 / L2 names ?????\n`;
        if (L1FBname === L2FBmultiencodefbname && typeof(L2FBmultiencodefbname) !== 'undefined') {
            L2FBprofileNameMatch = true;
            if (scoringFBDebug) debugOuptut += `     L2FBprofileNameMatch set to TRUE\n`;
        } else {
            L2FBprofileNameMatch = false;
            if (scoringFBDebug) debugOuptut += `     L2FBprofileNameMatch set to FALSE\n`;
        }

        if (apps[arr] === 'any' && fileTypes[arr] === 'any' && direction === 'both' && action === 'continue') {
            L3FBcorrectfbname = fbProfileName; // record profile name, should not be used in L1 or L2 profile
            if (scoringFBDebug) {
                debugOuptut += `     ?????? Continue Section?????\n`;
                debugOuptut += `     Continue on profile\n`;
                debugOuptut += `     apps[arr] = ${apps[arr]} : fileTypes[arr] = ${fileTypes[arr]} : direction = ${direction} : action = ${action}\n`;
                debugOuptut += `     L3FBcorrectfbname set to ${fbProfileName}\n`;
            }
        }

        if (scoringFBDebug) {
            debugOuptut += `     ?????? Variable States ?????\n`;
            debugOuptut += `     L1FBname: ${L1FBname}\n`;
            debugOuptut += `     L2FBmultiencodefbname: ${L2FBmultiencodefbname}\n`;
            debugOuptut += `     L3FBcorrectfbname set to ${L3FBcorrectfbname}\n`;
            debugOuptut += `?????????? File Blocking End ??????????\n\n`;
        }
    }

    if (scoringFBDebug == true)
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Profile - URL Filtering Scoring
exports.scoringURLFiltering = function (urlProfile, urlProfileName, fwName) {

    if (scoringURLDebug == true) {
        debugOuptut = `???????????? URL Filtering ????????????\n`;  
        debugOuptut += JSON.stringify(urlProfile);
        debugOuptut += `\n`;
        debugOuptut += `     Profile name: ${urlProfileName}\n`;
        //debugOuptut += `     name, apps, fileTypes, direction, action: ${name, apps, fileTypes, direction, action}\n`;
        debugOuptut += `???????????????????????????????????????\n\n`;
        debugOuptut += `     ?????? Variable States ?????\n`;
        debugOuptut += `     L1aBlockURLCorrectname: ${L1aBlockURLCorrectname}\n`;
        debugOuptut += `     L1aAlertURLCorrectname: ${L1aAlertURLCorrectname}\n`;
        debugOuptut += `     L1aURLNameMatch set to ${L1aURLNameMatch}\n\n`;
        debugOuptut += `     L1bContinueURLCorrectname: ${L1bContinueURLCorrectname}\n`;
        debugOuptut += `     L1bAlertURLCorrectname: ${L1bAlertURLCorrectname}\n`;
        debugOuptut += `     L1bURLNameMatch set to ${L1bURLNameMatch}\n\n`;
        debugOuptut += `     L2URLCorrectname: ${L2URLCorrectname}\n`;
        debugOuptut += `     L2URLContainerComplete: ${L2URLContainerComplete}\n`;
        debugOuptut += `     ????????????????????????????\n`;
    }

    if(typeof(urlProfile.override) !== 'undefined') { // make sure entries exist first
        if (Object.prototype.toString.call(urlProfile.override.member) === '[object Array]') { // we need more than just 1 entry...
            debugOuptut += `     override array length: ${urlProfile.override.member.length}\n`;
            debugOuptut += `     URL override detected \n`;
            debugOuptut += (urlProfile.override.member);
            debugOuptut += `\n`;
        }
    }

    //********************************************************************************************
    //Scenario L1a: URL block on parked, malware, proxy-avoidance-and-anonymizers, command-and-control, unknown, copyright-infringement, phishing, dynamic-dns, extremism, hacking AND alert on the rest
    //********************************************************************************************

    //********************************************************************************************
    //Scenario L3: custom URL cateogry check for Block & Continue
    //********************************************************************************************

    if (scoringURLDebug == true) debugOuptut += `     L3URLCorrectname = ${L3URLCorrectname}\n`;

    if(L1aComplete !== true) {
        if(typeof(urlProfile.block) !== 'undefined') { // make sure entries exist first
            if (Object.prototype.toString.call(urlProfile.block.member) === '[object Array]') { // we need more than just 1 entry...
                if (scoringURLDebug == true) debugOuptut += `     urlblock array length: ${urlProfile.block.member.length}\n`;
                const L1aBlockedURL = urlProfile.block.member;
                if (L1aBlockedURL.length === 10 && L1aBlockedURL.includes('parked') && L1aBlockedURL.includes('malware') && L1aBlockedURL.includes('proxy-avoidance-and-anonymizers') && L1aBlockedURL.includes('command-and-control') && L1aBlockedURL.includes('unknown') && L1aBlockedURL.includes('copyright-infringement') && L1aBlockedURL.includes('phishing') && L1aBlockedURL.includes('dynamic-dns') && L1aBlockedURL.includes('extremism') && L1aBlockedURL.includes('hacking')) {
                    L1aBlockURLCorrectname = urlProfileName;
                    if (scoringURLDebug == true) debugOuptut += `block matches\n`;
                } else if (typeof(L3URLCorrectname !== 'undefined'))
                    if (L1aBlockedURL.length === 11 && L1aBlockedURL.includes('parked') && L1aBlockedURL.includes('malware') && L1aBlockedURL.includes('proxy-avoidance-and-anonymizers') && L1aBlockedURL.includes('command-and-control') && L1aBlockedURL.includes('unknown') && L1aBlockedURL.includes('copyright-infringement') && L1aBlockedURL.includes('phishing') && L1aBlockedURL.includes('dynamic-dns') && L1aBlockedURL.includes('extremism') && L1aBlockedURL.includes('hacking') && L1aBlockedURL.includes(`${L3URLCorrectname}`)) {
                        if (scoringURLDebug == true) debugOuptut += `    L3 - picked up that custom is blocked\n`;
                        L1aBlockURLCorrectname = urlProfileName;
                        L3URLCustomURLBlock = urlProfileName;
                    }
            }
        }

        if (typeof(urlProfile.alert) !== 'undefined') { // make sure entries exist first
            if (Object.prototype.toString.call(urlProfile.alert.member) === '[object Array]')  { // we need more than just 1 entry...
                if (scoringURLDebug == true) debugOuptut += `     urlalert array length: ${urlProfile.alert.member.length}\n`;
                const L1aAlertURL = urlProfile.alert.member;
                if (L1aAlertURL.length === 62 && (typeof(L1aBlockURLCorrectname) !== 'undefined' || typeof(L1aAlertURLCorrectname) !== 'undefined')) { // might need to be fixed in the future if custom categories are added
                    L1aAlertURLCorrectname = urlProfileName;
                    if (scoringURLDebug == true) debugOuptut += `Alert matches\n`;
                }
            }
        }

        // for L1a scenario (block malicious / alert on rest) & L1b (continue on malicious / alert on rest)
        if (L1aAlertURLCorrectname === L1aBlockURLCorrectname && typeof(L1aAlertURLCorrectname) !== 'undefined' ) {
            L1aURLNameMatch = true;
            L1aComplete = true;
            L1aCorrectname = L1aAlertURLCorrectname;
            if (scoringURLDebug == true) debugOuptut += `L1aURLNameMatch = ${L1aURLNameMatch}\n`;
        } else if (L1aAlertURLCorrectname !== L1aBlockURLCorrectname)
            L1aURLNameMatch = false;
    }

    // L2 Log container page
    if (L1aAlertURLCorrectname === L1aBlockURLCorrectname && L1aBlockURLCorrectname === urlProfileName) {
        if (typeof(urlProfile['log-container-page-only']) !== 'undefined')
            if (urlProfile['log-container-page-only'] === 'no') {
                L2URLContainerComplete = true; 
                L2URLCorrectname = urlProfileName;
            }
    } else if (L1aAlertURLCorrectname === L1aBlockURLCorrectname && L1aBlockURLCorrectname === urlProfileName) {
        if (typeof(urlProfile['log-container-page-only']) === 'undefined')
            if (urlProfile['log-container-page-only'] === 'yes') {
                L2URLContainerComplete = false; 
            }
    } else if (typeof(L2URLCorrectname) === 'undefined')
        L2URLContainerComplete = false;

    //********************************************************************************************
    //Scenario L1b: URL continue on parked, malware, proxy-avoidance-and-anonymizers, command-and-control, unknown, copyright-infringement, phishing, dynamic-dns, extremism, hacking AND alert on the rest
    //********************************************************************************************
    if(L1bComplete !== true) {
        if(typeof(urlProfile.continue) !== 'undefined') { // make sure entries exist first
            if (Object.prototype.toString.call(urlProfile.continue.member) === '[object Array]') { // we need more than just 1 entry...
                if (scoringURLDebug == true) debugOuptut += `     urlcontinue array length: ${urlProfile.continue.member.length}\n`;

                const L1bContinueURL = urlProfile.continue.member;
                if (L1bContinueURL.length === 10 && L1bContinueURL.includes('parked') && L1bContinueURL.includes('malware') && L1bContinueURL.includes('proxy-avoidance-and-anonymizers') && L1bContinueURL.includes('command-and-control') && L1bContinueURL.includes('unknown') && L1bContinueURL.includes('copyright-infringement') && L1bContinueURL.includes('phishing') && L1bContinueURL.includes('dynamic-dns') && L1bContinueURL.includes('extremism') && L1bContinueURL.includes('hacking')) {
                    L1bContinueURLCorrectname = urlProfileName;
                    if (scoringURLDebug == true) debugOuptut += `continue matches\n`;
                } else if (typeof(L3URLCorrectname !== 'undefined'))
                    if (L1bContinueURL.length === 11 && L1bContinueURL.includes('parked') && L1bContinueURL.includes('malware') && L1bContinueURL.includes('proxy-avoidance-and-anonymizers') && L1bContinueURL.includes('command-and-control') && L1bContinueURL.includes('unknown') && L1bContinueURL.includes('copyright-infringement') && L1bContinueURL.includes('phishing') && L1bContinueURL.includes('dynamic-dns') && L1bContinueURL.includes('extremism') && L1bContinueURL.includes('hacking') && L1bContinueURL.includes(`${L3URLCorrectname}`)) {
                        if (scoringURLDebug == true) debugOuptut += `     L3 picked up that custom is on continue\n`;
                        L1bContinueURLCorrectname = urlProfileName;
                        L3URLCustomURLContinue = urlProfileName;
                    }
            }
        }

        if (typeof(urlProfile.alert) !== 'undefined') { // make sure entries exist first
            if (Object.prototype.toString.call(urlProfile.alert.member) === '[object Array]')  { // we need more than just 1 entry...
                if (scoringURLDebug == true)debugOuptut += `     urlalert array length: ${urlProfile.alert.member.length}\n`;

                const L1bAlertURL = urlProfile.alert.member
                if (L1bAlertURL.length === 62 && (typeof(L1aBlockURLCorrectname) !== 'undefined' || typeof(L1bContinueURLCorrectname) !== 'undefined')) { // might need to be fixed in the future if custom categories are added
                    L1bAlertURLCorrectname = urlProfileName;
                    if (scoringURLDebug == true) debugOuptut += `Alert matches`;
                }
            }
        }    

        if (L1bAlertURLCorrectname === L1bContinueURLCorrectname && typeof(L1bAlertURLCorrectname) !== 'undefined') {
            L1bURLNameMatch = true;
            L1bComplete = true;
            L1bCorrectname = L1bAlertURLCorrectname;
            if (scoringURLDebug == true) debugOuptut += `     L1bURLNameMatch = ${L1bURLNameMatch}`; 
        } else if (L1bAlertURLCorrectname !== L1bContinueURLCorrectname)
            L1bURLNameMatch = false;
    }

    if (typeof(L3URLCorrectname) !== 'undefined') {
        if (L3URLCustomURLContinue === L1bContinueURLCorrectname && L1aBlockURLCorrectname === L3URLCustomURLBlock) {
            L3URLCustomURLComplete = true;
        } else if (L3URLCustomURLContinue !== L1bContinueURLCorrectname || L1aBlockURLCorrectname !== L3URLCustomURLBlock)
            L3URLCustomURLComplete = false;
    }

    if (scoringURLDebug == true) {
        debugOuptut += `\n     ?????? Variable States ?????\n`;
        debugOuptut += `     L1aBlockURLCorrectname: ${L1aBlockURLCorrectname}\n`;
        debugOuptut += `     L1aAlertURLCorrectname: ${L1aAlertURLCorrectname}\n`;
        debugOuptut += `     L1aURLNameMatch set to ${L1aURLNameMatch}\n\n`;
        debugOuptut += `     L1bContinueURLCorrectname: ${L1bContinueURLCorrectname}\n`;
        debugOuptut += `     L1bAlertURLCorrectname: ${L1bAlertURLCorrectname}\n`;
        debugOuptut += `     L1bURLNameMatch set to ${L1bURLNameMatch}\n\n`;
        debugOuptut += `     L2URLContainerComplete: ${L2URLContainerComplete}\n`;
        debugOuptut += `     L2URLCorrectname: ${L2URLCorrectname}\n`;
        debugOuptut += `     ????????????????????????????\n`;
    }

    if (scoringURLDebug == true)
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Profile - Custom URL Categories Scoring
exports.scoringCustomURLFiltering = function (customUrlProfile, customUrlProfileName, fwName) {
    let name;
    let type;

    if (scoringCustomURLDebug == true) {
        debugOuptut = `???????????? Custom URL Filtering ????????????\n`;  
        debugOuptut += JSON.stringify(customUrlProfile);
        debugOuptut += `\n`;
        debugOuptut += `     Profile name: ${customUrlProfileName}\n`;
        debugOuptut += `???????????????????????????????????????\n\n`; 
        debugOuptut += `     ?????? Variable States ?????\n`;
        debugOuptut += `     L3URLCorrectname: ${L3URLCorrectname}\n`;
        debugOuptut += `     ????????????????????????????\n`; 
    }

    //********************************************************************************************
    //Scenario L3: URL custom category: high-risk & CDN, then set to block in the block profile / continue in the continue profile
    //********************************************************************************************

    if (typeof(customUrlProfile['$']) !== 'undefined') {
        name = customUrlProfile['$'].name
        if (typeof(customUrlProfile.type) !== 'undefined' && typeof(customUrlProfile.list) !== 'undefined') {
            type = customUrlProfile.type;
            if (typeof(customUrlProfile.list.member) !== 'undefined') {
                if (Object.prototype.toString.call(customUrlProfile.list.member) === '[object Array]')
                    if (customUrlProfile.list.member.includes('high-risk') && customUrlProfile.list.member.includes('content-delivery-networks'))
                        L3URLCorrectname = customUrlProfileName;
            }
        }

    } // the rest of this needs to be checked in the URL category section to make sure it is applied properly to the profiles

    if (scoringCustomURLDebug == true) {
        debugOuptut += `\n     ?????? Variable States ?????\n`;
        debugOuptut += `     L3URLCorrectname: ${L3URLCorrectname}\n`;
        debugOuptut += `     ????????????????????????????\n`;
    }

    // debug write to file
    if (scoringCustomURLDebug == true)
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Profile - Custom Data Filtering Scoring
exports.scoringCustomDataFiltering = function (customDataProfile, customDataProfileName, fwName) {
    let [name, type, regExfileType, regexParam, predefinedCC, predefinedFileType] = ['', '', '', '', '', ''];

    if (scoringCustomDataDebug == true) {
        debugOuptut = `???????????? Custom Data Filtering ???????????\n`;  
        debugOuptut += JSON.stringify(customDataProfile);
        debugOuptut += `\n`;
        debugOuptut += `     Profile name: ${customDataProfileName}\n`;
        debugOuptut += `???????????????????????????????????????\n\n`; 
        debugOuptut += `     ?????? Variable States ?????\n`;
        debugOuptut += `     L2DataAlertCorrectName: ${L2DataAlertCorrectName}\n`;
        debugOuptut += `     L2DataAlertObjectiveComplete: ${L2DataAlertObjectiveComplete}\n`;
        debugOuptut += `     L3DataPatternCorrectName: ${L3DataPatternCorrectName}\n`;
        debugOuptut += `     ????????????????????????????\n`; 
    }

    //*******************************************************************************************
    //Scenario L2: Data custom: Alert on Confidential (regex type) && CC set to block
    //********************************************************************************************

    if (typeof(customDataProfile['$']) !== 'undefined') {
        name = customDataProfile['$'].name
        if (scoringCustomDataDebug == true) debugOuptut += `name = ${name}\n`;
        if (typeof(customDataProfile['pattern-type']) !== 'undefined') {
            type = customDataProfile['pattern-type']; // just a shortener
            if (typeof(type.regex) !== 'undefined') 
                if (typeof(type.regex.pattern) !== 'undefined') 
                    if (typeof(type.regex.pattern.entry) !== 'undefined') 
                        if (typeof(type.regex.pattern.entry['file-type']) !== 'undefined') {
                            if (typeof(type.regex.pattern.entry['file-type'].member) !== 'undefined') {
                                regExfileType = type.regex.pattern.entry['file-type'].member;
                                if (scoringCustomDataDebug == true) debugOuptut += `file-type.member = ${regExfileType}\n`;
                            }
                            if (typeof(type.regex.pattern.entry.regex) !== 'undefined') {
                                regexParam = type.regex.pattern.entry.regex;
                                if (scoringCustomDataDebug == true) debugOuptut += `regex = ${regexParam}\n`;
                            }
                        }
            if (typeof(type.predefined) !== 'undefined') 
                if (typeof(type.predefined.pattern) !== 'undefined')
                    if (typeof(type.predefined.pattern.entry) !== 'undefined') {
                        predefinedCC = type.predefined.pattern.entry['$'].name;
                        if (typeof(type.predefined.pattern.entry['file-type']) !== 'undefined') 
                            if (typeof(type.predefined.pattern.entry['file-type'].member) !== 'undefined')
                                predefinedFileType = type.predefined.pattern.entry['file-type'].member;
                    }
        }
    } // the rest of this needs to be checked in the URL category section to make sure it is applied properly to the profiles

    if (regExfileType === 'any' && regexParam === 'Confidential') {
        L2DataAlertCorrectName = name;
    } else if (regExfileType === 'any' && regexParam === 'SSBsb3ZlIFBhbG8gQWx0byBOZXR3b3JrcyE=') {
        L3DataPatternCorrectName = name;
    }

    if (predefinedFileType === 'any' && predefinedCC === 'credit-card-numbers') {
        L2DataCCCorrectName = name;
    }

    if (scoringCustomDataDebug == true) {
        debugOuptut += `\n     ?????? Variable States ?????\n`;
        debugOuptut += `     L2DataAlertCorrectName: ${L2DataAlertCorrectName}\n`;
        debugOuptut += `     L2DataAlertObjectiveComplete: ${L2DataAlertObjectiveComplete}\n`;
        debugOuptut += `     L2DataCCCorrectName: ${L2DataCCCorrectName}\n`;
        debugOuptut += `     L2DataCCComplete: ${L2DataCCComplete}\n`;
        debugOuptut += `     L3DataPatternCorrectName: ${L3DataPatternCorrectName}\n`;
        debugOuptut += `     ????????????????????????????\n`;
    }

    if (scoringCustomDataDebug == true)
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Profile - Data Filtering Scoring
exports.scoringDataFiltering = function (arr, datafilteringProfileName, fwName) {
    let [appMemberArr, direction, fileTypeMemberArr, alertThresh, blockThresh, dataObj, logSev] = [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6]];

    if (scoringDataDebug == true) {
        debugOuptut = `???????????? Data Filtering ???????????\n`;  
        debugOuptut += `app = ${appMemberArr}, direction = ${direction}, fileTypeMemberArr = ${fileTypeMemberArr}\n`;
        debugOuptut += `alertThresh = ${alertThresh}, blockThresh = ${blockThresh}, dataObj = ${dataObj}, logSev = ${logSev}\n`;
        debugOuptut += `     Profile name: ${datafilteringProfileName}\n`;
        debugOuptut += `???????????????????????????????????????\n\n`; 
        debugOuptut += `     ?????? Pre Variable States ?????\n`;
        debugOuptut += `     datafilteringProfileName: ${datafilteringProfileName}\n`;
        debugOuptut += `     L2DataAlertObjectiveComplete: ${L2DataAlertObjectiveComplete}\n`;
        debugOuptut += `     L2DataCCCorrectName: ${L2DataCCCorrectName}\n`;
        debugOuptut += `     L2DataCCObjectiveComplete: ${L2DataCCObjectiveComplete}\n`;
        debugOuptut += `     L3DataPatternCorrectProfileName: ${L3DataPatternCorrectProfileName}\n`;
        debugOuptut += `     L3DataPatternComplete: ${L3DataPatternComplete}\n`;
        debugOuptut += `     ????????????????????????????????\n`; 
    }

    //*******************************************************************************************
    //Scenario L2: Data profile: Alert on Confidential (regex type) && CC set to block
    //*******************************************************************************************

    if (dataObj === L2DataAlertCorrectName) { // make sure the custom data object is assigned to this profile
        if (appMemberArr.length == 1 && appMemberArr[0] == 'any' && fileTypeMemberArr.length == 1 && fileTypeMemberArr[0] == 'any' && direction === 'upload' && alertThresh == 1 && blockThresh == 5 && logSev === 'medium') {
            if (scoringDataDebug == true) debugOuptut += `Correct Data Alert Profile Match\n`; 
            L2DataAlertObjectiveComplete = true;
            L2DataCorrectAlertProfileName = datafilteringProfileName;
        }
        else
            L2DataAlertObjectiveComplete = false;
    } else if (typeof(L2DataAlertCorrectName) === 'undefined')
        L2DataAlertObjectiveComplete = false;

    if (dataObj === L2DataCCCorrectName) { // make sure the custom data object is assigned to this profile
        if (appMemberArr.length == 1 && appMemberArr[0] == 'any' && fileTypeMemberArr.length == 1 && fileTypeMemberArr[0] == 'any' && direction === 'both' && alertThresh == 1 && blockThresh == 5 && logSev === 'high') {
            L2DataCorrectCCProfileName = datafilteringProfileName;
            if (scoringDataDebug == true) debugOuptut += `Correct Data CC Profile Match\n`; 
            if (L2DataCorrectCCProfileName === L2DataCorrectAlertProfileName) // make sure Confidential is in the same profile name
                L2DataCCObjectiveComplete = true;
        }
        else if (L2DataCorrectCCProfileName === datafilteringProfileName)
            L2DataCCObjectiveComplete = false;
    } else if (typeof(L2DataCCCorrectName) === 'undefined')
        L2DataCCObjectiveComplete = false;

    if (dataObj === L3DataPatternCorrectName) { // make sure the custom data object is assigned to this profile
        if (appMemberArr.length == 1 && appMemberArr[0] == 'any' && fileTypeMemberArr.length == 1 && fileTypeMemberArr[0] == 'any' && direction === 'both' && alertThresh == 1 && blockThresh == 1 && logSev === 'critical') {
            if (scoringDataDebug == true) debugOuptut += `Correct Data DB Profile Match\n`; 
            L3DataPatternCorrectProfileName = datafilteringProfileName;
            if (L2DataCCObjectiveComplete && L2DataCorrectCCProfileName === L3DataPatternCorrectProfileName) {
                L3DataPatternComplete = true;
            }
        }
        else if (L3DataPatternCorrectProfileName === datafilteringProfileName)
            L3DataPatternComplete = false;
    } else if (typeof(L3DataPatternCorrectName) === 'undefined')
        L3DataPatternComplete = false;

    if (scoringDataDebug == true) {
        debugOuptut += `\n     ?????? Post Variable States ?????\n`;
        debugOuptut += `     datafilteringProfileName: ${datafilteringProfileName}\n`;
        debugOuptut += `     L2DataAlertObjectiveComplete: ${L2DataAlertObjectiveComplete}\n`;
        debugOuptut += `     L2DataCCCorrectName: ${L2DataCCCorrectName}\n`;
        debugOuptut += `     L2DataCCObjectiveComplete: ${L2DataCCObjectiveComplete}\n`;
        debugOuptut += `     L3DataPatternCorrectProfileName: ${L3DataPatternCorrectProfileName}\n`;
        debugOuptut += `     L3DataPatternComplete: ${L3DataPatternComplete}\n`;
        debugOuptut += `     ????????????????????????????????\n`; 
    }

    if (scoringDataDebug == true)
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Profile - Spyware Scoring
exports.scoringSpyware = function (obj, spywareProfileName, fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);

    const [name, action, category, severity] = [obj[0], obj[1], obj[2], obj[3]];

    if (scoringSpyDebug == true) {
        debugOuptut = `??????????????? Spyware ???????????????\n`;  
        debugOuptut += JSON.stringify(obj);
        debugOuptut += `\n`;
        debugOuptut += `     Profile name: ${spywareProfileName}\n`;
        debugOuptut += `     name, action, category, severity: ${name, action, category, severity}\n`;
        debugOuptut += `     severity array length: ${severity.length}\n`;
        debugOuptut += `???????????????????????????????????????\n`;
        debugOuptut += `     ?????? Variable States ?????\n`;
        debugOuptut += `     L1SpywarecorrectSpyname: ${L1SpywarecorrectSpyname}\n`;
        debugOuptut += `     L1SpywareCorrectResetClientName: ${L1SpywareCorrectResetClientName}\n`;
        debugOuptut += `     L1SpywareprofileNameMatch set to ${L1SpywareprofileNameMatch}\n`;
        debugOuptut += `     L2FBprofileNameMatch set to ${L2FBprofileNameMatch}\n`;
        debugOuptut += `     ????????????????????????????\n`;
    }

    //********************************************************************************************
    //Scenario L1: spyware reset-client low-crit; alert on inf
    //********************************************************************************************
    // the order of the severity array may not be in order, so we need to check includes and that the length of array is specific to the number of answers we need.
    if (scoringSpyDebug == true) {
        debugOuptut += `     ?????? Reset Client ?????\n`;
        debugOuptut += `     action = ${action} : category = ${category} : severity = ${severity}\n`;
    }
    if ((action === 'reset-client' || action === 'drop') && category === 'any' && severity.length === 4 && severity.includes('critical') && severity.includes('high') && severity.includes('medium') && severity.includes('low')) {
        if (scoringSpyDebug == true) {
            debugOuptut += `     Spyware Reset Client on profile}\n`;
            debugOuptut += `     L1SpywarecorrectSpyname: ${spywareProfileName}\n`;
        }
        L1SpywarecorrectSpyname = spywareProfileName; // need to make sure each rule is in the same profile for matching
    } else if (action === 'default' && category === 'any' && severity[0] == 'informational') { // L2FB
        L1SpywareCorrectResetClientName = spywareProfileName; // record names so that L1 and L2 should match, which indicates they are in the same profile
        if (scoringSpyDebug == true) {
            debugOuptut += `     Spyware Default / Inf on profile}\n`;
            debugOuptut += `     L1SpywareCorrectResetClientName: ${spywareProfileName}\n`;
        }
    } 

    if (typeof(L1SpywareCorrectResetClientName) !== 'undefined' && (L1SpywareCorrectResetClientName === L1SpywarecorrectSpyname)) { // L1 and L2 should be in the same profile
        L1SpywareprofileNameMatch = true;
        if (scoringSpyDebug == true) {
            debugOuptut += `     L1 Reset Client Rule matches Inf Rule}\n`;
            debugOuptut += `     L1SpywareprofileNameMatch: ${L1SpywareprofileNameMatch}\n`;
        }
    }

    if (scoringSpyDebug == true) {
        debugOuptut += `     ?????? Variable States ?????\n`;
        debugOuptut += `     L2FBmultiencodefbname: ${L2FBmultiencodefbname}\n`;
        debugOuptut += `     L3FBcorrectfbname set to ${L3FBcorrectfbname}\n`;
        debugOuptut += `?????????? Spyware End ??????????\n\n`;
    }

    if (scoringSpyDebug == true)
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}
exports.scoringSpywareSinkhole = function (obj, spywareProfileName, fwName) {
    let points = 0;
    if (scoringSpywareSinkholenDebug === true) {
        debugOuptut = `     ?????? Spyware Sinkhole Scoring ??????\n`;
        debugOuptut += JSON.stringify(obj);
        debugOuptut += `\n     spywareProfileName :`;
        debugOuptut += JSON.stringify(spywareProfileName, fwName);
        debugOuptut += `\n     L1SpywarecorrectSpyname = ${L1SpywarecorrectSpyname}\n      L1SpywarecorrectSpyname = ${L1SpywarecorrectSpyname}\n\n`;
    }

    if (Object.prototype.toString.call(obj) === '[object Array]') {
        for (let arr in obj) {
            if (obj[arr]['$'].name === 'default-paloalto-cloud') {
                // the action returns a property, not a value for some reason, so we need to loop for it.
                for (var prop in obj[arr]['action']) {
                    if (scoringSpywareSinkholenDebug) debugOuptut += `\n     prop = \n`;
                    if (scoringSpywareSinkholenDebug) debugOuptut += (prop);
                    if (prop === 'sinkhole')
                        L2SpywareSinkholeName = spywareProfileName;
                }
            }
        }
    } else {
        console.log(`Sinkhole is an Object`);
    }

    if (scoringSpywareSinkholenDebug === true)
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Profile - Vulnerability Scoring
exports.scoringVulnerability = function (obj, vulnerabilityProfileName, fwName) {
    let playerNum = 0; // not sure if I need this, don't think so
    playerNum = fwName.match(/[0-9]{1,}$/);

    const [name, action, category, severity, pcap] = [obj[0], obj[1], obj[2], obj[3], obj[4]];

    if (scoringVulnDebug == true) {
        debugOuptut = `???????????? Vulnerability ????????????\n`;  
        debugOuptut += JSON.stringify(obj);
        debugOuptut += `\n`;
        debugOuptut += `     Profile name: ${vulnerabilityProfileName}\n`;
        debugOuptut += `     name, action, category, severity, pcap: ${name, action, category, severity, pcap}\n`;
        debugOuptut += `     severity length: ${severity.length}\n`;
        debugOuptut += `???????????????????????????????????????\n`;
        debugOuptut += `     ?????? Variable States ?????\n`;
        debugOuptut += `     L1VulCorrectResetClientName: ${L1VulCorrectResetClientName}\n`;
        debugOuptut += `     L1VulCorrectDefaultClientName: ${L1VulCorrectDefaultClientName}\n`;
        debugOuptut += `     L1VulnprofileClientNameMatch: ${L1VulnprofileClientNameMatch}\n`;
        debugOuptut += `     L1VulCorrectResetServerName: ${L1VulCorrectResetServerName}\n`;
        debugOuptut += `     L1VulCorrectDefaultServerName: ${L1VulCorrectDefaultServerName}\n`;
        debugOuptut += `     L1VulnprofileServerNameMatch: ${L1VulnprofileServerNameMatch}\n`;
        debugOuptut += `     ????????????????????????????\n`;
    }

    //********************************************************************************************
    //Scenario L1 Client: vulnerability reset-client med-crit; alert on low-inf
    //********************************************************************************************
    if (scoringVulnDebug == true) {
        debugOuptut += `     ?????? Reset Client ?????\n`;
        debugOuptut += `     action = ${action} : category = ${category} : severity = ${severity}\n`;
    }
    // the order of the severity array may not be in order, so we need to check includes and that the length of array is specific to the number of answers we need.
    if (action === 'reset-client' && category === 'any' && severity.length === 3 && severity.includes('critical') && severity.includes('high') && severity.includes('medium')) {
        L1VulCorrectResetClientName = vulnerabilityProfileName; // need to make sure each rule is in the same profile for matching
        if (scoringVulnDebug == true) {
            debugOuptut += `     Reset Client on profile: ${vulnerabilityProfileName}\n`;
            debugOuptut += `     L1VulCorrectResetClientName: ${L1VulCorrectResetClientName}\n`;
        }
    } else if (action === 'default' && category === 'any' && severity.length === 2 && severity.includes('informational') && severity.includes('low')) { // L1 vuln
        L1VulCorrectDefaultClientName = vulnerabilityProfileName; // record names so that L1 and L2 should match, which indicates they are in the same profile
        if (scoringVulnDebug == true) {
            debugOuptut += `     Inf / default on profile: ${vulnerabilityProfileName}\n`;
            debugOuptut += `     L1VulCorrectDefaultClientName: ${L1VulCorrectDefaultClientName}\n`;
        }
    } 

    if (scoringVulnDebug == true) {
        debugOuptut += `     ?????? Match L1 / L2 names ?????\n`;
        debugOuptut += `     L1VulCorrectResetClientName = ${L1VulCorrectResetClientName} : L1VulCorrectDefaultClientName = ${L1VulCorrectDefaultClientName}\n`;
    }
    if (typeof(L1VulCorrectResetClientName) !== 'undefined' && (L1VulCorrectResetClientName === L1VulCorrectDefaultClientName)) { // L1 and L2 should be in the same profile
        L1VulnprofileClientNameMatch = true;
        L1VulCorrectClientName = L1VulCorrectResetClientName;
        if (scoringVulnDebug == true) {
            debugOuptut += `     L1 / L2 names match\n`;
            debugOuptut += `     L1VulnprofileClientNameMatch: ${L1VulnprofileClientNameMatch}\n`;
            debugOuptut += `     L1VulCorrectDefaultClientName: ${L1VulCorrectDefaultClientName}\n`;
        } 
    }

    //********************************************************************************************
    //Scenario L1 Server: vulnerability reset-server med-crit; alert on low-inf
    //********************************************************************************************
    if (scoringVulnDebug == true) {
        debugOuptut += `     ?????? Reset Server ?????\n`;
        debugOuptut += `     action = ${action} : category = ${category} : severity = ${severity}\n`;
    }
    // the order of the severity array may not be in order, so we need to check includes and that the length of array is specific to the number of answers we need.
    if (action === 'reset-server' && category === 'any' && severity.length === 3 && severity.includes('critical') && severity.includes('high') && severity.includes('medium')) {
        L1VulCorrectResetServerName = vulnerabilityProfileName; // need to make sure each rule is in the same profile for matching
        if (scoringVulnDebug == true) {
            debugOuptut += `     Reset Server on profile: ${vulnerabilityProfileName}\n`;
            debugOuptut += `     L1VulCorrectResetServerName: ${L1VulCorrectResetServerName}\n`;
        }
    } else if (action === 'default' && category === 'any' && severity.length === 2 && severity.includes('informational') && severity.includes('low')) { // L1 vuln
        L1VulCorrectDefaultServerName = vulnerabilityProfileName; // record names so that L1 and L2 should match, which indicates they are in the same profile
        if (scoringVulnDebug == true) {
            debugOuptut += `     Inf / Default on profile: ${vulnerabilityProfileName}\n`;
            debugOuptut += `     L1VulCorrectDefaultServerName: ${L1VulCorrectDefaultServerName}\n`;
        } 
    }

    if (scoringVulnDebug == true) {
        debugOuptut += `     ?????? L1 L2 Name Match ?????\n`;
        debugOuptut += `     L1VulCorrectResetServerName = ${L1VulCorrectResetServerName} : L1VulCorrectDefaultServerName = ${L1VulCorrectDefaultServerName}\n`;
    }
    if (typeof(L1VulCorrectResetServerName) !== 'undefined' && (L1VulCorrectResetServerName === L1VulCorrectDefaultServerName)) { // L1 and L2 should be in the same profile
        L1VulnprofileServerNameMatch = true;
        L1VulCorrectServerName = L1VulCorrectResetServerName;
        if (scoringVulnDebug == true) debugOuptut += `     L1VulnprofileServerNameMatch = ${L1VulnprofileServerNameMatch}\n`;
    }

    if (scoringVulnDebug == true) {
        debugOuptut += `     ?????? Variable States ?????\n`;
        debugOuptut += `     L1VulnprofileServerNameMatch: ${L1VulnprofileServerNameMatch}\n`;
        debugOuptut += `?????????? Vulnerability End ??????????\n\n`;
    }

    if (scoringVulnDebug == true)
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}
exports.scoringVulnerabilityException = function (name, vulnerabilityProfileName, fwName) {
    if (scoringVulnExceptionDebug === true) {
        debugOuptut = `     ?????? Vuln Exception Scoring ??????\n`;
        debugOuptut += JSON.stringify(name);
        debugOuptut += `\n     vulnerabilityProfileName :\n`;
        debugOuptut += JSON.stringify(vulnerabilityProfileName, fwName);
        debugOuptut += `\n     L1SpywareprofileNameMatch : `;
        debugOuptut += JSON.stringify(L1SpywareprofileNameMatch);
        debugOuptut += `\n`;
    }

    if (vulnerabilityProfileName === L1VulCorrectResetClientName) // check to see the exception is on the Client profile and not Server
        if (name === '54319') {
            if (scoringVulnExceptionDebug === true) debugOuptut += `     vuln exception ID correct\n`;
            if (scoringVulnExceptionDebug === true) debugOuptut += `     vulnerabilityProfileName = ${vulnerabilityProfileName}\n`;
            L2VulCorrectExceptionName = vulnerabilityProfileName;
        }

    if (scoringVulnExceptionDebug === true) 
        // debug write to file
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Profile - DoS scoring
exports.processDosProfiles = function(name, type, tcpEnable, tcpSynCookieEnable, synCookieDuration, synCookieAlarm, synCookieActivate, synCookieMax) {

    if (debug_dosProfile == true) {
        debugOuptut = `???????????? DoS ????????????\n`;  
        debugOuptut += `???????????????????????????????????????\n`;
        debugOuptut += `     ?????? Variable States ?????\n`;
        debugOuptut += `     name: ${name}\n`;
        debugOuptut += `     type: ${type}\n`;
        debugOuptut += `     tcpEnable: ${tcpEnable}\n`;
        debugOuptut += `     tcpSynCookieEnable: ${tcpSynCookieEnable}\n`;
        debugOuptut += `     synCookieDuration: ${synCookieDuration}\n`;
        debugOuptut += `     synCookieAlarm: ${synCookieAlarm}\n`;
        debugOuptut += `     synCookieActivate: ${synCookieActivate}\n`;
        debugOuptut += `     synCookieMax: ${synCookieMax}\n`;
        debugOuptut += `     ????????????????????????????\n`;
    }

    debugOuptut += `\n     synCookieDuration = ${synCookieDuration}\n     synCookieAlarm = ${synCookieAlarm}\n     synCookieActivate = ${synCookieActivate}\n     synCookieMax = ${synCookieMax}\n`;

    if (tcpEnable && tcpSynCookieEnable && type === 'classified' && (synCookieDuration == 300 || synCookieDuration === '') && synCookieAlarm == 1000 && synCookieActivate == 1500 && synCookieMax == 3000)
        L2DosProfileObjectName = name;

    debugOuptut += `\n     ?????? Post States ?????\n`;
    debugOuptut += `     L2DosProfileObjectName: ${L2DosProfileObjectName}\n`;
    debugOuptut += `     ????????????????????????????\n`;

    if (debug_dosProfile === true) 
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Security Profiles Scoring
exports.scoringSecurityProfiles = function (fwName) {
    let playerNum = 0; // these will have to move to security policy once complete
    playerNum = fwName.match(/[0-9]{1,}$/);

    let score = 0;
    //let [virus, spyware, fileBlocking] = [obj[0], obj[1], obj[2]];

    if (scoringSPDebug == true) {
        debugOuptut = `???????????? Profile Scoring ????????????\n`;  
        //debugOuptut += `     virus, spyware, fileBlocking: ${virus, spyware, fileBlocking}\n`;
        //debugOuptut += `???????????????????????????????????????\n\n`;
        debugOuptut += `     ?????? PRE Variable States ?????\n`; 
        debugOuptut += `     ??????? File Bloocking ?????\n`;
        debugOuptut += `     L1FBname: ${L1FBname}\n`;
        debugOuptut += `     L1FileBlockingComplete[playerNum]: ${L1FileBlockingComplete[playerNum]}\n`;
        debugOuptut += `     L2FileBlockingComplete[playerNum]: ${L2FileBlockingComplete[playerNum]}\n`;
        debugOuptut += `     L2FBprofileNameMatch: ${L2FBprofileNameMatch}\n`;
        debugOuptut += `     L2FBmultiencodefbname: ${L2FBmultiencodefbname}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;

        debugOuptut += `     ?????????? Spyware ?????????\n`;
        debugOuptut += `     L1SpywareprofileNameMatch: ${L1SpywareprofileNameMatch}\n`;
        debugOuptut += `     L1SpywareComplete[playerNum]: ${L1SpywareComplete[playerNum]}\n`;
        debugOuptut += `     L1SpywareCorrectResetClientName: ${L1SpywareCorrectResetClientName}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;

        debugOuptut += `     ????????? Antivirus ???????\n`;
        debugOuptut += `     L1AVprofileNameMatch: ${L1AVprofileNameMatch}\n`;
        debugOuptut += `     L1AntivirusComplete[playerNum]: ${L1AntivirusComplete[playerNum]}\n`;
        debugOuptut += `     L1aAVCorrectavname: ${L1aAVCorrectavname}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;

        debugOuptut += `     ???????? Vuln Client ???????\n`;
        debugOuptut += `     L1VulnprofileClientNameMatch: ${L1VulnprofileClientNameMatch}\n`;
        debugOuptut += `     L1VulnerabilityClientComplete[playerNum]: ${L1VulnerabilityClientComplete[playerNum]}\n`;
        //debugOuptut += `     L1aAVCorrectavname: ${L1aAVCorrectavname}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;

        debugOuptut += `     ???????? Vuln L2 Exp ??????\n`;
        debugOuptut += `     L1VulnprofileClientNameMatch: ${L1VulnprofileClientNameMatch}\n`;
        debugOuptut += `     L1VulnerabilityClientComplete[playerNum]: ${L1VulnerabilityClientComplete[playerNum]}\n`;
        debugOuptut += `     L2VulnerabilityExceptionComplete[playerNum]: ${L2VulnerabilityExceptionComplete[playerNum]}\n`;
        debugOuptut += `     L1VulCorrectResetClientName: ${L1VulCorrectResetClientName}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;  

        debugOuptut += `     ????????? URL Filter ???????\n`;
        debugOuptut += `     L1aURLNameMatch: ${L1aURLNameMatch}\n`;
        debugOuptut += `     L1aBlockURLCorrectname: ${L1aBlockURLCorrectname}\n`;
        debugOuptut += `     L1aURLComplete[playerNum]: ${L1aURLComplete[playerNum]}\n`;
        debugOuptut += `     L1aAlertURLCorrectname: ${L1aAlertURLCorrectname}\n\n`;
        debugOuptut += `     L1bURLNameMatch: ${L1bURLNameMatch}\n`;
        debugOuptut += `     L1bContinueURLCorrectname: ${L1bContinueURLCorrectname}\n`;
        debugOuptut += `     L1bURLComplete[playerNum]: ${L1bURLComplete[playerNum]}\n`;
        debugOuptut += `     L1bAlertURLCorrectname: ${L1bAlertURLCorrectname}\n\n`;
        debugOuptut += `     L2URLComplete[playerNum]: ${L2URLComplete[playerNum]}\n`;
        debugOuptut += `     L2URLContainerComplete: ${L2URLContainerComplete}\n`;
        debugOuptut += `     L2URLCorrectname: ${L2URLCorrectname}\n\n`;
        debugOuptut += `     L3URLComplete[playerNum]: ${L3URLComplete[playerNum]}\n`;
        debugOuptut += `     L3URLCustomURLComplete: ${L3URLCustomURLComplete}\n`;
        debugOuptut += `     L3URLCorrectname: ${L3URLCorrectname}\n`;
        debugOuptut += `     L3URLCustomURLContinue: ${L3URLCustomURLContinue}\n`;
        debugOuptut += `     L3URLCustomURLBlock: ${L3URLCustomURLBlock}\n\n`;
        debugOuptut += `     ????????????????????????????\n\n`; 

    }

    if (scoringSPDebug == true) {
        debugOuptut += `     ****************************************************************************************\n`;
        debugOuptut += `     ?????? POST Variable States ?????\n`; 
        debugOuptut += `     ??????? File Bloocking ?????\n`;
        debugOuptut += `     L1FBname: ${L1FBname}\n`;
        debugOuptut += `     L1FileBlockingComplete[playerNum]: ${L1FileBlockingComplete[playerNum]}\n`;
        debugOuptut += `     L2FileBlockingComplete[playerNum]: ${L2FileBlockingComplete[playerNum]}\n`;
        debugOuptut += `     L2FBprofileNameMatch: ${L2FBprofileNameMatch}\n`;
        debugOuptut += `     L2FBmultiencodefbname: ${L2FBmultiencodefbname}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;

        debugOuptut += `     ??????? File Bloocking ?????\n`;
        debugOuptut += `     L1FBname: ${L1FBname}\n`;
        debugOuptut += `     L1FileBlockingComplete[playerNum]: ${L1FileBlockingComplete[playerNum]}\n`;
        debugOuptut += `     L2FileBlockingComplete[playerNum]: ${L2FileBlockingComplete[playerNum]}\n`;
        debugOuptut += `     L2FBprofileNameMatch: ${L2FBprofileNameMatch}\n`;
        debugOuptut += `     L2FBmultiencodefbname: ${L2FBmultiencodefbname}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;

        debugOuptut += `     ?????????? Spyware ?????????\n`;
        debugOuptut += `     L1SpywareprofileNameMatch: ${L1SpywareprofileNameMatch}\n`;
        debugOuptut += `     L1SpywareComplete[playerNum]: ${L1SpywareComplete[playerNum]}\n`;
        debugOuptut += `     L1SpywareCorrectResetClientName: ${L1SpywareCorrectResetClientName}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;

        debugOuptut += `     ????????? Antivirus ???????\n`;
        debugOuptut += `     L1AVprofileNameMatch: ${L1AVprofileNameMatch}\n`;
        debugOuptut += `     L1AntivirusComplete[playerNum]: ${L1AntivirusComplete[playerNum]}\n`;
        debugOuptut += `     L1aAVCorrectavname: ${L1aAVCorrectavname}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;    

        debugOuptut += `     ???????? Vuln Client ???????\n`;
        debugOuptut += `     L1VulnprofileClientNameMatch: ${L1VulnprofileClientNameMatch}\n`;
        debugOuptut += `     L1VulnerabilityClientComplete[playerNum]: ${L1VulnerabilityClientComplete[playerNum]}\n`;
        //debugOuptut += `     L1aAVCorrectavname: ${L1aAVCorrectavname}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;

        debugOuptut += `     ???????? Vuln Server ???????\n`;
        debugOuptut += `     L1VulnprofileServerNameMatch: ${L1VulnprofileServerNameMatch}\n`;
        debugOuptut += `     L1VulnerabilityServerComplete[playerNum]: ${L1VulnerabilityServerComplete[playerNum]}\n`;
        //debugOuptut += `     L1aAVCorrectavname: ${L1aAVCorrectavname}\n`;
        debugOuptut += `     ????????????????????????????\n\n`;

        debugOuptut += `     ???????? Vuln L2 Exp ??????\n`;
        debugOuptut += `     L1VulnprofileClientNameMatch: ${L1VulnprofileClientNameMatch}\n`;
        debugOuptut += `     L1VulnerabilityClientComplete[playerNum]: ${L1VulnerabilityClientComplete[playerNum]}\n`;
        debugOuptut += `     L2VulnerabilityExceptionComplete[playerNum]: ${L2VulnerabilityExceptionComplete[playerNum]}\n`;
        debugOuptut += `     L1VulCorrectResetClientName: ${L1VulCorrectResetClientName}\n`;
        debugOuptut += `     ????????????????????????????\n\n`; 

        debugOuptut += `     ????????? URL Filter ???????\n`;
        debugOuptut += `     L1aURLNameMatch: ${L1aURLNameMatch}\n`;
        debugOuptut += `     L1aBlockURLCorrectname: ${L1aBlockURLCorrectname}\n`;
        debugOuptut += `     L1aURLComplete[playerNum]: ${L1aURLComplete[playerNum]}\n`;
        debugOuptut += `     L1aAlertURLCorrectname: ${L1aAlertURLCorrectname}\n\n`;
        debugOuptut += `     L1bURLNameMatch: ${L1bURLNameMatch}\n`;
        debugOuptut += `     L1bContinueURLCorrectname: ${L1bContinueURLCorrectname}\n`;
        debugOuptut += `     L1bURLComplete[playerNum]: ${L1bURLComplete[playerNum]}\n`;
        debugOuptut += `     L1bAlertURLCorrectname: ${L1bAlertURLCorrectname}\n\n`;
        debugOuptut += `     L2URLComplete[playerNum]: ${L2URLComplete[playerNum]}\n`;
        debugOuptut += `     L2URLContainerComplete: ${L2URLContainerComplete}\n`;
        debugOuptut += `     L2URLCorrectname: ${L2URLCorrectname}\n\n`;

        debugOuptut += `     L3URLComplete[playerNum]: ${L3URLComplete[playerNum]}\n`;
        debugOuptut += `     L3URLCustomURLComplete: ${L3URLCustomURLComplete}\n`;
        debugOuptut += `     L3URLCorrectname: ${L3URLCorrectname}\n`;
        debugOuptut += `     L3URLCustomURLContinue: ${L3URLCustomURLContinue}\n`;
        debugOuptut += `     L3URLCustomURLBlock: ${L3URLCustomURLBlock}\n\n`;
        debugOuptut += `     ????????????????????????????\n\n`; 

    }

    if (scoringSPDebug == true)
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}

//Security Profile Groups Scoring
exports.scoringSecurityProfileGroups = function (obj, spgProfileName, fwName) {
    let playerNum = 0; // these will have to move to security policy once complete
    playerNum = fwName.match(/[0-9]{1,}$/);

    let score = 0;
    let [virus, spyware, vulnerability, url, fileBlocking, dataFiltering, wildfire] = [obj[0], obj[1], obj[2], obj[3], obj[4], obj[5], obj[6]];

    if (scoringSPGDebug === true) {
        debugOuptut = `\n????????????? Security Profile Groups ???????????????\n`;
        debugOuptut += `name = ${spgProfileName}\n`;
        debugOuptut += `Antivirus member = ${virus}\n`; 
        debugOuptut += `Antivirus correct = ${L1AVCorrectname}\n`; 
        debugOuptut += `Spyware member = ${spyware}\n`; 
        debugOuptut += `Spyware correct = ${L1SpywarecorrectSpyname}\n`; 
        debugOuptut += `Vulnerability member = ${vulnerability}\n`; 
        debugOuptut += `Vulnerability correct = ${L1VulCorrectClientName} (client)\n`; 
        debugOuptut += `URL member = ${url}\n`; 
        debugOuptut += `URL correct = ${L1aCorrectname}\n`; 
        debugOuptut += `File Blocking member = ${fileBlocking}\n`; 
        debugOuptut += `File Blocking correct = ${L1FBname}\n`; 
        debugOuptut += `Data filtering member = ${dataFiltering}\n`;
        debugOuptut += `Wildfire member = ${wildfire}\n`; 
        debugOuptut += `?????????????????????????????????????????????????????\n\n`;
        debugOuptut += `\n?????????? Variable States PRE ?????????\n`;
        debugOuptut += `L1SecurityProfileGroupComplete = ${L1SecurityProfileGroupComplete}\n`; 
        debugOuptut += `L1SPGComplete[playerNum] = ${L1SPGComplete[playerNum]}\n`; 
        debugOuptut += `L1SecurityProfileGroupName = ${L1SecurityProfileGroupName}\n`; 
        debugOuptut += `?????????? Variable States PRE ?????????\n\n`;
    }

    if (scoringSPGDebug === true) debugOuptut += `     ????????? Security Profile Groups ???????\n`;  

    //********************************************************************************************
    //Scenario L1 Profile Group: take the correctnames from all the individual profiles
    //********************************************************************************************

    console.log(`WF profile is = ${wildfire}`);

    // check of L1 Profile Group
    if (virus == `${L1AVCorrectname}` && spyware == `${L1SpywarecorrectSpyname}` && vulnerability == `${L1VulCorrectClientName}` && url == `${L1aCorrectname}` && fileBlocking == `${L1FBname}` && dataFiltering == `${L2DataCorrectAlertProfileName}` && dataFiltering == `${L2DataCorrectCCProfileName}` && wildfire === 'default') {
        if (scoringSPGDebug === true) debugOuptut += `     L1 SPG Profile match\n`;  
        L1SecurityProfileGroupName = spgProfileName;
        L1SecurityProfileGroupComplete = true;
        if (L1SecurityProfileGroupName === 'default')
            L3SecurityProfileGroupComplete = true;
    }

    if (scoringSPGDebug == true) {
        debugOuptut += `\n????????????????????????????\n`;
        debugOuptut += `name = ${spgProfileName}\n`;
        debugOuptut += `Antivirus member = ${virus}\n`; 
        debugOuptut += `Spyware member = ${spyware}\n`; 
        debugOuptut += `Vulnerability member = ${vulnerability}\n`; 
        debugOuptut += `URL member = ${url}\n`; 
        debugOuptut += `File Blocking member = ${fileBlocking}\n`; 
        debugOuptut += `Wildfire member = ${wildfire}\n`; 
        debugOuptut += `????????????????????????????\n`;
        debugOuptut += `\n?????????? Variable States POST ?????????\n`;
        debugOuptut += `L1SecurityProfileGroupComplete = ${L1SecurityProfileGroupComplete}\n`; 
        debugOuptut += `L1SPGComplete[playerNum] = ${L1SPGComplete[playerNum]}\n`; 
        debugOuptut += `L1SecurityProfileGroupName = ${L1SecurityProfileGroupName}\n`; 
        debugOuptut += `?????????? Variable States POST ?????????\n\n`;
    }

    if (scoringSPGDebug == true)
        fs.appendFile(file, `${debugOuptut}\r\n`, (err) => {
            if (err) throw err;
        });
}
