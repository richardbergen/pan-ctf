// Not set up for multi-vsys. It will likely not work, but it could. I don't know.
// certifying for 9.0.7

// Known issues:
// category should be changed to arrays in all security rules
// **** not any more **** if player inputs the wrong API key, you have to restart the whole process again. Allow user to reset api key or have game admin do that through console?
// **** not any more **** if they choose the wrong FW in the drop down, it scews up both players
// *** create unique logins per person with their name*** player number is assigned by who connects first. With Clientless VPN, if they log in as "player0" but hit the CTF GUI first, they might get the 5'th firewall. Not really a coding issue per se, but might be confusing to the person
// 
// FEATURE REQUESTS
// expand-all / collapse all option in the player dashboard?

/* TODO
- if the app crashes, the players will have to rejoin in the exact order... that's a problem!
- input validation
- test if an extra firewall sends a command in? Probably also will crash.
- change player name change to emit instead of form submission
*/

const gameTimeInSeconds = 4500; // 3600 = 1 hr
//const gameTimeInSeconds = 120;
let gameTimeRemaining;
let gameStart = false;
let scoreboardLeader = {
    currentLeader: '',
    currentHighScore: 0,
    previousLeader: '',
    previousHighScore: 0
}

const soundServer = 'bf59c005.ngrok.io';
const soundServerPort = 80;  

// sounds clips
const startGame = 'game_start'; // 'prepare' for just short
let firstBlood = false;


process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0; // disable untrusted cert error when connecting to FW API
// all da includes
const http = require('http');
const https = require('https');
const url = require('url');
const fs = require('fs');
var path = require('path');

let parseString = require('xml2js').parseString;
const { parse } = require('querystring');

const admin = fs.readFileSync(__dirname + '/admin.html');
const player = fs.readFileSync(__dirname + '/player.html');
const images = fs.readFileSync(__dirname + '/images.html');
const init = fs.readFileSync(__dirname + '/init.html');

var sockets = require('http'),
    // NEVER use a Sync function except at start-up!
    index = fs.readFileSync(__dirname + '/index.html');

let players = 0; // number of players initially set (overwritten) by game admin
let playerCounter = 0; // increments as players join and decrements from the players counter set by the game admin
let playerIPs = []; // store the IP addresses of the clients connecting in to hand out player numbers
let prevPlayerNum = -1;
let playerStreak = 1;

// Load scenario file here
const scenario = require('./scenario1.js');

const fwAPI = 'LUFRPT1XU3pHYWRLdDhSaEFaUmNBR09YK0w2dkF3TGs9NnloL1lPNWk4OUVRa1NBTlUzbmZUdz09'; // need to add per FW and see if this is stored in the config or if this needs to be generated per FW 
//LUFRPT14MW5xOEo1R09KVlBZNnpnemh0VHRBOWl6TGM9bXcwM3JHUGVhRlNiY0dCR0srNERUQT09

/**************************************************
             Debugging Stuffs
**************************************************/
// Generic debugs
fullConfigDebug = false;
httpGetDebug = false;
scoringDebug = false;
scoreBoardDebug = false; // scoreboard

//Policies tab
debug_decryptPolicies = false;
debug_dosPolicies = false;
debug_qosPolicies = false;
debug_securityPolicies = false; // security policies debug

// Objects tab
debug_addressObjects = false; // address objects debug
debug_scheduleObject = false; // schedule object debug
debug_AppFilter = false;

debug_logForward = false;

debug_fileBlocking = false; // file blocking
urlDebug = false;
customUrlDebug = false;
debug_dataFilteringRules = false;
debug_customDataObject = false;
spyDebug = false; // spyware
spySinkDebug = false; // spyware sinkholing
virDebug = false; // antivirus
debug_vulnRules = false; // vulnerability
debug_dosProfile = false;
vulnExceptionDebug = false; // vulnerability exception testing

debug_securityProfiles = false; // generic profiles processing

debug_securityProfileGroup = false; // security profile groups

// Network tab
debug_zones = false;
debug_vr = false;
debug_physInt = false;
debug_qosProfile = false;
debug_qosInterface = false;

// Device tab
debug_deviceConfig = false; // schedules, dynamic updates, logo images, CTD, system time, hostname and other Device > Settings config
debug_sharedConfig = false; // app overrides, app filters, filters botnet config, custom threat sigs, roles
debug_settingConfig = false; // allow-forward-decrypted-content
debug_networkProfile = false;
debug_userID = false;

// Firewall IPs
pavm0_ip = '192.168.152.100';
pavm1_ip = '192.168.152.101';
pavm2_ip = '192.168.152.102';
pavm3_ip = '192.168.152.103';
pavm4_ip = '192.168.152.104';
pavm5_ip = '192.168.152.105';
pavm6_ip = '192.168.152.106';
pavm7_ip = '192.168.152.107';
pavm8_ip = '192.168.152.108';
pavm9_ip = '192.168.152.109';



//******************* End Debugs **********************

//***********************************************************************************************
//player HTML page
//***********************************************************************************************
function getPlayerHTML(playerNum) {
    let playerHTML = `<html> \n`;
    playerHTML += `        <style> \n`;
    playerHTML += `            .btnActive { \n`;
    playerHTML += `                background: none!important; \n`;
    playerHTML += `                color:lime; \n`;
    playerHTML += `                border: none; \n`;
    playerHTML += `                font-family:Andale Mono; \n`;
    playerHTML += `                font-size: 16px; \n`;
    playerHTML += `                padding: 0!important; \n`;
    playerHTML += `                text-decoration: underline; \n`;
    playerHTML += `                cursor: pointer; \n`;
    playerHTML += `            } \n`; 
    playerHTML += `            .btnNotActive { \n`;
    playerHTML += `                background: none!important; \n`;
    playerHTML += `                color:green; \n`;
    playerHTML += `                border: none; \n`;
    playerHTML += `                font-family:Andale Mono; \n`;
    playerHTML += `                font-size: 16px; \n`;
    playerHTML += `                padding: 0!important; \n`;
    playerHTML += `                text-decoration: underline; \n`;
    playerHTML += `                cursor: pointer; \n`;
    playerHTML += `            } \n`; 

    playerHTML += `                 .fadeOut { \n `;
    playerHTML += `                     position: fixed; \n`;
    playerHTML += `                     top: 50%; \n`;
    playerHTML += `                     left: 50%; \n`;
    playerHTML += `                     transform: translate(-50%, -50%); \n`;
    playerHTML += `                     color: lime; \n`;
    playerHTML += `                     font-size: 100px; \n`;
    playerHTML += `                     opacity: 1; \n`;
    playerHTML += `                     animation: fadeOut 1.5s ease-in 1 forwards; \n`;
    playerHTML += `                  } \n`;

    playerHTML += `                 .fadeOut2 { \n `;
    playerHTML += `                     position: fixed; \n`;
    playerHTML += `                     top: 50%; \n`;
    playerHTML += `                     left: 50%; \n`;
    playerHTML += `                     transform: translate(-50%, -50%); \n`;
    playerHTML += `                     color: lime; \n`;
    playerHTML += `                     font-size: 100px; \n`;
    playerHTML += `                     opacity: 1; \n`;
    playerHTML += `                     animation: fadeOut 1.5s ease-in 1 forwards; \n`;
    playerHTML += `                  } \n`;

    playerHTML += `                  @keyframes fadeOut { \n`;
    playerHTML += `                     to { \n`;
    playerHTML += `                       opacity: 0; \n`;
    playerHTML += `                     } \n`;
    playerHTML += `                  } \n`;

    playerHTML += `                 .hidden { \n `;
    playerHTML += `                     position: fixed; \n`;
    playerHTML += `                     top: 50%; \n`;
    playerHTML += `                     left: 50%; \n`;
    playerHTML += `                     transform: translate(-50%, -50%); \n`;
    playerHTML += `                       opacity: 0; \n`;
    playerHTML += `                  } \n`;


    playerHTML += `        </style>\n`;
    playerHTML += `    <script src='/socket.io/socket.io.js'></script> \n`;
    playerHTML += `    <div style="background-image:url(background.jpg); \n`;
    playerHTML += `                background-repeat:no-repeat; \n`;
    playerHTML += `                background-attachment:fixed; \n`;
    playerHTML += `                background-position: center center; \n`;
    playerHTML += `                overflow-y: auto; overflow-x:hidden; \n`;
    //playerHTML += `                background-size: 100% 100%; \n`;
    playerHTML += `                line-height:2em;">\n \n`;
    playerHTML += `                <div id="announcement" class="hidden" style="font-family:Andale Mono;"></font></div> \n`;
    //playerHTML += `                <div id="announcement" class="centered" style="position:absolute; top:0; left:0;">test</div> \n`;
    playerHTML += `        <body style="font-family:Andale Mono;" bgcolor="black"> <font color="lime"> \n`;
    //playerHTML += `        <body style="font-family:Andale Mono;"> <font color="lime"> \n`;
    //playerHTML += `             <form method="post"> \n`;
    playerHTML += `            <table border=0 width=100%><tr><td align=left><font color="lime">Player Name: \n`;
    playerHTML += `                <input style="border-color:lime; background-color:Black; color:lime;" type="text" name="playerNameField" size="10" id="playerNameField"/> \n`; // newly aded
    playerHTML += `                <input type="button" class="btnActive" value="Submit" onclick="setPlayerName(document.getElementById('playerNameField').value);"/> \n`; // newly added
    //playerHTML += `                <input style="border-color:lime; background-color:Black; color:lime;" name="playerName"></font><br><input type="submit"></form></td> \n`;
    playerHTML += `                <td valign="top" align=left>&nbsp;&nbsp;&nbsp;<font color="lime">[&nbsp;&nbsp;Player Name = <span id="playerName">Refreshing...</span> \n`;
    playerHTML += `                &nbsp;|&nbsp;&nbsp;Score = <span id="totalScore">Refreshing...</span> \n`;
    playerHTML += `                &nbsp|&nbsp;\n`;
    playerHTML += `                    <a href="/images.html" target="_blank" style="target-new: tab;"><font color="lime">Game Images</font></a> \n`;
    playerHTML += `                &nbsp;|&nbsp;\n`;
    playerHTML += `                FW URL: <a id="firewall_link" target="_blank" style="target-new: tab;" href=""></a> \n`;
    playerHTML += `                    &nbsp;]</font></td> \n`;
    playerHTML += `                <td align="right"><font color="lime">Time remaining:&nbsp;<span id=minutesRemaining>Sync'ing...</span>&nbsp;<span id=secondsRemaining></span></font></td> \n`;
    playerHTML += `             </tr></table> \n`;
    playerHTML += `            <ul id='messages'></ul> \n`;
    playerHTML += `            <ul id='dynamic-list'></ul> \n`;
    playerHTML += `            Game Objectives: \n`;
    playerHTML += `            <div id='testing123'></div> \n`;
    playerHTML += `            </font>\n`;
    playerHTML += `    <script> \n`;
    playerHTML += `        function parentUL(taskID, category, task, points, hintAvailable, hintPoints) { \n`;
    playerHTML += `            function insertAfter(newElement,targetElement) { \n`;
    playerHTML += `                var parent = targetElement.parentNode; \n`;
    playerHTML += `                if(parent.lastchild == targetElement) \n`;
    playerHTML += `                    parent.appendChild(newElement); \n`;
    playerHTML += `                else  \n`;
    playerHTML += `                    parent.insertBefore(newElement, targetElement.nextSibling); \n`;
    playerHTML += `            } \n`;
    playerHTML += `            var divStart = document.getElementById("testing123"); \n`;
    playerHTML += `            var summary = document.createElement("summary"); \n`;
    playerHTML += `            var details = document.createElement("details"); \n`;
    playerHTML += `            var divEnd = document.createElement("div"); \n`;
    playerHTML += `            var ul = document.createElement("ul"); \n`;
    playerHTML += `            var li = document.createElement("li"); \n`;
    playerHTML += `            li.setAttribute('id', taskID); \n`;
    playerHTML += `            li.innerHTML = \`($\{points\}: Points) $\{task\}\`; \n`;
    playerHTML += `            if (hintAvailable)\n`;
    playerHTML += `                li.innerHTML += \`&nbsp;[<button id="$\{taskID\}-button" class="btnActive" onclick="retrieveHint('$\{taskID\}')">Hint Available</button>: $\{hintPoints\} points]\`; \n`;
    playerHTML += `            summary.setAttribute('id', category); \n`;
    playerHTML += `            summary.innerHTML = category; \n`;
    playerHTML += `            divStart.appendChild(details); \n`;
    playerHTML += `            details.appendChild(summary); \n`;
    playerHTML += `            insertAfter(ul, summary); \n`;
    playerHTML += `            ul.appendChild(li); \n`;
    playerHTML += `            return taskID; \n`;
    playerHTML += `        } \n`;
    playerHTML += `        function childLI(taskID, prevTaskID, category, task, points, hintAvailable, hintPoints) { \n`;
    playerHTML += `            function insertAfter(newElement,targetElement) { \n`;
    playerHTML += `                var parent = targetElement.parentNode; \n`;
    playerHTML += `                if(parent.lastchild == targetElement) \n`;
    playerHTML += `                    parent.appendChild(newElement); \n`;
    playerHTML += `                else  \n`;
    playerHTML += `                    parent.insertBefore(newElement, targetElement.nextSibling); \n`;
    playerHTML += `            } \n`;
    playerHTML += `            let prevLi = document.getElementById(prevTaskID); \n`;
    playerHTML += `            var li = document.createElement("li"); \n`;
    playerHTML += `            li.setAttribute('id', taskID); \n`;
    playerHTML += `            li.innerHTML = \`($\{points\}: Points) $\{task\}\`;\n `;
    playerHTML += `            if (hintAvailable)\n`;
    playerHTML += `                li.innerHTML += \`&nbsp;[<button id="$\{taskID\}-button" class="btnbtnActive" onclick="retrieveHint('$\{taskID\}')">Hint Available</button>: $\{hintPoints\} points]\`; \n`;
    playerHTML += `            insertAfter(li, prevLi); \n`;
    playerHTML += `        } \n`;
    playerHTML += `        function childLIArray(arr) { \n`;
    playerHTML += `            function insertAfter(newElement,targetElement) { \n`;
    playerHTML += `                var parent = targetElement.parentNode; \n`;
    playerHTML += `                if(parent.lastchild == targetElement) \n`;
    playerHTML += `                    parent.appendChild(newElement); \n`;
    playerHTML += `                else  \n`;
    playerHTML += `                    parent.insertBefore(newElement, targetElement.nextSibling); \n`;
    playerHTML += `            } \n`;
    //playerHTML += `            console.log('arr = '); \n`;
    //playerHTML += `            console.log(arr); \n`;
    //playerHTML += `            console.log('arr.length = '); \n`;
    //playerHTML += `            console.log(arr.length); \n`;
    playerHTML += `            let [taskID, prevTaskID, category, task, points, hintAvailable, hintPoints] = ['', '', '', '', '', '', '']; \n`;
    //playerHTML += `            for (let i=arr.length; i > 0; i--) \n`;
    //playerHTML += `                console.log(\`i = $\{i\}\`); \n`;
    playerHTML += `            if (arr.length >= 1) \n`;
    //playerHTML += `                for (i in arr) { \n`;
    playerHTML += `                for (let i=arr.length; i > 0; i--) { \n`;
    playerHTML += `                    [taskID, prevTaskID, category, task, points, hintAvailable, hintPoints] = \n`;
    playerHTML += `                         [arr[i-1][0], arr[i-1][1], arr[i-1][2], arr[i-1][3], arr[i-1][4], arr[i-1][5], arr[i-1][6]]; \n`;
    //playerHTML += `                    console.log(\`taskID = $\{taskID\}\`) \n`;
    //playerHTML += `                    console.log(\`prevTaskID = $\{prevTaskID\}\`) \n`;
    //playerHTML += `                    console.log(\`category = $\{category\}\`) \n`;
    //playerHTML += `                    console.log(\`task = $\{task\}\`) \n`;
    //playerHTML += `                    console.log(\`points = $\{points\}\`) \n`;
    //playerHTML += `                    console.log(\`hintAvailable = $\{hintAvailable\}\`) \n`;
    //playerHTML += `                    console.log(\`hintPoints = $\{hintPoints\}\`) \n`;
    playerHTML += `                    let prevLi = document.getElementById(prevTaskID); \n`;
    playerHTML += `                    var li = document.createElement("li"); \n`;
    playerHTML += `                    li.setAttribute('id', taskID); \n`;
    playerHTML += `                    li.innerHTML = \`($\{points\}: Points) $\{task\}\`;\n `;
    playerHTML += `                    if (hintAvailable)\n`;
    playerHTML += `                       li.innerHTML += \`&nbsp;[<button id="$\{taskID\}-button" class="btnActive" onclick="retrieveHint('$\{taskID\}')">Hint Available</button>: $\{hintPoints\} points]\`; \n`;
    playerHTML += `                    insertAfter(li, prevLi); \n`;
    playerHTML += `                } \n`;
    playerHTML += `        } \n`;
    playerHTML += `        function setPlayerName(playerName) { \n `;
    playerHTML += `            document.getElementById('playerNameField').value = ""; \n `;
    playerHTML += `            socket.emit('updatePlayerName', { playerName: playerName }); \n `;
    //playerHTML += `            console.log(\`player submitted name: $\{playerName\}\`); \n `;
    playerHTML += `        } \n `;
    /*playerHTML += `        function Category(name) { \n`;
    playerHTML += `            this.categoryName = name; \n`;
    playerHTML += `            this.totalCategoryTasks = 0; \n`;
    playerHTML += `            this.remainingCategoryTasks = ''; \n`;
    playerHTML += `            this.addTotalCategoryTask = function() { \n`;
    playerHTML += `                 this.totalCategoryTasks++; \n`;
    playerHTML += `                 console.log(\`this.totalCategoryTasks = $\{this.totalCategoryTasks\}\`); \n`;
    playerHTML += `            } \n`;
    playerHTML += `            this.getTotalCategoryTasks = function() { \n`;
    playerHTML += `                 return this.totalCategoryTasks; \n`;
    playerHTML += `            } \n`;
    playerHTML += `        } \n`; */
    playerHTML += `        var socket = io(); \n`;
    playerHTML += `        let seconds = 0; \n`;
    playerHTML += `        let refreshIntervalId = setInterval(() => { \n`;
    playerHTML += `            if (seconds > 0) { \n`;
    playerHTML += `                 seconds--; \n`;
    playerHTML += `                 document.getElementById('secondsRemaining').textContent = \`$\{seconds\}s\`; \n`;
    playerHTML += `             } \n`;
    playerHTML += `        }, 1000); \n`;
    playerHTML += `        let playerNum = ${playerNum}; \n`;
    playerHTML += `        let prevCategory; \n`;
    playerHTML += `        let categories = []; \n`;
    playerHTML += `        let categoryCount = -1; \n`;
    playerHTML += `        console.log("Welcome to PAN CTF! No answers or hints in the source code. Sorry! But really, I\'m not. lolz. Cheater ;)"); \n`;
    playerHTML += `        socket.on('player${playerNum}-dashUpdate', function(data) { \n`;
    //playerHTML += `            console.log('emitter fired'); \n`;
    playerHTML += `            function updateCompleted(completedMissions) { \n`;
    playerHTML += `                for (let arr in completedMissions[0]) { \n`;
    playerHTML += `                    document.getElementById(completedMissions[0][arr]).style.textDecoration='line-through'; \n`;
    playerHTML += `                    document.getElementById(completedMissions[0][arr]).style.color='green'; \n`;
    playerHTML += `                    try { document.getElementById(\`$\{completedMissions[0][arr]\}-button\`).style.color='green'; } catch(error) { } \n`;
    //playerHTML += `                    document.getElementById(\`$\{completedMissions[0][arr]\}-button\`).className = "btnNotActive"; \n`;
    playerHTML += `                } \n`;
    playerHTML += `            } \n`;
    playerHTML += `            function updateScore(score) { \n`;
    playerHTML += `                document.getElementById("totalScore").textContent=\`$\{score\}\`; \n`;
    playerHTML += `            } \n`;
    playerHTML += `            function updatePlayerName(name) { \n`;
    playerHTML += `                document.getElementById("playerName").textContent=\`$\{name\}\`; \n`;
    playerHTML += `            } \n`;
    playerHTML += `            console.log('dashupdate called');  \n`;
    playerHTML += `            console.log(data);  \n`;
    playerHTML += `            let taskID;  \n`;
    playerHTML += `            let category; \n`;
    playerHTML += `            let task; \n`;
    playerHTML += `            let points; \n`;
    playerHTML += `            let prevtaskID; \n`;
    playerHTML += `            let hintAvailable = false; \n`;
    playerHTML += `            let hintPoints; \n`;
    playerHTML += `            let orderedTasks = [];; \n`;
    playerHTML += `            updatePlayerName(\`Player $\{playerNum\}\`); \n`;
    playerHTML += `            let playerName = data.tasks[1]; \n`;
    playerHTML += `            updatePlayerName(playerName); \n`;
    //playerHTML += `            console.log(\`playernum = $\{playerNum\} && data.tasks[0] = $\{data.tasks[0]\} \`); \n`;
    playerHTML += `            let tasks = data.tasks[2]; \n`;
    //playerHTML += `            console.log(tasks); \n`;
    playerHTML += `            let score = data.tasks[3]; \n`;
    playerHTML += `            updateScore(score); \n`;
    playerHTML += `            let completedMissions = data.tasks[4]; \n`;
    //playerHTML += `            console.log(completedMissions) \n`;
    //playerHTML += `            console.log(data.tasks);`;
    playerHTML += `            let availableHints = data.tasks[5]; \n`;
    playerHTML += `            let hintsPoints = data.tasks[6]; \n`;
    //playerHTML += `            console.log(hintsPoints); \n`;
    //playerHTML += `            console.log('tasks ='); \n`;
    //playerHTML += `            console.log(tasks); \n`;
    playerHTML += `             \n`;
    playerHTML += `            for (let i in tasks) { \n`;
    playerHTML += `                taskID = tasks[i][0]; \n`;
    playerHTML += `                let taskIDCheck = document.getElementById(taskID); \n`;
    playerHTML += `                if (typeof(taskIDCheck) !== 'undefined' && taskIDCheck != null) { \n`;
    //playerHTML += `                    console.log('taskID already exists!'); \n`;
    //playerHTML += `                    console.log(taskIDCheck); \n`;
    playerHTML += `                    break; \n`;
    playerHTML += `                } \n`;
    //playerHTML += `                console.log(taskID); \n`;
    playerHTML += `                if (availableHints.includes(taskID)) { \n`;
    //playerHTML += `                    console.log(i); \n`;
    playerHTML += `                    hintPoints = hintsPoints[i]; \n`;
    //playerHTML += `                    console.log('there is a match~'); \n`;
    playerHTML += `                    hintAvailable = true; \n`;
    playerHTML += `                } \n`;
    playerHTML += `                else \n`;
    playerHTML += `                    hintAvailable = false; \n`;
    playerHTML += `                category = tasks[i][1]; \n`;
    playerHTML += `                task = tasks[i][2]; \n`;
    playerHTML += `                points = tasks[i][3]; \n`;
    //playerHTML += `                console.log(\`i = $\{\i} and tasks.length = $\{tasks.length\}\ncategory = $\{category\}\`);`;
    playerHTML += `                if (category !== prevCategory) {  \n`; 
    //playerHTML += `                    console.log('New category= '); \n`;
    playerHTML += `                    if (orderedTasks.length > 0) { \n`;
    //playerHTML += `                        console.log(orderedTasks); \n`;
    playerHTML += `                        childLIArray(orderedTasks); \n`;
    playerHTML += `                    } \n`;
    playerHTML += `                    orderedTasks = []; // reset ordered tasks list for new Category \n`;
    //playerHTML += `                    console.log(taskID, category, task, points, hintAvailable, hintPoints); \n`;
    playerHTML += `                    prevtaskID = parentUL(taskID, category, task, points, hintAvailable, hintPoints); \n`;
    //playerHTML += `                    console.log('Creating new category ' + category); \n`;
    //playerHTML += `                    categoryCount++; \n`;
    //playerHTML += `                    categories[categoryCount] = new Category(category); \n`;
    //playerHTML += `                    categories[categoryCount].addTotalCategoryTask(); \n`;
    //playerHTML += `                    console.log(categories[categoryCount]); \n`;
    playerHTML += `                } else if (category === prevCategory) { \n`;
    playerHTML += `                    if (i == tasks.length - 1) { \n`; // capture the last element in the array to push as a child Li
    //playerHTML += `                        console.log([taskID, prevtaskID, category, task, points, hintAvailable, hintPoints]);\n `;
    playerHTML += `                        childLIArray([[taskID, prevtaskID, category, task, points, hintAvailable, hintPoints]]);\n `;
    playerHTML += `                        continue; \n`;
    playerHTML += `                    } \n`;
    //playerHTML += `                    console.log('Existing category'); \n`;
    //playerHTML += `                    console.log(taskID, prevtaskID, category, task, points, hintAvailable, hintPoints); \n`;
    //playerHTML += `                    categories[categoryCount].addTotalCategoryTask(); \n`;
    //playerHTML += `                    console.log('Adding task count to ' + category + ' for a total of ' + categories[categoryCount].getTotalCategoryTasks()); \n`;
    playerHTML += `                    orderedTasks.push([taskID, prevtaskID, category, task, points, hintAvailable, hintPoints]); \n`;
    //playerHTML += `                    childLI(taskID, prevtaskID, category, task, points, hintAvailable, hintPoints); \n`;
    playerHTML += `                } else { console.log('something else happened'); }\n`;
    playerHTML += `                prevCategory = category; \n`;
    playerHTML += `            } \n`;
    playerHTML += `            if (completedMissions.length > 0) \n`;
    playerHTML += `                updateCompleted(completedMissions); \n`;
    playerHTML += `        }); \n`;
    playerHTML += `        function retrieveHint(taskID) {  \n`;
    //playerHTML += `            console.log(\`button pressed for taskID $\{taskID\}\`); \n`;
    playerHTML += `            socket.emit('hint', { hintID: taskID }); \n`;
    playerHTML += `        } \n`;
    playerHTML += `        socket.on('player${playerNum}-complete', function(data) { \n`;
    playerHTML += `            for (let arr in data.complete) { \n`;
    playerHTML += `                document.getElementById(data.complete[arr]).style.textDecoration='line-through'; \n`;
    playerHTML += `                document.getElementById(data.complete[arr]).style.color='green'; \n`;
    playerHTML += `                try { document.getElementById(\`$\{data.complete[arr]\}-button\`).style.color='green'; } catch(error) { } \n`;
    //    playerHTML += `                // get zone array element  \n`;
    playerHTML += `            } \n`;
    playerHTML += `        }); \n`;
    playerHTML += `        socket.on('player${playerNum}-uncomplete', function(data) { \n`;
    playerHTML += `            for (let arr in data.uncomplete) { \n`;
    playerHTML += `                document.getElementById(data.uncomplete[arr]).style.textDecoration='none'; \n`;
    playerHTML += `                document.getElementById(data.uncomplete[arr]).style.color='lime'; \n`;
    playerHTML += `                try { document.getElementById(\`$\{data.uncomplete[arr]\}-button\`).style.color='lime'; } catch(error) { } \n`;
    playerHTML += `            } \n`;
    playerHTML += `        });  \n`;
    playerHTML += `        socket.on('player${playerNum}-totalScore', function(data) {  \n`;
    playerHTML += `            document.getElementById("totalScore").textContent=data.totalScore; \n`;
    playerHTML += `        });  \n`;
    playerHTML += `        socket.on('player${playerNum}-hint', function(data) {  \n`;
    //playerHTML += `            console.log('hint has been triggered'); \n`;
    //playerHTML += `            console.log(data.hint); \n`;
    playerHTML += `            alert(\`Hint: $\{data.hint\}\n\nPoints deducted: $\{data.points\}\`); \n`;
    playerHTML += `        });  \n`;
    playerHTML += `        socket.on('player${playerNum}-updatePlayerName', function(data) { \n`; // data.playerName
    playerHTML += `            document.getElementById("playerName").textContent=\`$\{data.playerName\}\`; \n`; 
    playerHTML += `            console.log(data); \n`; 
    playerHTML += `        }); \n`; 
    playerHTML += `        socket.on('player${playerNum}-alert', function(data) {  \n`;
    playerHTML += `            alert(\`$\{data.alert\}\`); \n`;
    playerHTML += `        });  \n`;
    playerHTML += `        socket.on('error', console.error.bind(console)); \n`;
    playerHTML += `        socket.on('message', console.log.bind(console)); \n`;
    playerHTML += `        socket.on('player${playerNum}-updateTime', function(data) { // take in array player name[0] score[1] \n`;
    playerHTML += `            console.log(data.remainingMinutes); \n`;
    playerHTML += `            if (data.remainingMinutes !==0) { \n`;
    playerHTML += `                let minutes = data.remainingMinutes - 1; \n`;
    playerHTML += `                document.getElementById('minutesRemaining').textContent = \`$\{minutes\}m\`; \n`;
    playerHTML += `                document.getElementById('secondsRemaining').textContent = ''; \n`;
    playerHTML += `                seconds = 60; \n`;
    playerHTML += `            }  \n `;
    playerHTML += `            if (data.gameEnd) { \n`;
    playerHTML += `                console.log('game over.const'); \n`;
    playerHTML += `                clearInterval(refreshIntervalId); \n`;
    playerHTML += `                document.getElementById('minutesRemaining').textContent = 'Game Over!'; \n`;
    playerHTML += `                document.getElementById('secondsRemaining').textContent = ''; \n`;
    playerHTML += `            } \n`;
    playerHTML += `        }); \n`;
    playerHTML += `        socket.on('player${playerNum}-setFirewallIP', function(data) { \n`;
    playerHTML += `            console.log(data.firewallip); \n`;
    //playerHTML += `            document.getElementById('firewall_ip').textContent = \`https://$\{data.firewallip\}\`; \n`;
    playerHTML += `            document.getElementById('firewall_link').setAttribute('href', \`https://$\{data.firewallip\}\`); \n`;
    playerHTML += `            document.getElementById('firewall_link').innerHTML = \`<font color="lime">https://$\{data.firewallip\}</font>\`; \n`;
    //playerHTML += `            link.href = \`https://$\{data.firewallip\}\`; \n`;
    playerHTML += `        }); \n`;
    playerHTML += `        socket.on('player${playerNum}-announcement', function(data) {  \n`;
    playerHTML += `            let announcementEl = document.getElementById('announcement'); \n`;
    playerHTML += `            announcementEl.textContent = \`$\{data.announcement\}\`; \n`;
    playerHTML += `            if (announcementEl.className == "hidden") { \n`;
    playerHTML += `                announcementEl.className = "fadeOut"; \n`;
    playerHTML += `                setTimeout(function() { announcementEl.className = "hidden"; }, 3000); \n`;
    playerHTML += `            } \n`;
    playerHTML += `            console.log(data); \n`;
    playerHTML += `        });  \n`;
    playerHTML += `    </script> \n`;
    playerHTML += `<br></body></div></html> \n`;
    return playerHTML;
}

function getApikeyHTML() {
    apikeyHTML = `<html> \n`;
    apikeyHTML += `    <script src='/socket.io/socket.io.js'></script> \n`;
    apikeyHTML += `        <body style="font-family:Andale Mono;" bgcolor="black"> <font color="lime"> \n`;
    apikeyHTML += `        Choose your VM firewall name from the drop-down list as seen on the dashboard of the firewall.<br><br> \n`;
    apikeyHTML += `            <form method="post"><font color="lime">VM Number: <select name="firewallName">`;
    for (let i = 0; i < players; i++)
        apikeyHTML += `                <option value="${i}">PA-VM${i}</option> \n`;
    apikeyHTML += `            </select>`
    //apikeyHTML += `&nbsp;&nbsp;&nbsp;&nbsp;API Key: <input size = "130" name="apikey"></font>`
    apikeyHTML += `<br><br><input type="submit"></form> \n`;
    apikeyHTML += `            </font></body> \n`;
    apikeyHTML += `<script> \n`;
    apikeyHTML += `        socket.on('update-list', function(data) { \n`;
    apikeyHTML += `             console.log('data = ' + data); \n`;
    apikeyHTML += `        }); \n`;
    apikeyHTML += `</html> \n`;
    return apikeyHTML;
}

//***********************************************************************************************
//Live dashboard HTML page
//***********************************************************************************************
function getLiveHTML() {
    let liveHTML = `<html> \n`;
    liveHTML += `    <style type="text/css"> \n `;
    liveHTML += `        body { \n `;
    liveHTML += `            font-family:Andale Mono; \n `;
    liveHTML += `            color: lime; \n `;
    liveHTML += `        } \n `;
    liveHTML += `    </style>     \n `;
    liveHTML += `    <script src='/socket.io/socket.io.js'></script> \n`;
    liveHTML += `        <body bgcolor="black"> \n`;
    //liveHTML += `    <div style="background-image:url(https://blog.paloaltonetworks.com/wp-content/uploads/2016/06/Unit-42-CTF-1.png); \n`;
    //liveHTML += `                background-repeat:no-repeat; \n`;
    //liveHTML += `                background-attachment:fixed; \n`;
    //liveHTML += `                background-position: center center; \n`;
    //liveHTML += `                overflow-y: auto; overflow-x: hidden; \n`;
    //liveHTML += `                line-height:2em;">\n \n`;
    liveHTML += `            <center><br><table id="scoreboard" style ='font-size: 45px'>\n `;
    liveHTML += `                <tr><th>&nbsp;&nbsp;[&nbsp;&nbsp;Player Name&nbsp;&nbsp;]&nbsp;&nbsp;</th> \n`;
    liveHTML += `                <th valign="top">&nbsp;&nbsp;[&nbsp;&nbsp;Score&nbsp;&nbsp;]&nbsp;&nbsp;</th></tr> \n`;
    for (let i = 0; i < players; i++) {
        //liveHTML += `                <tr><td align="center"><font color="lime" size="36"><span id="${i}-name">${eval("player" + i + ".name")}</span></font></td><td align="center"><font color="lime" size="36"><span id="${i}-score">${eval("player" + i + ".score")}</span></font></td></tr>`;
        liveHTML += `                <tr><td align="center" id="${i}-name" style='padding-top:20px;padding-bottom:20px;'>${eval("player" + i + ".name")}</td><td align="center" id="${i}-score">${eval("player" + i + ".score")}</td></tr> \n`;
    }
    //liveHTML += `                <tr><td><font color="lime">&nbsp;&nbsp;[&nbsp;&nbsp;<span id="playerName">default</span></font></td> \n`;
    //liveHTML += `                <td valign="top"><font color="lime">&nbsp;&nbsp;|&nbsp;&nbsp;Score<span id="totalScore">0</span>&nbsp;&nbsp;]</font></td></tr>`;
    liveHTML += `            </font></table></center> \n`;
    liveHTML += `            </body></div> \n`;
    liveHTML += `    <script> \n`;
    liveHTML += `        console.log(\`number of players = ${players}\`); \n`;
    liveHTML += `        var socket = io(); \n`;
    liveHTML += `        function sortTable() { \n`;
    liveHTML += `          var table, rows, switching, i, x, y, shouldSwitch; \n`;
    liveHTML += `          table = document.getElementById("scoreboard"); \n`;
    liveHTML += `          switching = true; \n`;
    liveHTML += `          while (switching) { \n`;
    liveHTML += `            switching = false; \n`;
    liveHTML += `            rows = table.rows; \n`;
    liveHTML += `            console.log('rows'); \n`;
    liveHTML += `            console.log(rows); \n`;
    liveHTML += `            for (i = 1; i < (rows.length - 1); i++) { \n`;
    liveHTML += `              shouldSwitch = false; \n`;
    liveHTML += `              x = rows[i].getElementsByTagName("TD")[1]; \n`;
    liveHTML += `              y = rows[i + 1].getElementsByTagName("TD")[1]; \n`;
    liveHTML += `              console.log('x = '); \n`;
    liveHTML += `              console.log(x); \n`;
    liveHTML += `              console.log('y = '); \n`;
    liveHTML += `              console.log(y); \n`;
    liveHTML += `              console.log('x.innerHTML = '); \n`;
    liveHTML += `              console.log(x.innerHTML.toLowerCase()); \n`;
    liveHTML += `              console.log('y.innerHTML = '); \n`;
    liveHTML += `              console.log(y.innerHTML.toLowerCase()); \n`;
    liveHTML += `              if (x.innerHTML < y.innerHTML) { \n`;
    liveHTML += `                shouldSwitch = true; \n`;
    liveHTML += `                break; \n`;
    liveHTML += `              } \n`;
    liveHTML += `            } \n`;
    liveHTML += `            if (shouldSwitch) { \n`;
    liveHTML += `              rows[i].parentNode.insertBefore(rows[i + 1], rows[i]); \n`;
    liveHTML += `              switching = true; \n`;
    liveHTML += `            } \n`;
    liveHTML += `          } \n`;
    liveHTML += `        } \n`;
    liveHTML += `        socket.on('dashUpdate', function(data) { \n`;
    liveHTML += `            function updateDashboard(playerNum, playerName, playerScore) { \n`;
    liveHTML += `                let namePosition = \`$\{playerNum\}-name\`;`;
    liveHTML += `                let scorePosition = \`$\{playerNum\}-score\`;`;
    //liveHTML += `                console.log('position = ' + position);`;
    liveHTML += `                document.getElementById(namePosition).textContent=playerName; \n`;
    liveHTML += `                document.getElementById(scorePosition).textContent=playerScore; \n`;
    liveHTML += `                sortTable(); \n`;
    liveHTML += `            } \n`;
    liveHTML += `            let playerNum = data.dashUpdate[0]; \n`;
    liveHTML += `            let playerName = data.dashUpdate[1]; \n`;
    liveHTML += `            let playerScore = data.dashUpdate[2]; \n`;
    liveHTML += `            console.log(\`playernum = $\{playerNum\} && playerName = $\{playerName\} && playerScore = $\{playerScore\} \`); \n`;
    liveHTML += `            updateDashboard(playerNum, playerName, playerScore); \n`;
    liveHTML += `        }); \n`;
    liveHTML += `        socket.on('error', console.error.bind(console)); \n`;
    liveHTML += `        socket.on('message', console.log.bind(console)); \n`;
    liveHTML += `            console.log(\`name = $\{name\}\`); \n`;
    liveHTML += `    </script> \n`;
    liveHTML += `</html> \n`;
    return liveHTML;
}

//***********************************************************************************************
//Generate and retreive API keys from firewalls
//***********************************************************************************************
async function generateFWApiKeys() {

    let fwHTTPSOptions = {
        host : 'pa-vm0', // here only the domain name
        port : 443,
        // https://192.168.55.10/api/?type=keygen&user=admin&password=admin
        path: `/api/?type=keygen&user=admin&password=Password1`, 
        method : 'GET',
        timeout: 5000
    };

    let apiKey;

    function httpsCall(playerNum) {
        let message;
        return new Promise((resolve, reject) => {
            let output = '';

            fwHTTPSOptions.host = `PA-VM${playerNum}`;

            const req = https.request(fwHTTPSOptions, res => {
                //console.log(`status code = `);
                //console.log(res.statusCode);
                if (res.statusCode == 403) {
                    message = `Received HTTP forbidden 403 message from ${fwHTTPSOptions.host}. Password has either been changed or NGFW is not fully booted up. REINITIALIZE PLAYERS.`;
                    console.log(message);
                    io.emit('player', { message: [message] });
                } else if (res.statusCode == 200) {
                    message = `API key successfully received for ${fwHTTPSOptions.host}.`;
                    console.log(message);
                    io.emit('player', { message: [message] });
                } else {
                    throw(`Status code not OK. Expecting 200 but received ${res.statusCode}.\nhost = ${fwHTTPSOptions.host}, path = ${fwHTTPSOptions.path}`);
                }

                res.on('data', d => {
                    output += d.toString(); 
                    //process.stdout.write(d)
                });

                res.on('end', function() {
                    //console.log(output)
                    resolve(output);
                });

                req.on('socket', function (socket) {
                    socket.setTimeout(myTimeout);  
                    socket.on('timeout', function() {
                        req.abort();
                    });
                });                

            });

            req.on('timeout', () => {
                message = `API call to ${fwHTTPSOptions.host} timed out. Please validate that the firewall is fully accessible and REINITIALIZE PLAYERS.`;
                console.log(message);
                io.emit('player', { message: [message] });
                req.abort();
            });

            //     io.emit('player', { message: messages });
            req.on('error', error => {
                if (error.code == 'ECONNREFUSED') {
                    console.log(`API call to ${fwHTTPSOptions.host} was refused.`);
                    io.emit('player', { message: [`API call to ${fwHTTPSOptions.host} was refused.`] });
                }
                console.log(`Full error message:`);
                console.error(error)
            });

            req.end();
        });
    }


    for (var i = 0; i < players; i++) { //instantiate players 0-9 (will map to VM-FW0 - VM-FW9)
        //console.log(eval("player" + i + ".api"));

        let XMLresult = await httpsCall(i);
        //console.log(XMLresult);
        parseString(XMLresult, { explicitArray : false }, function(err,result){
            apiKey = result.response.result.key;
            //console.log(result.response.result.key);
        });

        eval(`player${i}.apikey = "${apiKey}"`);
        //console.log(eval(`player${i}.apikey`));
        //eval("player" + i + ".api = 0");
        //console.log(player`${i}`.api);
    }
}

async function deleteConfigHistory() {

    let output = '';

    for (var i = 0; i < players; i++) { 

        let apikey = eval("player" + i + ".apikey");
        let firewall = `PA-VM${i}`;

        var optionsgetmsg = {            
            host: firewall, // here only the domain name
            port: 443,
            path: `/api/?type=op&cmd=<delete><config-audit-history><%2Fconfig-audit-history><%2Fdelete>&key=${apikey}`,
            method : 'GET' // do GET
        };

        //console.log(optionsgetmsg);

        var reqGet = https.request(optionsgetmsg, function(resp) { // make API call      
            resp.on('data', function(d) {
                output += d.toString(); //take all the output 
            }); // receive data in chunks
            resp.on('end', function() { // process output

                function checkConfigRemovalStatus(result) {
                    if (typeof(result['$'].status) !== undefined)
                        if (result['$'].status === 'success')
                            return 'Saved config(s) removed.';
                        else
                            return 'No saved configs found.';
                }

                parseString(output, { explicitArray : false }, function(err,result){ 
                    console.log(`Removing old commit histories from previous sessions for ${firewall}: ` + checkConfigRemovalStatus(result.response));
                    //try { console.log(`${result.response['$'].status} - ${result.response.msg.line}`); }
                    //catch(error) { console.log(result.response); }
                });
                output = '';

            });
        });       
        reqGet.end();
        reqGet.on('error', function(e) {
            console.error(`Error: ${e}`);
        }); 

    }
}

//***********************************************************************************************
//Live Board, Admin and Player Dashboard pages
//***********************************************************************************************
var appLiveBoard = sockets.createServer(function(req, res) {
    const pathName = url.parse(req.url, true).pathname;
    liveBoardHTML = getLiveHTML();

    if (typeof(player0) !== 'undefined') {
        if (req.method === 'GET') {
            res.writeHead(200, {'Content-Type': 'text/html'});
            console.log(`pathName = ${pathName}`);
            res.end(liveBoardHTML);
        }
    } else
        res.end(init);
    req.on('end', function () {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(liveBoardHTML);
        //res.writeHead(200);
        //    res.end(postHTML);
    });

});
// Admin websockets page
var appAdmin = sockets.createServer(function(req, res) {
    const pathName = url.parse(req.url, true).pathname;

    // Just for debugging - print out any querystring variables
    //for(q in queryString['query']) {
    //    console.log( q + " = " + queryString['query'][q]);
    //}

    if (pathName === '/background.jpg') {
        if (req.url.match("\.jpg$")) {
            console.log('background hit');
            var imagePath = path.join(__dirname, '/images', req.url);
            console.log(imagePath);
            var fileStream = fs.createReadStream(imagePath);
            res.writeHead(200, {"Content-Type": "image/jpg"});
            fileStream.pipe(res);
        }
    }

    if (pathName === '/admin') {
        if (req.method === 'POST') {
            var body = "";
            req.on('data', function (chunk) {
                body += chunk.toString();
            });
        } else if (req.method === 'GET') {
            const queryString = url.parse(req.url, true).query;

            // dynamic debugs without having to restart the program and therefore lose player spots and all that jazz
            if (typeof(queryString.debug) !== 'undefined') {
                let debugArr = queryString.debug.split(",");
                let debugMsg = `Received command to enable debug for: ${debugArr}`;
                console.log(debugMsg);
                io.emit('player', { message: [debugMsg] });
                for (arr in debugArr)
                    eval(`${debugArr[arr]} = true;`);
            }
            if (typeof(queryString.nodebug) !== 'undefined') {
                let noDebugArr = queryString.nodebug.split(",");
                let nodebugMsg = `Received command to disable debug for: ${noDebugArr}`;
                console.log(nodebugMsg);
                io.emit('player', { message: [nodebugMsg] });
                for (arr in noDebugArr)
                    eval(`${noDebugArr[arr]} = false;`);
            }

            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(admin);
        }
        req.on('end', function () {
            if (typeof(body) !== 'undefined') {
                console.log('POSTed: ' + body);
                let parsed = parse(body);
                //console.log(parsed);
                //console.log(parsed.numPlayers);
                if (parsed.numPlayers <= 10)
                    console.log(`number is less than or equal to 10`);
                if ((typeof(parsed.numPlayers) !== 'undefined' && isNaN(parsed.numPlayers) !== true) && parsed.numPlayers <= 10) {
                    console.log(`input contains a number`);
                    players = parsed.numPlayers;
                    playerInit(players);
                    playAudio(startGame);
                    generateFWApiKeys(); // programatically generate and store player API keys.

                } else {
                    let invalidPlayerNumberMsg = 'Number of players entered was empty or did not contain only numbers or the number entered was greater than 10.';
                    console.log(invalidPlayerNumberMsg);
                    io.emit('player', { message: [invalidPlayerNumberMsg] });
                }
                //console.log(`typeof numPlayers = ${typeof(parsed.numPlayers)}`)
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.end(admin);
                //res.writeHead(200);
                //    res.end(postHTML);
            }
        });
    } else {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(index);
    }
});
// Player websockets page
var appPlayer = sockets.createServer(function(req, res) {
    //console.log(`******** appPlayer called`);

    const pathName = url.parse(req.url, true).pathname;
    let playerPosition;

    if (pathName === "/favicon.ico") {
        res.writeHead(404, { 'Content-type': 'text/html' });
        res.end('<http><h1>wat! 404</h1>lolz<img src="http://i.imgur.com/iMtqa1d.jpeg" width="300"></http>');
    }

    let clientIpAddress = req.connection.remoteAddress
    //console.log(`Player connecting in from : ${clientIpAddress}`);


    if (req.method === 'POST') {
        console.log(`getting chunked data`);
        var bodyPlayer = "";
        req.on('data', function (chunk2) {
            bodyPlayer += chunk2.toString();
        });
    } else if (req.method === 'GET') {

        if (pathName === '/images.html') {
            res.writeHead(200, {'Content-Type': 'text/html'});

            res.end(images);
            return;

        } else if (req.url.match("\.jpg$")) {
            var imagePath = path.join(__dirname, 'images', req.url);
            var fileStream = fs.createReadStream(imagePath);
            res.writeHead(200, {"Content-Type": "image/jpg"});
            fileStream.pipe(res);
        } else {
            res.writeHead(200, {'Content-Type': 'text/html'});

            if (typeof(player0) === 'undefined') {
                console.log(`Player-0 connnecting in but has not been defined, calling init`);
                res.end(init);
                return;

            } else if (playerIPs.includes(clientIpAddress)) {
                console.log(`Position of player with IP is ${playerIPs.indexOf(clientIpAddress)}`);
                playerNum = Number(playerIPs.indexOf(clientIpAddress));
                playerHTML = getPlayerHTML(playerNum);
                res.end(playerHTML);
                playerDashboardInit(playerNum);
            }

            if (playerIPs.indexOf(clientIpAddress) != -1 ) {
                let apikey = eval("player" + playerNum + ".apikey");
                console.log('wut');

                if (typeof(player0) != 'undefined') {// make sure there is at least one player initialized
                    playerDashboardInit(playerNum);
                }

            }
            // ?????? added new chunk below

            if (playerIPs.indexOf(clientIpAddress) == -1 ) {
                //console.log(`IP not previously seen. Pushing IP into array`);
                playerIPs.push(clientIpAddress);
                playerNum = Number(playerIPs.indexOf(clientIpAddress));
                playerHTML = getPlayerHTML(playerNum);
                res.end(playerHTML);
                playerDashboardInit(playerNum);
            }
        }
    } else {
        res.statusCode = 501;
        res.setHeader('Content-Type', 'text/plain');
        return res.end('Method not implemented');
    }


    req.on('end', function () {
        //console.log(`******** req.on - end called`);

        if (typeof(bodyPlayer) !== 'undefined' && typeof(playerNum) !== 'undefined') {
            //console.log('POSTed: ' + bodyPlayer);
            bodyPlayerParsed = parse(bodyPlayer);
            if (typeof(bodyPlayerParsed.playerName) !== 'undefined' && playerNum != -1) {
                //console.log(bodyPlayerParsed.playerName);
                playerNum = Number(playerIPs.indexOf(clientIpAddress));

                eval("player" + playerNum + ".name = bodyPlayerParsed.playerName"); 
                console.log(`Player-${playerNum} changed name to ${eval("player" + playerNum + ".name")}`);
                ioLiveBoard.emit('dashUpdate', { dashUpdate: [playerNum, eval("player" + playerNum + ".name"), eval("player" + playerNum + ".score")]});
                //ioPlayer.emit('player-name', { name: [playerNum, bodyPlayerParsed.playerName] });
            }
        }

        res.writeHead(200, {'Content-Type': 'text/html'});
        if (playerIPs.indexOf(clientIpAddress) != -1) {
            //console.log(`req.on - end`);
            playerNum = playerIPs.indexOf(clientIpAddress);
            //console.log(`playerNum = ${playerNum}`);
            playerHTML = getPlayerHTML(playerNum);
            res.end(playerHTML);

            function myFunction() {
                playerDashboardInit(playerNum);
                ioPlayer.emit(`player${playerNum}-setFirewallIP`, { firewallip: eval("pavm" + playerNum + "_ip") }); //pavm0_ip
            }

            // stop for sometime if needed
            setTimeout(myFunction, 1000);

        } else
            res.end(init);
    }); 
});

// Socket.io server listens to our app
var io = require('socket.io').listen(appAdmin);
var ioPlayer = require('socket.io').listen(appPlayer);
var ioLiveBoard = require('socket.io').listen(appLiveBoard);

// Game Admin Dashboard
io.on('connection', function(socket) {
    // Use socket to communicate with this particular client only, sending it it's own id
    if (gameStart != true)
        socket.emit('welcome', { message: 'Welcome! Please make sure you receive a message indicating that all API keys were successfully generated. REINITIALIZE PLAYERS again if there is a problem.', id: socket.id });

    socket.on('numberOfPlayers', function(data) {
        //console.log(data);

        async function gameInit() {
            await generateFWApiKeys(); // programatically generate and store player API keys.
            await deleteConfigHistory();
        }

        function startGameTimer() {
            gameTimeRemaining = gameTimeInSeconds;
            let refreshIntervalId = setInterval(() => {
                if (gameTimeRemaining % 60 === 0) {
                    //console.log(`Minutes remaining: ${gameTimeRemaining/60}`);
                    syncDashboardGameTimers(gameTimeRemaining/60);
                }
                switch(gameTimeRemaining) {
                    case 300:
                        playAudio('5_minute_warning');
                        break;
                    case 180:
                        playAudio('3_minutes_remain');
                        break;
                    case 120:
                        playAudio('2_minutes_remain');
                        break;
                    case 60:
                        playAudio('1_minute_remains');
                        break;
                    case 30:
                        playAudio('30_seconds_remain');
                        break;
                    case 0:
                        playAudio('winner');
                        clearInterval(refreshIntervalId);
                        gameStart = false;
                        syncDashboardGameTimers(0);
                        io.emit('updateTime', { gameEnd: true });
                        for (let i = 0; i < players; i++)
                            ioPlayer.emit(`player${i}-updateTime`, { gameEnd: true });
                        break;  
                }
                gameTimeRemaining = gameTimeRemaining - 5;
            }, 5000);
        }

        async function syncDashboardGameTimers(minutes) {
            io.emit('updateTime', { remainingMinutes: minutes });
            for (let i = 0; i < players; i++) {
                ioPlayer.emit(`player${i}-updateTime`, { remainingMinutes: minutes });
            }
        }

        let numberOfPlayers = data.numberOfPlayers;
        if (typeof((numberOfPlayers) !== 'undefined' && isNaN(numberOfPlayers) !== true) && numberOfPlayers <= 10 && numberOfPlayers != '') {
            console.log(`Number of players initialized: ${numberOfPlayers}`);
            gameStart = true;
            players = numberOfPlayers;
            playerInit(players);
            playAudio(startGame);
            gameInit();
            startGameTimer();
        } else {
            let invalidPlayerNumberMsg = 'ERROR: Number of players entered was invalid. It contained something other than numbers or the number entered was greater than 10.';
            console.log(invalidPlayerNumberMsg);
            io.emit('player', { message: [invalidPlayerNumberMsg] });
        }
    });
});

ioPlayer.on('connection', (socket) => {
    //console.log(`**********************connection established`);
    socket.on('hint', function(data) {
        let playerNum = playerIPs.indexOf(socket.request.connection._peername.address);
        //console.log(data);
        //console.log( socket.request.connection._peername.address);
        //console.log(`player ID = `);
        //console.log(playerIPs.indexOf(socket.request.connection._peername.address));
        //console.log(data.hintID);
        let hints = scenario.hints();
        let hint;
        let hintPoints;
        for (hintNum in hints) 
            if (hints[hintNum][0] === data.hintID) {
                hint = hints[hintNum][2];
                hintPoints = hints[hintNum][3];
            }

        // check if hint has already been claimed and add to client list if not
        if (playerNum !== -1) {
            let usedHints = eval("player" + playerNum + ".hintsUsed");
            let ioHintMsg = `Player-${playerNum} lost ${hintPoints} point(s) for used hint: ${hint}.`;
            io.emit('player', { message: [ioHintMsg] });

            if (usedHints.indexOf(data.hintID) === -1) {
                eval("player" + playerNum + ".hintsUsed.push(data.hintID)");
                updateScoreBoard(['', hintPoints, '', ''], `PA-VM${playerNum}`);  
            }

            ioPlayer.emit(`player${playerNum}-hint`, { hint: hint, points: hintPoints } );

        } else {
            let hintError = 'Received hint request, but game has not been initialized!';
            console.log(hintError);
        }
    });
    socket.on('updatePlayerName', function(data) {       
        let clientIpAddress = socket.request.connection._peername.address;
        if (data.playerName.match(/^[A-Za-z]+$/) && data.playerName.length <= 15) {
            //console.log(`client iP = ${clientIpAddress}`);
            playerNum = Number(playerIPs.indexOf(clientIpAddress));
            eval("player" + playerNum + ".name = data.playerName"); 
            console.log(`Player-${playerNum} changed name to ${eval("player" + playerNum + ".name")}`);
            ioPlayer.emit(`player${playerNum}-updatePlayerName`, { playerName: data.playerName });
            ioLiveBoard.emit('dashUpdate', { dashUpdate: [playerNum, eval("player" + playerNum + ".name"), eval("player" + playerNum + ".score")]});
        } else {
            try { ioPlayer.emit(`player${playerNum}-alert`, { alert: 'You either entered invalid characters for your Player Name, or is longer than 15 characters. Only letters are supported.' }); }
            catch(error) { }
        }
    }); 
});

appAdmin.listen(3000);
appPlayer.listen(3001);
appLiveBoard.listen(5000);

//***********************************************************************************************
//Player Init
//***********************************************************************************************
function playerInit(numPlayers) {
    var Player = function(name, score, missions, apikey, hintsUsed) {
        this.name = name;
        this.score = score;
        this.missions = missions;
        this.apikey = apikey;
        this.hintsUsed = hintsUsed;
    }

    let messages = [];

    for (var i = 0; i < numPlayers; i++) { //instantiate players 0-9 (will map to VM-FW0 - VM-FW9)
        eval("player" + i + " = new Player()");
        eval("player" + i + ".name = 'Player-" + i + "'");
        eval("player" + i + ".score = 0");
        eval("player" + i + ".missions = []");
        eval("player" + i + ".apikey = 0");
        eval("player" + i + ".hintsUsed = []");

        messages.push(`Initializing PA-VM${i}.`);
    }
    io.emit('player', { message: messages });
}

//***********************************************************************************************
//Update Scoreboard
//***********************************************************************************************
function updateScoreBoard(arr, fwName) { // [0] = message [1] = points
    let [playerNum, playerName] = [0, ''];
    let [messages, points, completedTasks, removedTasks] = [arr[0], arr[1], arr[2], arr[3]];

    if (scoreBoardDebug ) console.log(`player${playerNum} : total = ${eval("player" + playerNum + ".score")}`);
    if (scoreBoardDebug ) console.log(eval("player" + playerNum));

    points = Number(points);
    playerNum = fwName.match(/[0-9]{1,}$/);
    playerNum = Number(playerNum);
    playerName = eval("player" + playerNum + ".name");
    eval("player" + playerNum + ".missions.push(completedTasks)");

    if (scoreBoardDebug ) console.log(`${points} point(s) awarded to player${playerNum}`);
    eval("player" + playerNum + ".score += points"); // keep adding points for the player everytime func is executed

    if (points != 0) {
        // For Audio

        if (prevPlayerNum === playerNum && points > 0 ) { // for audio criteria, need to know if the same player scored multiple times in a row
            playerStreak += 1;
            console.log(`Current player streak: ${playerStreak}`);
        } else {
            playerStreak = 1;
        }
        prevPlayerNum = playerNum;
        //console.log(`player completed ${completedTasks.length} tasks`);

        // end of Audio section

        if (completedTasks !== '')
            ioPlayer.emit(`player${playerNum}-complete`, { complete: completedTasks });
        if (removedTasks !== '')
            ioPlayer.emit(`player${playerNum}-uncomplete`, { uncomplete: removedTasks });
        ioPlayer.emit(`player${playerNum}-totalScore`, { totalScore: eval("player" + playerNum + ".score") });

        ioLiveBoard.emit('dashUpdate', { dashUpdate: [playerNum, eval("player" + playerNum + ".name"), eval("player" + playerNum + ".score")]});
        console.log(`PA-VM${playerNum} / ${playerName} scored ${points} : total = ${eval("player" + playerNum + ".score")}`);

        if (messages !== '')
            io.emit('player', { message: messages });
        io.emit('player', { message: [`PA-VM${playerNum} / ${playerName} scored ${points} : total = ${eval("player" + playerNum + ".score")}`] });

        // audio logic here:

        let soundPlayed = false; // make sure we only play one sound per commit
        if (firstBlood === false) {
            soundPlayed = playAudio('first_blood');
            ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'First Blood!' });
            firstBlood = true;
        }
        /*
        console.log(`****************************`);
        console.log(`     Before Leaderboard`);
        console.log(`****************************`);
        console.log(`Current Leader  : ${scoreboardLeader.currentLeader}`);
        console.log(`Current Score   : ${scoreboardLeader.currentHighScore}`);
        console.log(`Previous Leader : ${scoreboardLeader.previousLeader}`);
        console.log(`Previous Score  : ${scoreboardLeader.previousHighScore}`);
        console.log(`****************************\n`);
        */

        //console.log(`Player scores are:`);
        for (let i = 0; i < players; i++) {
            let score = eval("player" + i + ".score");
            let currentPlayer = `player${i}`;
            if (score > scoreboardLeader.currentHighScore) {

                if (soundPlayed === false && scoreboardLeader.previousLeader !== '' && scoreboardLeader.previousLeader == currentPlayer) {
                    soundPlayed = playAudio('retribution');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Retribution!' });
                } 

                if (scoreboardLeader.currentLeader != currentPlayer) {
                    scoreboardLeader.previousHighScore = scoreboardLeader.currentHighScore;
                    scoreboardLeader.previousLeader = scoreboardLeader.currentLeader;
                    scoreboardLeader.currentLeader = currentPlayer;
                }

                scoreboardLeader.currentHighScore = score;

                if (soundPlayed === false && scoreboardLeader.currentLeader !== currentPlayer) {
                    soundPlayed = playAudio('taken_the_lead');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Taken the lead!' });
                    ioPlayer.emit(`player${scoreboardLeader.previousLeader}-announcement`, { announcement: 'Lost the lead!' });
                }
            }
            //console.log('Player-' + i + ': ' + eval("player" + i + ".score"));
        }

        /*
        console.log(`****************************`);
        console.log(`     After Leaderboard`);
        console.log(`****************************`);
        console.log(`Current Leader  : ${scoreboardLeader.currentLeader}`);
        console.log(`Current Score   : ${scoreboardLeader.currentHighScore}`);
        console.log(`Previous Leader : ${scoreboardLeader.previousLeader}`);
        console.log(`Previous Score  : ${scoreboardLeader.previousHighScore}`);
        console.log(`****************************\n`);
        */

        if (soundPlayed === false)
            switch(playerStreak) {
                case 0:
                    break;
                case 1: 
                    break;
                case 2: // Two items - double_kill
                    soundPlayed = playAudio('Top_Gun');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Top Gun!' });
                    break;
                case 3: // Three items - tripple_kill
                    soundPlayed = playAudio('Hattrick!');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Hattrick!' });
                    break; // no class found
                case 4: // Four items - ultra_kill
                    soundPlayed = playAudio('unstoppable');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Unstoppable!' });
                    break;
                default:
                    soundPlayed = playAudio('dominating');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Dominating!!' });
                    break;
            }

        // high pointers
        if (soundPlayed === false)
            switch(points) {
                case 20:
                    soundPlayed = playAudio('FlackMonkey');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Flack Monkey!' });
                    break;
                case 50:
                    soundPlayed = playAudio('Monsterkill_F');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Monster Kill!' });
                    break;
                default:
                    break;
            }

        // how many items they completed in a single commit
        if (soundPlayed === false)
            switch(completedTasks.length) {
                case 0:
                    break;
                case 1: 
                    break;
                case 2: // Two items - double_kill
                    soundPlayed = playAudio('double_kill');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Double Kill!' });
                    break;
                case 3: // Three items - tripple_kill
                    soundPlayed = playAudio('triple_kill');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Triple Kill!' });
                    break; // no class found
                case 4: // Four items - ultra_kill
                    soundPlayed = playAudio('ultra_kill');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Ultra Kill!' });
                    break;
                default:
                    soundPlayed = playAudio('combo_whore');
                    ioPlayer.emit(`player${playerNum}-announcement`, { announcement: 'Combo Whore!' });
                    break;
            }

        soundPlayed = false; // reset soundPlayed so it will play next time

        /*

        - Level 1 and Level 2 at same time
        flack monkey

        - Three separate commits with points in a row
        hattrick

        - Four separate commits with points in a row
        slaughter

        - Five separate commits with points in a row
        bloodbath
        */

    }
}

//***********************************************************************************************
//Audio section
//***********************************************************************************************
function playAudio(wavFile) {
    var options = {
        hostname: soundServer,
        port: soundServerPort,
        //path: '/playSound',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    var req = http.request(options, function(res) {
        //console.log('Status: ' + res.statusCode);
        //console.log('Headers: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (body) {
            //console.log('Body: ' + body);
        });
    });
    req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });

    let jsonData = { playSound: `${wavFile}.wav` };
    req.write(JSON.stringify(jsonData));
    //console.log(`Result is ${result}`);

    req.end();

    return true;
}

//***********************************************************************************************
//Tasks for Dashboard
//***********************************************************************************************
function playerDashboardInit(playerNum) {
    //console.log(`******** function playerDashboardInit called`);
    let tasks = scenario.tasks();
    let hints = scenario.hints();
    let completedMissions = []; 
    let hintsAvailable = [];
    let hintsPoints = [];

    // take hints array and remove the ones where they have already been used.

    // create a new array with just the question indicator that a hint is available so on the client side no data is present
    // present this array of question number (ie. q1) to the client's browser and to create a link that a hint is available for that question

    if (typeof(player0) !== 'undefined') {

        for (hintNum in hints) {
            if (hints[hintNum][2] != '') // 3'rd field in this array is the actual hint text. If it doesn't exist, then there is no hint.
                hintsAvailable.push(hints[hintNum][0]);
            hintsPoints.push(hints[hintNum][3]);
        }

        let hintsUsed = eval("player" + playerNum + ".hintsUsed");
        if (hintsUsed.length > 0) {
            for (arr in hintsUsed) {
                console.log(hintsUsed[arr]);
            }
        } //else
        //console.log(`No used hints for player ${playerNum}`);

        let score = eval("player" + playerNum + ".score");
        let name = eval("player" + playerNum + ".name");
        //console.log(`score = ${eval("player" + playerNum + ".score")}`);
        //let missions = eval("player" + i + ".missions");
        completedMissions = eval("player" + playerNum + ".missions");;
        //console.log(`api = ${eval("player" + playerNum + ".apikey")}`);
        //console.log(`completedMissions = `);
        //console.log(completedMissions);
        //console.log(`emitting player${playerNum}-dashUpdate`);
        ioPlayer.emit(`player${playerNum}-dashUpdate`, { tasks: [playerNum, name, tasks, score, completedMissions, hintsAvailable, hintsPoints] });
    } else {
        console.log(`Player-0 is undefined in dashboard init`);
    }
}

//***********************************************************************************************
//Device Config Parsing
//***********************************************************************************************
function processDeviceConfig(deviceConfig, fwName) {

    if (debug_deviceConfig) {
        console.log(`\n**************************************`);
        console.log(`Function: processDeviceConfig`);
        console.log(`**************************************\n`);
        console.log(`deviceConfig = `);
        console.log(deviceConfig);
        console.log(``);
    }

    // let points = 0;
    let wfSettingConfig; // XML path shortener variable
    let wfSettingConfigArr = []; // send an array for processing regardless if string or object
    let dynUpdateConfig; // XML path chortener variable

    // Wildfire Settings
    let [pe, apk, pdf, msoffice, jar, flash, macosx, archive, linux, script] = ['', '', '', '', '', '', '', '', '', ''];

    if(typeof(deviceConfig.setting) !== 'undefined')
        if(typeof(deviceConfig.setting.wildfire) !== 'undefined') 
            if(typeof(deviceConfig.setting.wildfire['file-size-limit']) !== 'undefined') 
                if(typeof(deviceConfig.setting.wildfire['file-size-limit'].entry) !== 'undefined')
                    wfSettingConfig = deviceConfig.setting.wildfire['file-size-limit'].entry;
    if (Object.prototype.toString.call(wfSettingConfig) === '[object Array]') {
        for (let arr in wfSettingConfig) {
            if (typeof(wfSettingConfig[arr]['$'].name) !== 'undefined' && typeof(wfSettingConfig[arr]['size-limit']) !== 'undefined') {
                if (wfSettingConfig[arr]['$'].name === 'pe')
                    pe = wfSettingConfig[arr]['size-limit'];
                if (wfSettingConfig[arr]['$'].name === 'apk')
                    apk = wfSettingConfig[arr]['size-limit'];
                if (wfSettingConfig[arr]['$'].name === 'pdf')
                    pdf = wfSettingConfig[arr]['size-limit'];
                if (wfSettingConfig[arr]['$'].name === 'ms-office')
                    msoffice = wfSettingConfig[arr]['size-limit']; 
                if (wfSettingConfig[arr]['$'].name === 'jar')
                    jar = wfSettingConfig[arr]['size-limit'];
                if (wfSettingConfig[arr]['$'].name === 'flash')
                    flash = wfSettingConfig[arr]['size-limit'];
                if (wfSettingConfig[arr]['$'].name === 'MacOSX')
                    macosx = wfSettingConfig[arr]['size-limit'];
                if (wfSettingConfig[arr]['$'].name === 'archive')
                    archive = wfSettingConfig[arr]['size-limit'];   
                if (wfSettingConfig[arr]['$'].name === 'linux')
                    linux = wfSettingConfig[arr]['size-limit'];  
                if (wfSettingConfig[arr]['$'].name === 'script')
                    script = wfSettingConfig[arr]['size-limit'];                
            }
        }
        scenario.scoringDeviceConfigWFConfig([pe, apk, pdf, msoffice, jar, flash, macosx, archive, linux, script], fwName);
    } else if (Object.prototype.toString.call(wfSettingConfig) === '[object String]') {
        wfSettingConfigArr[0] = wfSettingConfig;
        console.log(`Device Config - Wildfire Config: Max file sizes is a String (not valid?)`);
    }

    // Dynamic update settings
    let [threatInterval, threatInstall, avInterval, avInstall, wfInterval, wfInstall] = ['', '', '', '', '', ''];
    if (typeof(deviceConfig.system['update-schedule']) !== 'undefined') {
        dynUpdateConfig = deviceConfig.system['update-schedule'];
        if (typeof(dynUpdateConfig['threats']) !== 'undefined') {
            if (debug_deviceConfig) console.log(dynUpdateConfig['threats'].recurring);
            if (typeof(dynUpdateConfig['threats'].recurring) !== 'undefined')
                for (let prop in dynUpdateConfig['threats'].recurring) { // the interval is an object rather than a prop, so we need to loop to get it
                    threatInstall = dynUpdateConfig['threats'].recurring[`${prop}`].action;
                    threatInterval = prop;
                }
        }
        if (typeof(dynUpdateConfig['anti-virus']) !== 'undefined') {
            if (debug_deviceConfig) console.log(dynUpdateConfig['anti-virus'].recurring);
            if (typeof(dynUpdateConfig['anti-virus'].recurring) !== 'undefined')
                for (let prop in dynUpdateConfig['anti-virus'].recurring) { // the interval is an object rather than a prop, so we need to loop to get it
                    avInstall = dynUpdateConfig['anti-virus'].recurring[`${prop}`].action;
                    avInterval = prop;
                }
        }
        if (typeof(dynUpdateConfig['wildfire']) !== 'undefined') {
            if (debug_deviceConfig) console.log(dynUpdateConfig['wildfire'].recurring);
            if (typeof(dynUpdateConfig['wildfire'].recurring) !== 'undefined')
                for (let prop in dynUpdateConfig['wildfire'].recurring) { // the interval is an object rather than a prop, so we need to loop to get it
                    wfInstall = dynUpdateConfig['wildfire'].recurring[`${prop}`].action;
                    wfInterval = prop;
                }
        }
        scenario.scoringDynamicUpdateConfig([threatInterval, threatInstall, avInterval, avInstall, wfInterval, wfInstall], fwName);
    }

    // Banner settings
    if (typeof(deviceConfig.system['login-banner']) !== 'undefined') // FW removes XML banner from config if deleted, so it will be undefined
        scenario.scoringBannerConfig(deviceConfig.system['login-banner'], fwName);
    else if (typeof(deviceConfig.system['login-banner']) === 'undefined')
        scenario.scoringBannerConfig('', fwName); // send blank banner info to potentially remove points

    // CTD Queue exceed settings
    let [tcpBypassExceedQ, udpBypassExceedQ, allowHTTPRange, appIDexceedQ, extendedCapture] = ['', '', '', '', ''];

    if (typeof(deviceConfig.setting['ctd']) !== 'undefined') {
        if (typeof(deviceConfig.setting['ctd']['tcp-bypass-exceed-queue']) !== 'undefined') 
            tcpBypassExceedQ = deviceConfig.setting['ctd']['tcp-bypass-exceed-queue'];
        if (typeof(deviceConfig.setting['ctd']['udp-bypass-exceed-queue']) !== 'undefined') 
            udpBypassExceedQ = deviceConfig.setting['ctd']['udp-bypass-exceed-queue'];
        if (typeof(deviceConfig.setting['ctd']['allow-http-range']) !== 'undefined')
            allowHTTPRange = deviceConfig.setting['ctd']['allow-http-range'];
        if (typeof(deviceConfig.setting['ctd']['extended-capture-segment']) !== 'undefined')
            extendedCapture = deviceConfig.setting['ctd']['extended-capture-segment'];
    }
    if (typeof(deviceConfig.setting['application']) !== 'undefined') 
        if (typeof(deviceConfig.setting['application']['bypass-exceed-queue']) !== 'undefined') 
            appIDexceedQ = deviceConfig.setting['application']['bypass-exceed-queue'];
    scenario.scoringCTDConfig([tcpBypassExceedQ, udpBypassExceedQ, allowHTTPRange, appIDexceedQ, extendedCapture], fwName);

    // Custom Logo
    let [customLogoMainUI, customLogoLoginUI] = ['', ''];

    if (typeof(deviceConfig.setting['custom-logo']) !== 'undefined') {
        if (typeof(deviceConfig.setting['custom-logo']['main-ui']) !== 'undefined') 
            if (typeof(deviceConfig.setting['custom-logo']['main-ui']['content']) !== 'undefined')
                customLogoMainUI = deviceConfig.setting['custom-logo']['main-ui']['content'];
        if (typeof(deviceConfig.setting['custom-logo']['login-screen']) !== 'undefined') 
            if (typeof(deviceConfig.setting['custom-logo']['login-screen']['content']) !== 'undefined')
                customLogoLoginUI = deviceConfig.setting['custom-logo']['login-screen']['content'];
    }
    scenario.scoringCustomLogo([customLogoMainUI, customLogoLoginUI], fwName);

    if (debug_deviceConfig) {
        console.log(`\n**************************************`);
        console.log(`END Function: processDeviceConfig`);
        console.log(`**************************************\n`);
    }
}

//***********************************************************************************************
//Vsys Setting Config Parsing (forward decrypted content)
//***********************************************************************************************
function processEntrySettingsConfig(vsysSettings, fwName) {
    if (debug_settingConfig) {
        console.log(`\n**************************************`);
        console.log(`Function: processEntrySettingsConfig`);
        console.log(`**************************************\n`);
        console.log(`vsysSettings = `);
        console.log(vsysSettings);
        console.log(``);
    }

    if (typeof(vsysSettings['ssl-decrypt']) !== 'undefined')
        if (typeof(vsysSettings['ssl-decrypt']['allow-forward-decrypted-content']) !== 'undefined')
            scenario.processForwardDecryptContent(vsysSettings['ssl-decrypt']['allow-forward-decrypted-content'], fwName);

}

//***********************************************************************************************
//Shared Config Parsing
//***********************************************************************************************
function processSharedConfig(sharedConfig, fwName) {
    // this needs to be processed before Management Config section. Role needs to be correlated to the user, which is processed after in processManagementConfig function
    //let points = 0;
    let overrideAppName;
    let overrideAppRisk;
    let [certName, ca, privateKey ] = [[], [], []];

    let [forwardTrustName, forwardUntrustName] = ['', '']; 

    if (debug_sharedConfig) {
        console.log(`\n**************************************`);
        console.log(`Function: processSharedConfig`);
        console.log(`**************************************\n`);
        console.log(`sharedConfig = `);
        console.log(sharedConfig);
        console.log(``);
    }

    if (typeof(sharedConfig['admin-role']) !== 'undefined')
        if (Object.prototype.toString.call(sharedConfig['admin-role']) === '[object Object]') {
            if (typeof(sharedConfig['admin-role'].entry) !== 'undefined')
                scenario.scoringAdminRoles(sharedConfig['admin-role'], fwName); // no points, need to correlate after with user account
        } else if (Object.prototype.toString.call(sharedConfig['admin-role']) === '[object Array]') {
            for (let arr in sharedConfig['admin-role']) {
                scenario.scoringAdminRoles(sharedConfig[arr]['admin-role'], fwName); 
            }
        }

    if (typeof(sharedConfig.override) !== 'undefined')
        if (typeof(sharedConfig.override.application) !== 'undefined') {
            if (Object.prototype.toString.call(sharedConfig.override.application.entry) === '[object Object]') {
                overrideAppName = sharedConfig.override.application.entry['$'].name;
                overrideAppRisk = sharedConfig.override.application.entry.risk;
                scenario.scoringOverrideApps(overrideAppName, overrideAppRisk, fwName); // no points, need to correlate after with user account
            }
            if (Object.prototype.toString.call(sharedConfig.override.application.entry) === '[object Array]') 
                for (let arr in sharedConfig.override.application.entry) {
                    overrideAppName = sharedConfig.override.application.entry[arr]['$'].name;
                    overrideAppRisk = sharedConfig.override.application.entry[arr].risk;
                    scenario.scoringOverrideApps(overrideAppName, overrideAppRisk, fwName); // no points, need to correlate after with user account
                }
        }

    // only 1 forwardTrust cert can be configured    
    if (typeof(sharedConfig['ssl-decrypt']) !== 'undefined')
        if (typeof(sharedConfig['ssl-decrypt']['forward-trust-certificate']) !== 'undefined' && Object.prototype.toString.call(sharedConfig['ssl-decrypt']['forward-trust-certificate']) == '[object Object]')
            if (typeof(sharedConfig['ssl-decrypt']['forward-trust-certificate'].rsa) !== 'undefined' && Object.prototype.toString.call(sharedConfig['ssl-decrypt']['forward-trust-certificate'].rsa ) == '[object String]')
                forwardTrustName = sharedConfig['ssl-decrypt']['forward-trust-certificate'].rsa;

    if (debug_sharedConfig)  console.log(`forwardTrustName = ${forwardTrustName}`);

    // only 1 forward UnTrust cert can be configured    
    if (typeof(sharedConfig['ssl-decrypt']) !== 'undefined')
        if (typeof(sharedConfig['ssl-decrypt']['forward-untrust-certificate']) !== 'undefined' && Object.prototype.toString.call(sharedConfig['ssl-decrypt']['forward-untrust-certificate']) == '[object Object]')
            if (typeof(sharedConfig['ssl-decrypt']['forward-untrust-certificate'].rsa) !== 'undefined' && Object.prototype.toString.call(sharedConfig['ssl-decrypt']['forward-untrust-certificate'].rsa ) == '[object String]')
                forwardUntrustName = sharedConfig['ssl-decrypt']['forward-untrust-certificate'].rsa;

    if (debug_sharedConfig && typeof(forwardUntrustName) !== 'undefined')  console.log(`forwardUntrustName = ${forwardUntrustName}`);

    if (typeof(sharedConfig.certificate) !== 'undefined') 
        if (typeof(sharedConfig.certificate.entry) !== 'undefined') {
            if (Object.prototype.toString.call(sharedConfig.certificate.entry) === '[object Object]') {
                certName.push(sharedConfig.certificate.entry['$'].name);
                ca.push(sharedConfig.certificate.entry.ca);
                if (typeof(sharedConfig.certificate.entry['private-key']) !== undefined)
                    privateKey = true;
                if (debug_sharedConfig) console.log(`certName = ${certName}, ca = ${ca}, privateKey = ${privateKey}`);
                scenario.scoringCerts(certName, ca, privateKey, forwardTrustName, forwardUntrustName, fwName);
            }

            if (Object.prototype.toString.call(sharedConfig.certificate.entry) === '[object Array]') {
                for (arr in sharedConfig.certificate.entry) {
                    certName.push(sharedConfig.certificate.entry[arr]['$'].name);
                    ca.push(sharedConfig.certificate.entry[arr].ca);
                    if (typeof(sharedConfig.certificate.entry[arr]['private-key']) !== 'undefined')
                        privateKey.push(true);
                    else
                        privateKey.push(false);

                }
                if (debug_sharedConfig) console.log(`certName = ${certName}, ca = ${ca}, privateKey = ${privateKey}`);
                scenario.scoringCerts(certName, ca, privateKey, forwardTrustName, forwardUntrustName, fwName);
            }
        }

    certName, ca, privateKey = [[], [], []];

    if (debug_sharedConfig) {
        console.log(`\n**************************************`);
        console.log(`END Function: processSharedConfig`);
        console.log(`**************************************\n`);
    }

}

//***********************************************************************************************
//Zone Config Parsing
//***********************************************************************************************
function processZoneConfig(zones, fwName) {

    let [name, type, zonePP, packetBuff] = ['', '', '', ''];
    let interface = [];
    let userIDEnable = false;


    if (debug_zones) {
        console.log(`\n**************************************`);
        console.log(`Function: processZoneConfig`);
        console.log(`**************************************\n`);
        console.log(`zones = `);
        console.log(zones);
        console.log(zones)
        console.log(``);
    }

    if (Object.prototype.toString.call(zones) === '[object Object]') {
        if (typeof(zones['$'].name) !== 'undefined') {
            name = zones['$'].name;
            if (debug_zones) console.log(`Zone name = ${name}`);
        }
        if (typeof(zones.network) !== 'undefined') {
            if (Object.keys(zones.network).includes(`layer3`)) {
                if (debug_zones) console.log(`Zone ${name} is configured as an Layer-3 zone.`);
                type = 'layer3';
                if (typeof(zones.network.layer3.member) !== 'undefined') {
                    if (Object.prototype.toString.call(zones.network.layer3.member) === '[object String]') {
                        interface.push(zones[arr].network.layer3.member);
                        if (debug_zones) console.log(`${name} obj interface obj = ${interface}`);
                    }
                    if (Object.prototype.toString.call(zones.network.layer3.member) === '[object Array]')
                        for (let int in interface.push(zones.network.layer3.member)) {
                            interface.push(zones.network.layer3.member[int]);
                        }   
                }
            }
            if (typeof(zones.network['zone-protection-profile']) !== 'undefined') 
                zonePP = zones.network['zone-protection-profile'];
            if (typeof(zones.network['enable-packet-buffer-protection']) !== 'undefined') 
                packetBuff = zones.network['enable-packet-buffer-protection'];
        }
        if (typeof(zones['enable-user-identification']) !== 'undefined')
            if (zones['enable-user-identification'] === 'yes') {
                if (debug_zones) console.log(`user-id is enabled on the zone`);
                userIDEnable = true;
            }
        scenario.scoringZones(name, type, interface, userIDEnable, zonePP, packetBuff, fwName);
    }

    if (Object.prototype.toString.call(zones) === '[object Array]') {
        for (let arr in zones) {
            if (typeof(zones[arr]['$'].name) !== 'undefined') {
                name = zones[arr]['$'].name;
                if (debug_zones) console.log(`zone name = ${name}`);
            }
            if (typeof(zones[arr].network) !== 'undefined') {
                if (Object.keys(zones[arr].network).includes(`layer3`)) {
                    if (debug_zones) console.log(`Zone is a Layer-3 zone.`);
                    type = 'layer3';
                    if (typeof(zones[arr].network.layer3.member) !== 'undefined') {
                        if (Object.prototype.toString.call(zones[arr].network.layer3.member) === '[object String]') {
                            interface.push(zones[arr].network.layer3.member);
                            if (debug_zones) console.log(`${interface} is a member of this zone.`);
                        }
                        if (Object.prototype.toString.call(zones[arr].network.layer3.member) === '[object Array]') {
                            for (let int in interface.push(zones[arr].network.layer3.member)) {
                                interface.push(zones[arr].network.layer3.member[int]);
                                if (debug_zones) console.log(`${name} arr interface arr = ${interface}`);
                            }
                        }
                    }
                }
                if (typeof(zones[arr].network['zone-protection-profile']) !== 'undefined') 
                    zonePP = zones[arr].network['zone-protection-profile'];
                if (typeof(zones[arr].network['enable-packet-buffer-protection']) !== 'undefined') 
                    packetBuff = zones[arr].network['enable-packet-buffer-protection'];
            }
            if (typeof(zones[arr]['enable-user-identification']) !== 'undefined')
                if (zones[arr]['enable-user-identification'] === 'yes') {
                    if (debug_zones) console.log(`user-id is enabled on the zone`);
                    userIDEnable = true;
                }
            scenario.scoringZones(name, type, interface, userIDEnable, zonePP, packetBuff, fwName);
            // reset variables for next proesssing
            [name, type, userIDEnable] = ['', '', false];
            interface = [];
        }
    }

    if (debug_zones) {
        console.log(`\n**************************************`);
        console.log(`END Function: processZoneConfig`);
        console.log(`**************************************\n`);
    }
}

//***********************************************************************************************
//Network Profile Config Parsing
//***********************************************************************************************
function processNetworkProfileConfig(networkProfileConfig, fwName) {

    let name;
    let flood;
    let scan;
    let [looseSourceRouting, unknownOption, malformedOption, tcpSplitHandshake, overlappingTCPSegment, tcpTimestamp] = ['', '', '', '', '', ''];

    if (debug_networkProfile) {
        console.log(`\n**************************************`);
        console.log(`Function: processNetworkProfileConfig`);
        console.log(`**************************************\n`);
        console.log(`networkProfileConfig = `);
        console.log(networkProfileConfig);
        console.log(``);
    }

    if (Object.prototype.toString.call(networkProfileConfig) === '[object Object]') {
        name = networkProfileConfig['$'].name;
        if (typeof(networkProfileConfig.flood) !== 'undefined')
            flood = networkProfileConfig.flood;
        if (typeof(networkProfileConfig.scan) !== 'undefined')
            scan = networkProfileConfig.scan.entry;
        if (typeof(networkProfileConfig['discard-strict-source-routing']) !== 'undefined')
            looseSourceRouting = networkProfileConfig['discard-strict-source-routing'];
        if (typeof(networkProfileConfig['discard-loose-source-routing']) !== 'undefined')
            looseSourceRouting = networkProfileConfig['discard-loose-source-routing'];
        if (typeof(networkProfileConfig['discard-unknown-option']) !== 'undefined')
            unknownOption = networkProfileConfig['discard-unknown-option'];
        if (typeof(networkProfileConfig['discard-malformed-option']) !== 'undefined')
            malformedOption = networkProfileConfig['discard-malformed-option'];
        if (typeof(networkProfileConfig['discard-tcp-split-handshake']) !== 'undefined')
            tcpSplitHandshake = networkProfileConfig['discard-tcp-split-handshake'];      
        if (typeof(networkProfileConfig['discard-overlapping-tcp-segment-mismatch']) !== 'undefined')
            overlappingTCPSegment = networkProfileConfig['discard-overlapping-tcp-segment-mismatch']; 
        if (typeof(networkProfileConfig['remove-tcp-timestamp']) !== 'undefined')
            tcpTimestamp = networkProfileConfig['remove-tcp-timestamp']; 

        scenario.processZoneProfiles([name, flood, scan, looseSourceRouting, unknownOption, malformedOption, tcpSplitHandshake, overlappingTCPSegment, tcpTimestamp], fwName);

    }

    if (Object.prototype.toString.call(networkProfileConfig) === '[object Array]') {
        for (let arr in networkProfileConfig) {
            name = networkProfileConfig[arr]['$'].name;
            if (typeof(networkProfileConfig[arr].flood) !== 'undefined')
                flood = networkProfileConfig[arr].flood;
            if (typeof(networkProfileConfig[arr].scan) !== 'undefined')
                scan = networkProfileConfig[arr].scan;
            if (typeof(networkProfileConfig[arr]['discard-strict-source-routing']) !== 'undefined')
                looseSourceRouting = networkProfileConfig[arr]['discard-strict-source-routing'];
            if (typeof(networkProfileConfig[arr]['discard-loose-source-routing']) !== 'undefined')
                looseSourceRouting = networkProfileConfig[arr]['discard-loose-source-routing'];
            if (typeof(networkProfileConfig[arr]['discard-unknown-option']) !== 'undefined')
                unknownOption = networkProfileConfig[arr]['discard-unknown-option'];
            if (typeof(networkProfileConfig[arr]['discard-malformed-option']) !== 'undefined')
                malformedOption = networkProfileConfig[arr]['discard-malformed-option'];
            if (typeof(networkProfileConfig[arr]['discard-tcp-split-handshake']) !== 'undefined')
                tcpSplitHandshake = networkProfileConfig[arr]['discard-tcp-split-handshake'];      
            if (typeof(networkProfileConfig[arr]['discard-overlapping-tcp-segment-mismatch']) !== 'undefined')
                overlappingTCPSegment = networkProfileConfig[arr]['discard-overlapping-tcp-segment-mismatch']; 
            if (typeof(networkProfileConfig[arr]['remove-tcp-timestamp']) !== 'undefined')
                tcpTimestamp = networkProfileConfig[arr]['remove-tcp-timestamp']; 

            scenario.processZoneProfiles([name, flood, scan, looseSourceRouting, unknownOption, malformedOption, tcpSplitHandshake, overlappingTCPSegment, tcpTimestamp], fwName);
        }
    }
}

//***********************************************************************************************
//Qos Profile Config Parsing
//***********************************************************************************************
function processQoSProfileConfig(qosProfileConfig, fwName) {

    let [name, className] = ['', ''];
    let [class1, class2, class3, class4, class5, class6, class7, class8] = [[], [], [], [], [], [], [], []];
    let classification = [];
    let profileType;
    let shrtnr;

    if (debug_qosProfile) {
        console.log(`\n**************************************`);
        console.log(`Function: processQoSProfileConfig`);
        console.log(`**************************************\n`);
        console.log(`qosProfileConfig = `);
        console.log(qosProfileConfig);
        console.log(``);
    }    

    if (Object.prototype.toString.call(qosProfileConfig) === '[object Object]') {
        name = qosProfileConfig['$'].name;
        if (debug_qosProfile) console.log(`\narr name = ${name}`);
        if (typeof(qosProfileConfig['class']) !== 'undefined') 
            if (typeof(qosProfileConfig['class'].entry) !== 'undefined') {
                if (Object.prototype.toString.call(qosProfileConfig['class'].entry) === '[object Object]') {
                    shrtnr = qosProfileConfig['class'].entry['class-bandwidth'];
                    className = qosProfileConfig['class'].entry['$'].name;
                    switch(className) {
                        case 'class1': 
                            break;
                        case 'class8':
                            console.log('class8');
                            class8 = [shrtnr['egress-max'], shrtnr['egress-guaranteed'], qosProfileConfig['class'].entry.priority];
                            break;
                        default:
                            break; // no class found
                    }
                    if (debug_qosProfile) console.log('obj class8 = ');
                    if (debug_qosProfile) console.log(class8);
                } else if (Object.prototype.toString.call(qosProfileConfig['class'].entry) === '[object Array]') {
                    for (let classEntry in qosProfileConfig['class'].entry) {
                        shrtnr = qosProfileConfig['class'].entry[classEntry]['class-bandwidth'];
                        className = qosProfileConfig['class'].entry[classEntry]['$'].name;
                        switch(className) {
                            case 'class1': 
                                break;
                            case 'class8':
                                if (typeof(shrtnr) !== 'undefined') {
                                    class8 = [shrtnr['egress-max'], shrtnr['egress-guaranteed'], dosProfileConfig['class'].entry.priority];
                                    if (debug_qosProfile) console.log('arr class8 = ');
                                    if (debug_qosProfile) console.log(class8);
                                } else {
                                    class8 = ['', '', qosProfileConfig['class'].entry[classEntry].priority];
                                    if (debug_qosProfile) console.log('arr class8 = ');
                                    if (debug_qosProfile) console.log(class8);
                                }
                                break;
                            default:
                                break; // no class found
                        }

                    }
                }
            }
        scenario.scoringQoSProfiles(class1, class2, class3, class4, class5, class6, class7, class8, name, fwName);

        // clear array
        [class1, class2, class3, class4, class5, class6, class7, class8] = [[], [], [], [], [], [], [], []];
    }

    if (Object.prototype.toString.call(qosProfileConfig) === '[object Array]') {
        for (let arr in qosProfileConfig) {
            name = qosProfileConfig[arr]['$'].name;
            if (debug_qosProfile) console.log(`\narr name = ${name}`);
            if (typeof(qosProfileConfig[arr]['class']) !== 'undefined') 
                if (typeof(qosProfileConfig[arr]['class'].entry) !== 'undefined') {
                    if (debug_qosProfile) console.log(qosProfileConfig[arr]['class'].entry);
                    if (Object.prototype.toString.call(qosProfileConfig[arr]['class'].entry) === '[object Object]') {
                        shrtnr = qosProfileConfig[arr]['class'].entry['class-bandwidth'];
                        className = qosProfileConfig[arr]['class'].entry['$'].name;
                        switch(className) {
                            case 'class1': 
                                break;
                            case 'class8':
                                if (debug_qosProfile) console.log('class8');
                                class8 = [shrtnr['egress-max'], shrtnr['egress-guaranteed'], qosProfileConfig[arr]['class'].entry.priority];
                                break;
                            default:
                                console.log('no class found');
                                break;
                        }
                        if (debug_qosProfile) console.log('obj class8 = ');
                        if (debug_qosProfile) console.log(class8);
                    } else if (Object.prototype.toString.call(qosProfileConfig[arr]['class'].entry) === '[object Array]') {
                        for (let classEntry in qosProfileConfig[arr]['class'].entry) {
                            shrtnr = qosProfileConfig[arr]['class'].entry[classEntry]['class-bandwidth'];
                            className = qosProfileConfig[arr]['class'].entry[classEntry]['$'].name;
                            switch(className) {
                                case 'class1': 
                                    break;
                                case 'class8':
                                    if (typeof(shrtnr) !== 'undefined') {
                                        class8 = [shrtnr['egress-max'], shrtnr['egress-guaranteed'], qosProfileConfig[arr]['class'].entry.priority];
                                        if (debug_qosProfile) console.log('arr class8 = ');
                                        if (debug_qosProfile) console.log(class8);
                                    } else {
                                        class8 = ['', '', qosProfileConfig[arr]['class'].entry[classEntry].priority];
                                        if (debug_qosProfile) console.log('arr class8 = ');
                                        if (debug_qosProfile) console.log(class8);
                                    }
                                    break;
                                default:
                                    if (debug_qosProfile) console.log('no class found');
                                    break;
                            }

                        }
                    }
                }
            scenario.scoringQoSProfiles(class1, class2, class3, class4, class5, class6, class7, class8, name, fwName);

            // clear array
            [class1, class2, class3, class4, class5, class6, class7, class8] = [[], [], [], [], [], [], [], []];

        }
    }

    if (debug_qosProfile) {
        console.log(`\n**************************************`);
        console.log(`END Function: processQoSProfileConfig`);
        console.log(`**************************************\n`);
    }  

}

//***********************************************************************************************
//QoS Interface Config Parsing
//***********************************************************************************************
function processQoSInterfaceConfig(qosInterfaceConfig, fwName) {

    let intName;
    let defaultClearTextProfile;

    if (debug_qosInterface) {
        console.log(`\n**************************************`);
        console.log(`Function: processQoSInterfaceConfig`);
        console.log(`**************************************\n`);
        console.log(`qosInterfaceConfig = `);
        console.log(qosInterfaceConfig);
        console.log(``);
    }    

    if (Object.prototype.toString.call(qosInterfaceConfig) === '[object Object]') {
        intName = qosInterfaceConfig['$'].name; 
        if (typeof(qosInterfaceConfig['regular-traffic']) !== 'undefined')
            if (typeof(qosInterfaceConfig['regular-traffic']['default-group']) !== 'undefined')
                if (typeof(qosInterfaceConfig['regular-traffic']['default-group']['qos-profile']) !== 'undefined')
                    defaultClearTextProfile = qosInterfaceConfig['regular-traffic']['default-group']['qos-profile'];
    }

    if (debug_qosInterface) console.log(`\ndefaultClearTextProfile = ${defaultClearTextProfile}`);

    scenario.scoringQoSInterface(intName, defaultClearTextProfile, fwName);

    if (debug_qosInterface) {
        console.log(`\n**************************************`);
        console.log(`END Function: processQoSInterfaceConfig`);
        console.log(`**************************************\n`);
    }    

}

//***********************************************************************************************
//Management Config Parsing
//***********************************************************************************************
function processManagementConfig(managementConfig, fwName) {
    // let points = 0;
    let passComplexityConfig; // XML path shortener variable
    let [enabled, blockUserInclusion, minLength, minUpper, minLower] = ['', '', '', '', ''];

    if(typeof(managementConfig['password-complexity']) !== 'undefined') {
        passComplexityConfig = managementConfig['password-complexity'];
        if (typeof(passComplexityConfig.enabled) !== 'undefined')
            enabled = passComplexityConfig.enabled;
        if (typeof(passComplexityConfig['block-username-inclusion']) !== 'undefined')
            blockUserInclusion = passComplexityConfig['block-username-inclusion'];
        if (typeof(passComplexityConfig['minimum-length']) !== 'undefined')
            minLength = passComplexityConfig['minimum-length'];
        if (typeof(passComplexityConfig['minimum-uppercase-letters']) !== 'undefined')
            minUpper = passComplexityConfig['minimum-uppercase-letters'];
        if (typeof(passComplexityConfig['minimum-lowercase-letters']) !== 'undefined')
            minLower = passComplexityConfig['minimum-lowercase-letters'];

        scenario.scoringMgmtPassComplexityConfig([enabled, blockUserInclusion, minLength, minUpper, minLower], fwName);
    }

    if (typeof(managementConfig.users) !== 'undefined')
        if (typeof(managementConfig.users.entry) !== 'undefined')
            scenario.scoringMgmtUsers(managementConfig.users.entry, fwName); // pass over all users.

}

//***********************************************************************************************
//Security Profile Groups Parsing
//***********************************************************************************************
function processSecurityProfileGroupConfig(spgconfig, fwName) {
    //let points = 0;
    let profileName;
    let [virus, spyware, vulnerability, fileBlocking, url, dataFiltering, wildfire] = ['', '', '', '', '', '', ''];

    if (debug_securityProfileGroup) {
        console.log(`\n**************************************`);
        console.log(`Function: processSecurityProfileGroupConfig`);
        console.log(`**************************************\n`);
        console.log(`spgconfig = `);
        console.log(spgconfig);
        console.log(``);
    }    

    if (typeof(spgconfig.entry) !== 'undefined') {
        let securityProfileGroupEntry = spgconfig.entry;
        if (debug_securityProfileGroup) {
            console.log(`securityProfileGroupEntry = `);
            console.log(securityProfileGroupEntry);
            console.log(``);
        }
        scenario.scoringSecurityProfiles(fwName); // this scores the security profiles themselves

        if (Object.prototype.toString.call(securityProfileGroupEntry) === '[object Array]') { // multiple profiles
            if (debug_securityProfileGroup) console.log(`SPG profiles is an array`);
            for (var obj in securityProfileGroupEntry) { // if > 1 FB profile, loop through array
                profileName = securityProfileGroupEntry[obj]['$'].name;
                [virus, spyware, fileBlocking] = ['', '', '']; // clear previous values
                if (typeof(securityProfileGroupEntry[obj]['virus']) !== 'undefined') 
                    virus = securityProfileGroupEntry[obj].virus.member;
                if (typeof(securityProfileGroupEntry[obj]['file-blocking']) !== 'undefined') 
                    fileBlocking = securityProfileGroupEntry[obj]['file-blocking'].member;
                if (typeof(securityProfileGroupEntry[obj]['spyware']) !== 'undefined') 
                    spyware = securityProfileGroupEntry[obj]['spyware'].member;
                if (typeof(securityProfileGroupEntry[obj]['vulnerability']) !== 'undefined') 
                    vulnerability = securityProfileGroupEntry[obj]['vulnerability'].member;
                if (typeof(securityProfileGroupEntry[obj]['url-filtering']) !== 'undefined') 
                    url = securityProfileGroupEntry[obj]['url-filtering'].member;
                if (typeof(securityProfileGroupEntry[obj]['wildfire-analysis']) !== 'undefined')
                    wildfire = securityProfileGroupEntry[obj]['wildfire-analysis'].member;
                if (typeof(securityProfileGroupEntry[obj]['data-filtering']) !== 'undefined')
                    dataFiltering = securityProfileGroupEntry[obj]['data-filtering'].member;
                //if (typeof(securityProfileGroupEntry[obj]['data-filtering']) !== 'undefined')
                scenario.scoringSecurityProfileGroups ([virus, spyware, vulnerability, url, fileBlocking, dataFiltering, wildfire], profileName, fwName); // send for scoring
            } 
        } else if (Object.prototype.toString.call(securityProfileGroupEntry) === '[object Object]') { // single object
            if (debug_securityProfileGroup) console.log(`SPG profiles is an object`)
            profileName = securityProfileGroupEntry['$'].name;
            //console.log(securityProfileGroupEntry); 
            if (typeof(securityProfileGroupEntry.virus) !== 'undefined') 
                virus = securityProfileGroupEntry.virus.member;
            if (typeof(securityProfileGroupEntry['file-blocking']) !== 'undefined') 
                fileBlocking = securityProfileGroupEntry['file-blocking'].member;
            if (typeof(securityProfileGroupEntry['spyware']) !== 'undefined')
                spyware = securityProfileGroupEntry['spyware'].member;
            if (typeof(securityProfileGroupEntry['vulnerability']) !== 'undefined') 
                vulnerability = securityProfileGroupEntry['vulnerability'].member;
            if (typeof(securityProfileGroupEntry['url-filtering']) !== 'undefined') 
                url = securityProfileGroupEntry['url-filtering'].member;
            if (typeof(securityProfileGroupEntry['wildfire-analysis']) !== 'undefined') {
                wildfire = securityProfileGroupEntry['wildfire-analysis'].member;
            }
            if (typeof(securityProfileGroupEntry['data-filtering']) !== 'undefined')
                dataFiltering = securityProfileGroupEntry['data-filtering'].member;
            scenario.scoringSecurityProfileGroups ([virus, spyware, vulnerability, url, fileBlocking, dataFiltering, wildfire], profileName, fwName); // send for scoring
        } else {
            if (debug_securityProfileGroup) console.log(`Likely no SPG profiles configured`);
        }
    }

    if (debug_securityProfileGroup) {
        console.log(`\n**************************************`);
        console.log(`END Function: processSecurityProfileGroupConfig`);
        console.log(`**************************************\n`);
    }    
}

//***********************************************************************************************
//File blocking processing Rules 
//***********************************************************************************************
function processFileBlockingRules(obj, fbProfileName, fwName) { 

    if (debug_fileBlocking) {
        console.log(`\n**************************************`);
        console.log(`Function: processFileBlockingRules`);
        console.log(`**************************************\n`);
        console.log(`obj = `);
        console.log(obj);
        console.log(``);
    }

    let [name, direction, action] = [obj['$'].name, obj.direction, obj.action];
    let [apps, fileTypes] = [[], []]; // these are arrays.

    // multiple applications listed in the FB rule
    if (Object.prototype.toString.call(obj.application.member) === '[object Array]') {
        for (var arr in obj.application.member)
            apps[arr] = obj.application.member[arr];
    } else if (Object.prototype.toString.call(obj.application.member) === '[object String]') // only 1 app listed in the rule
        apps[0] = obj.application.member;

    // multiple file types listed in the FB rule
    if (Object.prototype.toString.call(obj['file-type'].member) === '[object Array]') {
        for (var arr in obj['file-type'].member)
            fileTypes[arr] = obj['file-type'].member[arr];
    } else if (Object.prototype.toString.call(obj['file-type'].member) === '[object String]') // only 1 listed in the FB rule
        fileTypes[0] = obj['file-type'].member;

    scenario.scoringFileBlocking([name, apps, fileTypes, direction, action], fbProfileName, fwName); // send for scoring

    if (debug_fileBlocking) {
        console.log(`\n**************************************`);
        console.log(`END Function: processFileBlockingRules`);
        console.log(`**************************************\n`);
    }

}

//***********************************************************************************************
//URL blocking processing Rules 
//***********************************************************************************************
function processURLRules(obj, urlProfileName, fwName) { 
    let urlProfile = obj;
    if (urlDebug) {
        console.log(`Function processURLRules called\n`);
        console.log(`urlProfileName = ${urlProfileName}`);
        console.log(`urlProfile = `);
        console.log(urlProfile);
    }

    scenario.scoringURLFiltering(urlProfile, urlProfileName, fwName); // send for scoring
}
function processCustomURLRules(obj, customUrlProfileName, fwName) { 
    let customUrlProfile = obj;
    if (customUrlDebug) {
        console.log(`Function processCustomURLRules called\n`);
        console.log(`customUrlProfileName = ${customUrlProfileName}`);
        console.log(`customUrlProfile = `);
        console.log(customUrlProfile);
    }

    scenario.scoringCustomURLFiltering(customUrlProfile, customUrlProfileName, fwName); // send for scoring
}

//***********************************************************************************************
//Data-Filtering processing (Rules within)
//***********************************************************************************************
function processDataFilteringRules(dataProfile, datafilteringProfileName, fwName) { 
    let appMemberArr = [];
    let fileTypeMember = [];
    let [direction, alertThresh, blockThresh, dataObj, logSev] = ['', '', '', '', ''];

    if (debug_dataFilteringRules) {
        console.log(`\n**************************************`);
        console.log(`Function: processDataFilteringRules`);
        console.log(`**************************************\n`);
        console.log(`dataProfile = `);
        console.log(dataProfile);
        console.log(``);
    }

    if(typeof(dataProfile['log-severity']) !== 'undefined')
        logSev = dataProfile['log-severity'];
    if(typeof(dataProfile['data-object']) !== 'undefined')
        dataObj = dataProfile['data-object'];
    if(typeof(dataProfile['alert-threshold']) !== 'undefined')
        alertThresh = dataProfile['alert-threshold'];
    if(typeof(dataProfile['block-threshold']) !== 'undefined')
        blockThresh = dataProfile['block-threshold'];
    if(typeof(dataProfile.direction) !== 'undefined')
        direction = dataProfile.direction;
    if (typeof(dataProfile.application) !== 'undefined')
        if (typeof(dataProfile.application.member !== 'undefined'))
            if (Object.prototype.toString.call(dataProfile.application.member) === '[object String]')
                appMemberArr[0] = dataProfile.application.member;
            else
                appMemberArr = dataProfile.application.member;
    if (typeof(dataProfile['file-type']) !== 'undefined') 
        if (typeof(dataProfile['file-type'].member !== 'undefined')) 
            if (Object.prototype.toString.call(dataProfile['file-type'].member) === '[object String]')
                fileTypeMember[0] = dataProfile['file-type'].member;
            else
                fileTypeMember = dataProfile['file-type'].member; 

    if (debug_dataFilteringRules) {
        console.log(`appMemberArr = ${appMemberArr}, fileTypeMember = ${fileTypeMember}, direction = ${direction}`);
        console.log(`alertThresh = ${alertThresh}, blockThresh = ${blockThresh}, logSev = ${logSev}`);
        console.log(`dataObj = ${dataObj}`);
        console.log(`\n**************************************`);
        console.log(`END Function: processDataFilteringRules`);
        console.log(`**************************************\n`);
    }

    scenario.scoringDataFiltering([appMemberArr, direction, fileTypeMember, alertThresh, blockThresh, dataObj, logSev], datafilteringProfileName, fwName); // send for scoring 
}
function processCustomDataRules(customDataProfile, customDataProfileName, fwName) { 

    if (debug_customDataObject) {
        console.log(`\n**************************************`);
        console.log(`Function: processCustomDataRules`);
        console.log(`**************************************\n`);
        console.log(`customDataProfile = `);
        console.log(customDataProfile);
        console.log(``);
    }

    scenario.scoringCustomDataFiltering(customDataProfile, customDataProfileName, fwName); // send for scoring

    if (debug_customDataObject) {
        console.log(`\n**************************************`);
        console.log(`END Function: processCustomDataRules`);
        console.log(`**************************************\n`);
    }
}

//***********************************************************************************************
//DoS Profile processing
//***********************************************************************************************
function processDoSProfile(dosProfile, profileName, fwName) {

    if (debug_dosProfile) {
        console.log(`\n**************************************`);
        console.log(`Function: processDoSProfile`);
        console.log(`**************************************\n`);
        console.log(`dosProfile = `);
        console.log(dosProfile);
        console.log(``);
    }

    let [name, type, udpFlood, icmpFlood, icmpv6Flood, otherIPFlood] = ['', '', [], [], [], []];
    let tcpEnable = false;
    let tcpSynCookieEnable = false;
    let synCookieDuration = '';
    let synCookieAlarm = '';
    let synCookieActivate = '';
    let synCookieMax = '';

    name = dosProfile['$'].name;
    type = dosProfile.type;

    if (typeof(dosProfile.flood) !== 'undefined')
        if (typeof(dosProfile.flood['tcp-syn']) !== 'undefined') {
            tcpEnable = true;
            if (typeof(dosProfile.flood['tcp-syn']['syn-cookies']) !== 'undefined') {
                if (dosProfile.flood['tcp-syn'].enable === 'yes')
                    tcpSynCookieEnable = true;
                if (typeof(dosProfile.flood['tcp-syn']['syn-cookies'].block) !== 'undefined') {
                    synCookieDuration = dosProfile.flood['tcp-syn']['syn-cookies'].block.duration;
                }
                synCookieAlarm = dosProfile.flood['tcp-syn']['syn-cookies']['alarm-rate'];
                synCookieActivate = dosProfile.flood['tcp-syn']['syn-cookies']['activate-rate'];
                synCookieMax = dosProfile.flood['tcp-syn']['syn-cookies']['maximal-rate'];
            }
        }

    if (debug_dosProfile) {
        console.log(`\n**************************************`);
        console.log(`END Function: processDoSProfile`);
        console.log(`**************************************\n`);
    }

    scenario.processDosProfiles(name, type, tcpEnable, tcpSynCookieEnable, synCookieDuration, synCookieAlarm, synCookieActivate, synCookieMax);
}

//***********************************************************************************************
//Spyware processing (Rules within)
//***********************************************************************************************
function processSpywareRules(obj, spywareProfileName, fwName) { 
    if (spyDebug) console.log(obj);
    if (spyDebug) console.log(`processSpywareRules obj, ${fwName}`);

    let [name, actionobj, category, severity] = [obj['$'].name, obj.action, obj.category, []];
    let action;

    // obj.action is an Object for some reason (from the FW) and property needs to be taken out
    for (var prop in actionobj) // should only ever be 1 property in this object
        action = prop;

    // multiple severities listed in the spyware rule
    if (Object.prototype.toString.call(obj.severity.member) === '[object Array]') {
        for (var arr in obj.severity.member)
            severity[arr] = obj.severity.member[arr];
    } else if (Object.prototype.toString.call(obj.severity.member) === '[object String]') // only 1 app listed in the rule
        severity[0] = obj.severity.member;

    scenario.scoringSpyware([name, action, category, severity], spywareProfileName, fwName); // send for scoring
}
function processSinkHoleRules(obj, spywareProfileName, fwName) {
    if (spySinkDebug) console.log(`processSinkHoleRules obj, ${fwName}`);
    if (spySinkDebug) console.log(obj);
    scenario.scoringSpywareSinkhole(obj, spywareProfileName, fwName); // send for scoring

}

//***********************************************************************************************
//Vulnerability processing (Rules within)
//***********************************************************************************************
function processVulnerabilityRules(obj, vulnerabilityProfileName, fwName) { 

    if (debug_vulnRules) {
        console.log(`\n**************************************`);
        console.log(`Function: processVulnerabilityRules`);
        console.log(`**************************************\n`);
        console.log(`vulnObj = `);
        console.log(obj);
        console.log(``);
    }

    if (debug_vulnRules) console.log(obj);
    if (debug_vulnRules) console.log(`processVulnerabilityRules obj, ${fwName}`);

    let [name, actionobj, category, severity, pcap] = [obj['$'].name, obj.action, obj.category, [], obj['packet-capture']];
    let action;

    // obj.action is an Object for some reason (from the FW) and property needs to be taken out
    for (var prop in actionobj) // should only ever be 1 property in this object
        action = prop;

    if (debug_vulnRules) console.log(name, action, category, pcap);

    // multiple severities listed in the vulnerability rule
    if (Object.prototype.toString.call(obj.severity.member) === '[object Array]') {
        for (var arr in obj.severity.member)
            severity[arr] = obj.severity.member[arr];
    } else if (Object.prototype.toString.call(obj.severity.member) === '[object String]') // only 1 app listed in the rule
        severity[0] = obj.severity.member;

    scenario.scoringVulnerability ([name, action, category, severity, pcap], vulnerabilityProfileName, fwName); // send for scoring

    if (debug_vulnRules) {
        console.log(`\n**************************************`);
        console.log(`END Function: processVulnerabilityRules`);
        console.log(`**************************************\n`);
    }
}
function processVulnThreatExceptionRules(obj, vulnerabilityProfileName, fwName) { 
    if (vulnExceptionDebug) console.log(`processVulnThreatExceptionRules obj, ${fwName}`);
    let name = obj.name;
    if (vulnExceptionDebug) console.log(name);
    scenario.scoringVulnerabilityException(name, vulnerabilityProfileName, fwName); // send for scoring

}
function processVulnCustomProfile(customVulnConfig, fwName) {
    if (debug_vulnRules) {
        console.log(`\n**************************************`);
        console.log(`Function: processVulnCustomProfile`);
        console.log(`**************************************\n`);
        console.log(`customVulnConfig = `);
        console.log(customVulnConfig);
        console.log(``);
    }

    let threatAction;
    let threatSigType;
    let threatOrderFree;
    let threatValue;
    let threatContext;
    if (typeof(customVulnConfig) !== 'undefined') {
        if (typeof(customVulnConfig.vulnerability) !== 'undefined') {
            if (Object.prototype.toString.call(customVulnConfig.vulnerability.entry) === '[object Object]') {
                threatAction = Object.keys(customVulnConfig.vulnerability.entry['default-action']).toString();;
                if (typeof(customVulnConfig.vulnerability.entry.signature) !== 'undefined') {
                    threatSigType = Object.keys(customVulnConfig.vulnerability.entry.signature).toString();
                    if (threatSigType === 'standard') {
                        let standard = customVulnConfig.vulnerability.entry.signature.standard; // shortener
                        if (typeof(standard.entry) !== 'undefined') {
                            if (typeof(standard.entry['order-free']) !== 'undefined')
                                threatOrderFree = standard.entry['order-free'];
                            if (typeof(standard.entry['and-condition']) !== 'undefined')
                                if (typeof(standard.entry['and-condition'].entry) !== 'undefined')
                                    if (typeof(standard.entry['and-condition'].entry['or-condition']) !== 'undefined') {
                                        let condition = standard.entry['and-condition'].entry['or-condition'];
                                        if (typeof(condition.entry) !== 'undefined')
                                            if (typeof(condition.entry.operator) !== 'undefined')
                                                if (typeof(condition.entry.operator['equal-to']) !== 'undefined') {
                                                    if (typeof(condition.entry.operator['equal-to'].value) !== 'undefined')
                                                        threatValue = condition.entry.operator['equal-to'].value;
                                                    if (typeof(condition.entry.operator['equal-to'].context) !== 'undefined')
                                                        threatContext = condition.entry.operator['equal-to'].context;
                                                }
                                    }
                        }
                    }

                }

                scenario.processCustomThreatSig(threatAction, threatSigType, threatOrderFree, threatValue, threatContext, fwName);
            }

            if (Object.prototype.toString.call(customVulnConfig.vulnerability.entry) === '[object Array]') {
                for (let arr in customVulnConfig.vulnerability.entry) {
                    threatAction = Object.keys(customVulnConfig.vulnerability.entry[arr]['default-action']).toString();;
                    if (typeof(customVulnConfig.vulnerability.entry[arr].signature) !== 'undefined') {
                        threatSigType = Object.keys(customVulnConfig.vulnerability.entry[arr].signature).toString();
                        if (threatSigType === 'standard') {
                            let standard = customVulnConfig.vulnerability.entry[arr].signature.standard; // shortener
                            if (typeof(standard.entry) !== 'undefined') {
                                if (typeof(standard.entry['order-free']) !== 'undefined')
                                    threatOrderFree = standard.entry['order-free'];
                                if (typeof(standard.entry['and-condition']) !== 'undefined')
                                    if (typeof(standard.entry['and-condition'].entry) !== 'undefined')
                                        if (typeof(standard.entry['and-condition'].entry['or-condition']) !== 'undefined') {
                                            let condition = standard.entry['and-condition'].entry['or-condition'];
                                            if (typeof(condition.entry) !== 'undefined')
                                                if (typeof(condition.entry.operator) !== 'undefined')
                                                    if (typeof(condition.entry.operator['equal-to']) !== 'undefined') {
                                                        if (typeof(condition.entry.operator['equal-to'].value) !== 'undefined')
                                                            threatValue = condition.entry.operator['equal-to'].value;
                                                        if (typeof(condition.entry.operator['equal-to'].context) !== 'undefined')
                                                            threatContext = condition.entry.operator['equal-to'].context;
                                                    }
                                        }
                            }
                        }

                    }
                    scenario.processCustomThreatSig(threatAction, threatSigType, threatOrderFree, threatValue, threatContext, fwName);
                }
            }
        }
    }

    if (debug_vulnRules) {
        console.log(`\n**************************************`);
        console.log(`Function: processVulnCustomProfile`);
        console.log(`**************************************\n`);
    }

}

//***********************************************************************************************
//Antivirus processing (Rules within)
//***********************************************************************************************
function processAntivirusRules(obj, antivirusProfileName, fwName) { 
    // AV / wildfire - reset-both = http, http2, ftp and SMB    

    if (virDebug) console.log(`processAntivirusRules obj, ${fwName}`);
    if (virDebug) console.log(obj);
    //if (virDebug) console.log(`looping through obj`);
    //if (virDebug) for (var arr in obj)
    //    if (virDebug) console.log(obj[arr]);

    scenario.scoringAntivirus(obj, antivirusProfileName, fwName);
}

//***********************************************************************************************
// Generic Security Profiles Processing
//***********************************************************************************************
function processSecurityProfileConfig(securityProfileConfig, profileType, fwName) {
    if (debug_securityProfiles) {
        console.log(`\n**************************************`);
        console.log(`Function: processSecurityProfileConfig`);
        console.log(`**************************************\n`);
        console.log(`profileType = ${profileType}`);
        console.log(``);
        console.log(`securityProfileConfig = `);
        console.log(securityProfileConfig);
        console.log(``);
    }

    let profileName;
    let processRules;
    let processThreatException;
    let processSinkHole;

    switch(profileType) {
        case 'spyware':
            processRules = processSpywareRules;
            processSinkHole = processSinkHoleRules;
            break;
        case 'fileblocking':
            processRules = processFileBlockingRules;
            break;
        case 'antivirus':
            processRules = processAntivirusRules;
            break;
        case 'vulnerability':
            processRules = processVulnerabilityRules;
            processThreatException = processVulnThreatExceptionRules;
            break;
        case 'url':
            processRules = processURLRules;
            break;
        case 'customUrl':
            processRules = processCustomURLRules;
            break;
        case 'customData':
            processRules = processCustomDataRules;
            break;
        case 'datafiltering':
            processRules = processDataFilteringRules;
            break;
        case 'dosProtection':
            processRules = processDoSProfile;
            break;
        default:
            console.log(`no switch match`);
            break;
    }

    if (typeof(securityProfileConfig.entry) !== 'undefined') {
        let profileConfigEntry = securityProfileConfig.entry;
        if (debug_securityProfiles) console.log(`profileConfigEntry = `);
        if (debug_securityProfiles) console.log(profileConfigEntry);
        if (Object.prototype.toString.call(profileConfigEntry) === '[object Array]') { // multiple profiles
            if (debug_securityProfiles) console.log(`profileType is an array`);
            for (var obj in profileConfigEntry) { // if > 1 Spyware profile, loop through array
                if (debug_securityProfiles) console.log(`Processing profileConfigEntry Loop `);
                if (debug_securityProfiles) console.log(`Processing profileConfigEntry name:  ${profileConfigEntry[obj]['$'].name}`);
                if (debug_securityProfiles) console.log(`Profiletype = ${profileType}`);
                profileName = profileConfigEntry[obj]['$'].name;
                if (debug_securityProfiles) console.log(`Profile name = ${profileName}`);
                if (profileType === 'dosProtection')
                    processRules(profileConfigEntry[obj], profileName, fwName);
                if (profileType === 'fileblocking' || profileType === 'spyware' || profileType === 'vulnerability' || profileType === 'datafiltering') { // virus doesn't have "rules.entry"
                    if (typeof(profileConfigEntry[obj].rules) !== 'undefined') { // profile without any rules check
                        if (Object.prototype.toString.call(profileConfigEntry[obj].rules.entry) === '[object Array]') {  // check if there are more than 1 rule in FB profile (array)
                            if (debug_securityProfiles) console.log(profileConfigEntry[obj].rules.entry);
                            for (var arr in profileConfigEntry[obj].rules.entry)    
                                processRules(profileConfigEntry[obj].rules.entry[arr], profileName, fwName);
                        } else if (Object.prototype.toString.call(profileConfigEntry[obj].rules.entry) === '[object Object]') { // single rule in Spyware profile
                            processRules(profileConfigEntry[obj].rules.entry, profileName, fwName);
                        } else
                            console.log(`Profiles check error.`);
                    } else
                        if (debug_securityProfiles) console.log(`profile without any rules ${profileConfigEntry[obj]}`);
                    // vulnerability specific stuff
                    if (profileType === `vulnerability`)
                        if (typeof(profileConfigEntry[obj]['threat-exception']) !== 'undefined') {
                            if (debug_securityProfiles) console.log(`threat exception obj = `);
                            if (debug_securityProfiles) console.log(profileConfigEntry[obj]['threat-exception']);
                            if (typeof(profileConfigEntry[obj]['threat-exception'].entry) !== 'undefined')
                                if (typeof(profileConfigEntry[obj]['threat-exception'].entry['$']) !== 'undefined')
                                    processThreatException(profileConfigEntry[obj]['threat-exception'].entry['$'], profileName, fwName);
                        }
                    // spyware specific stuff
                    if (profileType === `spyware`)
                        // check for DNS sinkhole under spyware
                        if (typeof(profileConfigEntry[obj]['botnet-domains']) !== 'undefined') {
                            if (debug_securityProfiles) console.log(`sinkhole object = `);
                            if (debug_securityProfiles) console.log(profileConfigEntry['botnet-domains']);
                            if (typeof(profileConfigEntry[obj]['botnet-domains'].lists) !== 'undefined')
                                if (typeof(profileConfigEntry[obj]['botnet-domains'].lists.entry) !== 'undefined')
                                    processSinkHole(profileConfigEntry[obj]['botnet-domains'].lists.entry, profileName, fwName)
                        }
                } else if (profileType === 'antivirus') {
                    if (debug_securityProfiles) console.log(`antivirus debug time! Arr`);
                    //console.log(profileConfigEntry[obj]);
                    if (typeof(profileConfigEntry[obj].decoder.entry) !== 'undefined') {
                        processRules(profileConfigEntry[obj].decoder.entry, profileName, fwName);
                    } else {
                        console.log(`no decoder entries exist apparently`);
                    }
                } else if (profileType === 'url' || profileType === 'customUrl' || profileType === 'datafiltering' || profileType === 'customData') { // url only has .entry and then .entry.alert or .entry.block tree structure
                    processRules(profileConfigEntry[obj], profileName, fwName);
                } else if (profileType === 'customData')
                    processRules(profileConfigEntry[obj], profileName, fwName);
            }
        } else if (Object.prototype.toString.call(profileConfigEntry) === '[object Object]') { // single object
            if (debug_securityProfiles) console.log(`profileType is an object`);
            profileName = profileConfigEntry['$'].name
            if (debug_securityProfiles) console.log(`Profile name = ${profileName}`);
            if (profileType === 'dosProtection')
                processRules(profileConfigEntry, profileName, fwName);
            if (profileType === 'fileblocking' || profileType === 'spyware' || profileType === 'vulnerability' || profileType === 'datafiltering') { // virus doesn't have "rules.entry"
                if (typeof(profileConfigEntry.rules) !== 'undefined') { //single profile, ensure rules configured
                    if (Object.prototype.toString.call(profileConfigEntry.rules.entry) === '[object Array]') {
                        if (debug_securityProfiles) console.log(profileConfigEntry.rules.entry);
                        if (debug_securityProfiles) console.log(`Single Profile - Rules is an array`);
                        for (var arr in profileConfigEntry.rules.entry)    
                            processRules(profileConfigEntry.rules.entry[arr], profileName, fwName);    
                    } else { // single rule in FB profile
                        if (debug_securityProfiles) console.log(`Single Profile - Single rule`);
                        processRules(profileConfigEntry.rules.entry, profileName, fwName);
                    }
                }
                // Vulnerability specific stuff
                if (profileType === 'vulnerability')
                    // check for threat-exceptions under vuln profile
                    if (typeof(profileConfigEntry['threat-exception']) !== 'undefined') {
                        if (debug_securityProfiles) console.log(`threat exception obj = `);
                        if (debug_securityProfiles) console.log(profileConfigEntry['threat-exception']);
                        if (typeof(profileConfigEntry['threat-exception'].entry) !== 'undefined')
                            if (typeof(profileConfigEntry['threat-exception'].entry['$']) !== 'undefined')
                                processThreatException(profileConfigEntry['threat-exception'].entry['$'], profileName, fwName);
                    }
                // Spyware specific stuff
                if (profileType === 'spyware')
                    // check for DNS sinkhole under spyware
                    if (typeof(profileConfigEntry['botnet-domains']) !== 'undefined') {
                        if (debug_securityProfiles) console.log(`sinkhole object = `);
                        if (debug_securityProfiles) console.log(profileConfigEntry['botnet-domains']);
                        if (typeof(profileConfigEntry['botnet-domains'].lists) !== 'undefined')
                            if (typeof(profileConfigEntry['botnet-domains'].lists.entry) !== 'undefined')
                                processSinkHole(profileConfigEntry['botnet-domains'].lists.entry, profileName, fwName)
                    }
            } else if (profileType === 'antivirus') {
                if (typeof(profileConfigEntry.decoder.entry) !== 'undefined') {
                    if (debug_securityProfiles) console.log(`antivirus debug time! Obj`);
                    processRules(profileConfigEntry.decoder.entry, profileName, fwName);
                }
            } else if (profileType === 'url' || profileType === 'customUrl') { // url only has .entry and then .entry.alert or .entry.block tree structure
                processRules(profileConfigEntry, profileName, fwName);
            } else if (profileType === 'customData')
                processRules(profileConfigEntry, profileName, fwName);
        } else {
            if (debug_securityProfiles) console.log(`Likely no Profiles configured`);
        }
    } else {
        if (debug_securityProfiles) console.log('No Profile entries exist');
    }

    if (debug_securityProfiles) {
        console.log(`\n**************************************`);
        console.log(`END Function: processSecurityProfileConfig`);
        console.log(`**************************************\n`);
    }

}

//***********************************************************************************************
// Security Policies Processing
//***********************************************************************************************
function processSecurityPolicies(securityPolicies, fwName) {
    // tags, type, qos, profiles, logging do not show up in XML by default unless configured
    let [ruleName, tag, toZone, fromZone, srcIP, dstIP, user, category, app, service, hip, action, profileType, profileSettings, schedule, qos, disabled] = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

    if (debug_securityPolicies) {
        console.log(`\n**************************************`);
        console.log(`Function: processSecurityPolicies`);
        console.log(`**************************************\n`);
        console.log(`securityPolicies = `);
        console.log(securityPolicies);
        console.log(``);
    }

    if (Object.prototype.toString.call(securityPolicies) === '[object Object]') {
        ruleName = securityPolicies['$'].name;
        toZone = securityPolicies.to.member;
        fromZone = securityPolicies.from.member;
        srcIP = securityPolicies.source.member;
        dstIP = securityPolicies.destination.member;
        user = securityPolicies['source-user'].member;
        category = securityPolicies.category.member;
        app = securityPolicies.application.member;
        service = securityPolicies.service.member;
        hip = securityPolicies['hip-profiles'].member;
        action = securityPolicies.action;

        if (typeof(securityPolicies.disabled) !== 'undefined')
            disabled = securityPolicies.disabled;

        if (typeof(securityPolicies.tag) !== 'undefined')
            if (typeof(securityPolicies.tag.member) !== 'undefined')
                tag = securityPolicies.tag.member;

        if (typeof(securityPolicies['profile-setting']) !== 'undefined') {
            profileType = Object.keys(securityPolicies['profile-setting'])[0]
            if (debug_securityPolicies) console.log(`profileType = ${profileType}`);
        }

        if (typeof(securityPolicies.schedule) !== 'undefined')
            schedule = securityPolicies.schedule;

        if (typeof(securityPolicies.qos) !== 'undefined')
            qos = securityPolicies.qos;

        if (profileType === 'profiles') {
            //console.log(Object.prototype.toString.call(securityPolicies[obj]['profile-setting'].profiles));
            if (Object.prototype.toString.call(securityPolicies['profile-setting'].profiles) === '[object String]') {
                // rather than just passing 'undefined', pass 'none' instead
                if (debug_securityPolicies) console.log(`profile is a profile and set to none`);
                profileSettings = 'none';
            } else if (Object.prototype.toString.call(securityPolicies['profile-setting'].profiles) === '[object Object]') {
                profileSettings = securityPolicies['profile-setting'].profiles;
                if (debug_securityPolicies) console.log(`profile is a profile and an object`);
                if (debug_securityPolicies) console.log(profileSettings);
            }
        } else if (profileType === 'group') {
            if (debug_securityPolicies) console.log(securityPolicies['profile-setting']);
            if (Object.prototype.toString.call(securityPolicies['profile-setting'].group) === '[object String]') {
                // rather than just passing 'undefined', pass 'none' instead
                if (debug_securityPolicies) console.log(`profile is a group and set to none`);
                profileSettings = 'none';
            } else if (Object.prototype.toString.call(securityPolicies['profile-setting'].group) === '[object Object]') {
                profileSettings = securityPolicies['profile-setting'].group;
                if (debug_securityPolicies) console.log(`profile is a group and an object`);
                if (debug_securityPolicies) console.log(profileSettings);
            }
        }

        scenario.scoringSecurityPolicies([ruleName, toZone, fromZone, srcIP, dstIP, user, category, app, service, hip, action, tag, schedule, qos, profileType, profileSettings, disabled], fwName);

    } else if (Object.prototype.toString.call(securityPolicies) === '[object Array]') {
        for (let obj in securityPolicies) {
            ruleName = securityPolicies[obj]['$'].name;
            toZone = securityPolicies[obj].to.member;
            fromZone = securityPolicies[obj].from.member;
            srcIP = securityPolicies[obj].source.member;
            dstIP = securityPolicies[obj].destination.member;
            user = securityPolicies[obj]['source-user'].member;
            category = securityPolicies[obj].category.member;
            app = securityPolicies[obj].application.member;
            service = securityPolicies[obj].service.member;
            hip = securityPolicies[obj]['hip-profiles'].member;
            action = securityPolicies[obj].action;

            if (typeof(securityPolicies.tag) !== 'undefined')
                if (typeof(securityPolicies[obj].tag.member) !== 'undefined')
                    tag = securityPolicies[obj].tag.member;

            if (typeof(securityPolicies[obj]['profile-setting']) !== 'undefined') 
                profileType = Object.keys(securityPolicies[obj]['profile-setting'])[0]
            else
                profileType = 'none'; // rather than send undefined

            if (typeof(securityPolicies[obj].disabled) !== 'undefined')
                disabled = securityPolicies[obj].disabled;

            if (typeof(securityPolicies[obj].schedule) !== 'undefined')
                schedule = securityPolicies[obj].schedule;

            if (typeof(securityPolicies[obj].qos) !== 'undefined')
                qos = securityPolicies[obj].qos;

            if (profileType === 'profiles') {
                //console.log(Object.prototype.toString.call(securityPolicies[obj]['profile-setting'].profiles));
                if (Object.prototype.toString.call(securityPolicies[obj]['profile-setting'].profiles) === '[object String]') {
                    // rather than just passing 'undefined', pass 'none' instead
                    if (debug_securityPolicies) console.log(`profile is a profile and set to none`);
                    profileSettings = 'none';
                } else if (Object.prototype.toString.call(securityPolicies[obj]['profile-setting'].profiles) === '[object Object]') {
                    profileSettings = securityPolicies[obj]['profile-setting'].profiles;
                    if (debug_securityPolicies) console.log(`profile is a profile and an object`);
                    if (debug_securityPolicies) console.log(profileSettings);
                }
            } else if (profileType === 'group') {
                if (debug_securityPolicies) console.log(securityPolicies[obj]['profile-setting']);
                if (Object.prototype.toString.call(securityPolicies[obj]['profile-setting'].group) === '[object String]') {
                    // rather than just passing 'undefined', pass 'none' instead
                    if (debug_securityPolicies) console.log(`profile is a group and set to none`);
                    profileSettings = 'none';
                } else if (Object.prototype.toString.call(securityPolicies[obj]['profile-setting'].group) === '[object Object]') {
                    profileSettings = securityPolicies[obj]['profile-setting'].group;
                    if (debug_securityPolicies) console.log(`profile is a group and an object`);
                    if (debug_securityPolicies) console.log(profileSettings);
                }
            }

            scenario.scoringSecurityPolicies([ruleName, toZone, fromZone, srcIP, dstIP, user, category, app, service, hip, action, tag, schedule, qos, profileType, profileSettings, disabled], fwName);

            tag = '';
        }
    }

}

//***********************************************************************************************
// Decryption Policies Processing
//***********************************************************************************************
function processDecryptPolicies(decryptPolicies, fwName) {

    let [ruleName, tag, toZone, fromZone, srcIP, dstIP, user, service, type, action, position, disabled] = ['', '', '', '', '', '', '', '', '', '', 0, ''];
    let category = [];

    if (debug_decryptPolicies) {
        console.log(`\n**************************************`);
        console.log(`Function: processDecryptPolicies`);
        console.log(`**************************************\n`);
        console.log(`decryptPolicies = `);
        console.log(decryptPolicies);
        console.log(``);
    }

    if (Object.prototype.toString.call(decryptPolicies) === '[object Object]') {
        ruleName = decryptPolicies['$'].name;
        toZone = decryptPolicies.to.member;
        fromZone = decryptPolicies.from.member;
        srcIP = decryptPolicies.source.member;
        dstIP = decryptPolicies.destination.member;
        user = decryptPolicies['source-user'].member;
        if (typeof(decryptPolicies.service) !== 'undefined') // for some reason the firewall won't always populate the service if not touched while creating
            service = decryptPolicies.service.member;
        else
            service = 'any';
        action = decryptPolicies.action;
        category = decryptPolicies.category.member;

        if (typeof(decryptPolicies.type) !== 'undefined')
            type = Object.keys(decryptPolicies.type);

        if (typeof(decryptPolicies.disabled) !== 'undefined')
            disabled = decryptPolicies.disabled;
        if (typeof(decryptPolicies.tag) !== 'undefined')
            if (typeof(decryptPolicies.tag.member) !== 'undefined')
                tag = decryptPolicies.tag.member;

        scenario.scoringDecryptPolicies([ruleName, toZone, fromZone, srcIP, dstIP, user, category, service, action, tag, type, position, disabled], fwName);

    } else if (Object.prototype.toString.call(decryptPolicies) === '[object Array]') {
        for (let obj in decryptPolicies) {
            ruleName = decryptPolicies[obj]['$'].name;
            toZone = decryptPolicies[obj].to.member;
            fromZone = decryptPolicies[obj].from.member;
            srcIP = decryptPolicies[obj].source.member;
            dstIP = decryptPolicies[obj].destination.member;
            user = decryptPolicies[obj]['source-user'].member;
            category = decryptPolicies[obj].category.member;
            if (typeof(decryptPolicies[obj].service) !== 'undefined') // for some reason the firewall won't always populate the service if not touched while creating
                service = decryptPolicies[obj].service.member;
            else
                service = 'any';
            action = decryptPolicies[obj].action;
            position = obj;

            if (typeof(decryptPolicies[obj].disabled) !== 'undefined')
                disabled = decryptPolicies.disabled;

            if (typeof(decryptPolicies[obj].type) !== 'undefined')
                type = Object.keys(decryptPolicies[obj].type);

            if (typeof(decryptPolicies.tag) !== 'undefined')
                if (typeof(decryptPolicies[obj].tag.member) !== 'undefined')
                    tag = decryptPolicies[obj].tag.member;

            scenario.scoringDecryptPolicies([ruleName, toZone, fromZone, srcIP, dstIP, user, category, service, action, tag, type, position, disabled], fwName);

            category = [];
            tag = '';
        }
    }

    if (debug_decryptPolicies) {
        console.log(`\n**************************************`);
        console.log(`END Function: processDecryptPolicies`);
        console.log(`**************************************\n`);
    }
}

//***********************************************************************************************
// DoS Policies Processing
//***********************************************************************************************
function processDoSPolicies(dosPolicies, fwName) {

    let [ruleName, tag, toZone, fromZone, srcIP, dstIP, user, service, action, position, classificationCriteria, classificationProfile, disabled] = ['', '', '', '', '', '', '', '', '', 0, '', '', ''];

    if (debug_dosPolicies) {
        console.log(`\n**************************************`);
        console.log(`Function: processDoSPolicies`);
        console.log(`**************************************\n`);
        console.log(`dosPolicies = `);
        console.log(dosPolicies);
        console.log(``);
    }

    if (Object.prototype.toString.call(dosPolicies) === '[object Object]') {
        ruleName = dosPolicies['$'].name;
        toZone = dosPolicies.to.zone.member;
        fromZone = dosPolicies.from.zone.member;
        srcIP = dosPolicies.source.member;
        dstIP = dosPolicies.destination.member;
        user = dosPolicies['source-user'].member;
        service = dosPolicies.service.member;

        if (typeof(dosPolicies.disabled) !== 'undefined')
            disabled = dosPolicies.disabled;

        if (typeof(dosPolicies.protection) !== 'undefined')
            action = Object.keys(dosPolicies.action).toString();

        if (typeof(dosPolicies.protection) !== 'undefined')
            if (typeof(dosPolicies.protection.classified) !== 'undefined') 
                if (typeof(dosPolicies.protection.classified['classification-criteria']) !== 'undefined') {
                    classificationCriteria = dosPolicies.protection.classified['classification-criteria'].address;
                    classificationProfile = dosPolicies.protection.classified.profile;
                }

        if (typeof(dosPolicies.tag) !== 'undefined')
            if (typeof(dosPolicies.tag.member) !== 'undefined')
                tag = dosPolicies.tag.member;

        if (debug_dosPolicies) console.log(`\nruleName = ${ruleName}, tag = ${tag}, toZone = ${toZone}, fromZone = ${fromZone}, srcIP = ${srcIP}, dstIP = ${dstIP}, user = ${user}, service = ${service}, action = ${action}, position = ${position}, classificationCriteria = ${classificationCriteria}, classificationProfile = ${classificationProfile}\n`);

        scenario.scoringDoSPolicies([ruleName, tag, toZone, fromZone, srcIP, dstIP, user, service, action, position, classificationCriteria, classificationProfile, disabled], fwName);

    } else if (Object.prototype.toString.call(dosPolicies) === '[object Array]') {
        for (let obj in dosPolicies) {
            ruleName = dosPolicies[obj]['$'].name;
            toZone = dosPolicies[obj].to.zone.member;
            fromZone = dosPolicies[obj].from.zone.member;
            srcIP = dosPolicies[obj].source.member;
            dstIP = dosPolicies[obj].destination.member;
            user = dosPolicies[obj]['source-user'].member;
            service = dosPolicies[obj].service.member;
            position = obj;

            if (typeof(dosPolicies[obj].protection) !== 'undefined')
                action = Object.keys(dosPolicies[obj].action).toString();

            if (typeof(dosPolicies[obj].disabled) !== 'undefined')
                disabled = dosPolicies[obj].disabled;

            if (typeof(dosPolicies[obj].protection) !== 'undefined')
                if (typeof(dosPolicies[obj].protection.classified) !== 'undefined') {
                    classificationProfile = dosPolicies[obj].protection.classified.profile;
                    if (typeof(dosPolicies[obj].protection.classified['classification-criteria']) !== 'undefined') 
                        classificationCriteria = dosPolicies[obj].protection.classified['classification-criteria'].address; 
                }

            if (typeof(dosPolicies[obj].tag) !== 'undefined')
                if (typeof(dosPolicies[obj].tag.member) !== 'undefined')
                    tag = dosPolicies[obj].tag.member;

            if (debug_dosPolicies) console.log(`\nruleName = ${ruleName}, tag = ${tag}, toZone = ${toZone}, fromZone = ${fromZone}, srcIP = ${srcIP}, dstIP = ${dstIP}, user = ${user}, service = ${service}, action = ${action}, position = ${position}, classificationCriteria = ${classificationCriteria}, classificationProfile = ${classificationProfile}, disabled = ${disabled}\n`);

            scenario.scoringDoSPolicies([ruleName, tag, toZone, fromZone, srcIP, dstIP, user, service, action, position, classificationCriteria, classificationProfile, disabled], fwName);

            [tag, classificationCriteria, classificationProfile, disabled] = ['', '', '', '', '', '', '', '', '', 0, '', ''];
        }
    }

    if (debug_dosPolicies) {
        console.log(`\n**************************************`);
        console.log(`END Function: processDoSPolicies`);
        console.log(`**************************************\n`);
    }

}

//***********************************************************************************************
// QoS Policies Processing
//***********************************************************************************************
function processQoSPolicies(qosPolicies, fwName) {
    // tags, type, qos, profiles, logging do not show up in XML by default unless configured

    let [ruleName, tag, toZone, fromZone, srcIP, dstIP, user, category, app, service, action, schedule, disabled] = ['', '', '', '', '', '', '', '', '', '', '', '', ''];

    if (debug_qosPolicies) {
        console.log(`\n**************************************`);
        console.log(`Function: processQoSPolicies`);
        console.log(`**************************************\n`);
        console.log(`qosPolicies = `);
        console.log(qosPolicies);
        console.log(``);
    }

    if (Object.prototype.toString.call(qosPolicies) === '[object Object]') {
        ruleName = qosPolicies['$'].name;
        toZone = qosPolicies.to.member;
        fromZone = qosPolicies.from.member;
        srcIP = qosPolicies.source.member;
        dstIP = qosPolicies.destination.member;
        user = qosPolicies['source-user'].member;
        category = qosPolicies.category.member;
        app = qosPolicies.application.member;
        service = qosPolicies.service.member;
        action = qosPolicies.action['class'];

        if (typeof(qosPolicies.tag) !== 'undefined')
            if (typeof(qosPolicies.tag.member) !== 'undefined')
                tag = qosPolicies.tag.member;

        if (typeof(qosPolicies.disabled) !== 'undefined')
            disabled = qosPolicies.disabled;

        if (typeof(qosPolicies.schedule) !== 'undefined')
            schedule = qosPolicies.schedule;

        scenario.scoringQoSPolicies([ruleName, toZone, fromZone, srcIP, dstIP, user, category, app, service, action, tag, schedule, disabled], fwName);

    } else if (Object.prototype.toString.call(qosPolicies) === '[object Array]') {
        for (let obj in qosPolicies) {
            ruleName = qosPolicies[obj]['$'].name;
            toZone = qosPolicies[obj].to.member;
            fromZone = qosPolicies[obj].from.member;
            srcIP = qosPolicies[obj].source.member;
            dstIP = qosPolicies[obj].destination.member;
            user = qosPolicies[obj]['source-user'].member;
            category = qosPolicies[obj].category.member;
            app = qosPolicies[obj].application.member;
            service = qosPolicies[obj].service.member;
            action = qosPolicies[obj].action['class'];

            if (typeof(qosPolicies.tag) !== 'undefined')
                if (typeof(qosPolicies[obj].tag.member) !== 'undefined')
                    tag = qosPolicies[obj].tag.member;

            if (typeof(qosPolicies[obj].disabled) !== 'undefined')
                disabled = qosPolicies[obj].disabled;

            if (typeof(qosPolicies[obj].schedule) !== 'undefined')
                schedule = qosPolicies[obj].schedule;

            scenario.scoringQoSPolicies([ruleName, toZone, fromZone, srcIP, dstIP, user, category, app, service, action, tag, schedule, disabled], fwName);
        }
    }
}

//***********************************************************************************************
// Objects Processing
//***********************************************************************************************
function processAddressObjects(addressObj, fwName) {

    let objType = [];
    let objName;

    if (debug_addressObjects) {
        console.log(`\n**************************************`);
        console.log(`Function: processAddressObjects`);
        console.log(`**************************************\n`);
        console.log(`addressObj = `);
        console.log(addressObj);
        console.log(``);
    }

    if (Object.prototype.toString.call(addressObj) === '[object Object]') { // IT STATEMENT NOT YET CHECKED
        objName = addressObj['$'].name;
        if (Object.keys(addressObj).includes(`ip-netmask`))
            scenario.scoringIPNetmask(addressObj[`ip-netmask`], objName);
        else if (Object.keys(addressObj).includes(`fqdn`))
            scenario.scoringIPNetmask(addressObj[`fqdn`], objName);
    }

    if (Object.prototype.toString.call(addressObj) === '[object Array]') {
        for (let obj in addressObj) {
            objName = addressObj[obj]['$'].name;
            if (Object.keys(addressObj[obj]).includes(`ip-netmask`))
                scenario.scoringIPNetmask(addressObj[obj][`ip-netmask`], objName);
            else if (Object.keys(addressObj[obj]).includes(`fqdn`))
                scenario.scoringIPNetmask(addressObj[obj][`fqdn`], objName);                     
        }
    }

    if (debug_addressObjects) {
        console.log(`\n**************************************`);
        console.log(`END Function: processAddressObjects`);
        console.log(`**************************************\n`);
    }

}
function processAppFilterObjects(appFilter, fwName) {

    let filterName;
    let subCategory = [];
    let risk = [];

    if (debug_AppFilter) {
        console.log(`\n**************************************`);
        console.log(`Function: processAppFilterObjects`);
        console.log(`**************************************\n`);
        console.log(`appFilter = `);
        console.log(appFilter);
        console.log(``);
    }

    if (Object.prototype.toString.call(appFilter) === '[object Object]') {
        if (typeof(appFilter['$'].name) !== 'undefined')
            filterName = appFilter['$'].name;
        if (typeof(appFilter.subcategory) !== 'undefined')
            if (typeof(appFilter.subcategory.member) !== 'undefined') // could be an array... *** need to process
                if (Object.prototype.toString.call(appFilter.subcategory.member) === '[object String]')
                    subCategory[0] = appFilter.subcategory.member;
                else if (Object.prototype.toString.call(appFilter.subcategory.member) === '[object Array]')
                    subCategory = appFilter.subcategory.member;
        if (typeof(appFilter.risk) !== 'undefined')
            if (typeof(appFilter.risk.member) !== 'undefined')
                if (Object.prototype.toString.call(appFilter.risk.member) === '[object String]')
                    risk[0] = appFilter.risk.member;
                else if (Object.prototype.toString.call(appFilter.risk.member) === '[object Array]')
                    risk = appFilter.risk.member;
        if (debug_AppFilter) console.log(`obj - filterName= ${filterName}\nsubcategory = ${subCategory}\nrisk = ${risk}`);
        scenario.scoringAppFilters([filterName, subCategory, risk], fwName);
    }

    if (Object.prototype.toString.call(appFilter) === '[object Array]') {
        for (let arr in appFilter) {
            if (typeof(appFilter[arr]['$'].name) !== 'undefined')
                filterName = appFilter[arr]['$'].name;
            if (typeof(appFilter[arr].subcategory) !== 'undefined')
                if (typeof(appFilter[arr].subcategory.member) !== 'undefined') // could be an array... *** need to process
                    if (Object.prototype.toString.call(appFilter[arr].subcategory.member) === '[object String]')
                        subCategory[0] = appFilter[arr].subcategory.member;
                    else if (Object.prototype.toString.call(appFilter[arr].subcategory.member) === '[object Array]')
                        subCategory = appFilter[arr].subcategory.member;
            if (typeof(appFilter[arr].risk) !== 'undefined')
                if (typeof(appFilter[arr].risk.member) !== 'undefined')
                    if (Object.prototype.toString.call(appFilter[arr].risk.member) === '[object String]')
                        risk[0] = appFilter[arr].risk.member;
                    else if (Object.prototype.toString.call(appFilter[arr].risk.member) === '[object Array]')
                        risk = appFilter[arr].risk.member;
            if (debug_AppFilter) console.log(`arr - filterName= ${filterName}\nsubcategory = ${subCategory}\nrisk = ${risk}`);
            scenario.scoringAppFilters([filterName, subCategory, risk], fwName);
            [risk, filterName, subCategory] = ['', '', ''];
        }
    }

    if (debug_AppFilter) {
        console.log(`\n**************************************`);
        console.log(`END Function: processAppFilterObjects`);
        console.log(`**************************************\n`);
    }
}

//***********************************************************************************************
// User-ID Config
//***********************************************************************************************
function processUserID(userIDConfig, fwName) {
    let ignoreUser;

    if (debug_userID) {
        console.log(`\n**************************************`);
        console.log(`Function: processUserID`);
        console.log(`**************************************\n`);
        console.log(`userIDConfig = `);
        console.log(userIDConfig);
        console.log(``);
    }

    if (typeof(userIDConfig['ignore-user']) !== 'undefined') 
        if (typeof(userIDConfig['ignore-user'].member) !== 'undefined')
            ignoreUser = userIDConfig['ignore-user'].member;  

    scenario.scoringUserID(ignoreUser, fwName);

    if (debug_userID) {
        console.log(`\n**************************************`);
        console.log(`END Function: processUserID`);
        console.log(`**************************************\n`);
    }
}

//***********************************************************************************************
// Log Forwarding Config
//***********************************************************************************************
function processLogFwd(logFwd, fwName) {

    // too repetative code below. Should be fixed in the future.

    if (debug_logForward) {
        console.log(`\n**************************************`);
        console.log(`Function: processLogFwd`);
        console.log(`**************************************\n`);
        console.log(`logFwd = `);
        console.log(logFwd);
        console.log(``);
    }

    let profileName = '';
    let [trafficName, trafficFilter, trafficPanorama] = [[], [], []];
    let [urlName, urlFilter, urlPanorama] = [[], [], []];
    let [threatName, threatFilter, threatPanorama, threatSyslog] = [[], [], [], []];
    let [dataName, dataFilter, dataPanorama] = [[], [], []];
    let [wildfireName, wildfireFilter, wildfirePanorama] = [[], [], []];

    if (Object.prototype.toString.call(logFwd) === '[object Object]') {
        if (typeof(logFwd['$'].name) !== 'undefined')
            name = logFwd['$'].name;
        if (typeof(logFwd['match-list']) !== 'undefined') {
            if (typeof(logFwd['match-list'].entry) !== 'undefined')
                if (Object.prototype.toString.call(logFwd['match-list'].entry) === '[object Array]') {
                    for (let arr in logFwd['match-list'].entry) {
                        if (debug_logForward) console.log(logFwd['match-list'].entry[arr]);
                        if (logFwd['match-list'].entry[arr]['log-type'] === 'traffic') {
                            trafficName.push(logFwd['match-list'].entry[arr]['$'].name);
                            trafficFilter.push(logFwd['match-list'].entry[arr].filter);
                            trafficPanorama.push((logFwd['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                        }
                        if (logFwd['match-list'].entry[arr]['log-type'] === 'threat') {
                            threatName.push(logFwd['match-list'].entry[arr]['$'].name);
                            threatFilter.push(logFwd['match-list'].entry[arr].filter);
                            threatPanorama.push((logFwd['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                            if (typeof(logFwd['match-list'].entry[arr]['send-syslog']) !== 'undefined') {
                                if (typeof(logFwd['match-list'].entry[arr]['send-syslog'].member) !== 'undefined')
                                    threatSyslog.push(logFwd['match-list'].entry[arr]['send-syslog'].member);
                            } else {
                                threatSyslog.push('');
                            }
                        }
                        if (logFwd['match-list'].entry[arr]['log-type'] === 'data') {
                            dataName.push(logFwd['match-list'].entry[arr]['$'].name);
                            dataFilter.push(logFwd['match-list'].entry[arr].filter);
                            dataPanorama.push((logFwd['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                        }
                        if (logFwd['match-list'].entry[arr]['log-type'] === 'url') {
                            urlName.push(logFwd['match-list'].entry[arr]['$'].name);
                            urlFilter.push(logFwd['match-list'].entry[arr].filter);
                            urlPanorama.push((logFwd['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                        }
                        if (logFwd['match-list'].entry[arr]['log-type'] === 'wildfire') {
                            wildfireName.push(logFwd['match-list'].entry[arr]['$'].name);
                            wildfireFilter.push(logFwd['match-list'].entry[arr].filter);
                            wildfirePanorama.push((logFwd['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                        }
                    }
                } else if (Object.prototype.toString.call(logFwd['match-list'].entry) === '[object Object]') {
                    if (debug_logForward) console.log(logFwd['match-list'].entry);
                    if (logFwd['match-list'].entry['log-type'] === 'traffic') {
                        trafficName.push(logFwd['match-list'].entry['$'].name);
                        trafficFilter.push(logFwd['match-list'].entry.filter);
                        trafficPanorama.push((logFwd['match-list'].entry['send-to-panorama'] == "yes"));
                    }
                    if (logFwd['match-list'].entry['log-type'] === 'threat') {
                        threatName.push(logFwd['match-list'].entry['$'].name);
                        threatFilter.push(logFwd['match-list'].entry.filter);
                        threatPanorama.push((logFwd['match-list'].entry['send-to-panorama'] == "yes"));
                        if (typeof(logFwd['match-list'].entry['send-syslog']) !== 'undefined') {
                            if (typeof(logFwd['match-list'].entry['send-syslog'].member) !== 'undefined')
                                threatSyslog.push(logFwd['match-list'].entry['send-syslog'].member);
                        } else {
                            threatSyslog.push('');
                        }
                    }
                    if (logFwd['match-list'].entry['log-type'] === 'data') {
                        dataName.push(logFwd['match-list'].entry['$'].name);
                        dataFilter.push(logFwd['match-list'].entry.filter);
                        dataPanorama.push((logFwd['match-list'].entry['send-to-panorama'] == "yes"));
                    }
                    if (logFwd['match-list'].entry['log-type'] === 'url') {
                        urlName.push(logFwd['match-list'].entry['$'].name);
                        urlFilter.push(logFwd['match-list'].entry.filter);
                        urlPanorama.push((logFwd['match-list'].entry['send-to-panorama'] == "yes"));
                    }
                    if (logFwd['match-list'].entry['log-type'] === 'wildfire') {
                        wildfireName.push(logFwd['match-list'].entry['$'].name);
                        wildfireFilter.push(logFwd['match-list'].entry.filter);
                        wildfirePanorama.push((logFwd['match-list'].entry['send-to-panorama'] == "yes"));
                    }

                }

            scenario.scoringLogForwarding(trafficName, trafficFilter, trafficPanorama, threatName, threatFilter, threatPanorama, threatSyslog, dataName, dataFilter, dataPanorama, urlName, urlFilter, urlPanorama, wildfireName, wildfireFilter, wildfirePanorama, name, fwName);

            // clear arrays
            [trafficName, trafficFilter, trafficPanorama] = [[], [], []];
            [threatName, threatFilter, threatPanorama, threatSyslog] = [[], [], [], []];
            [dataName, dataFilter, dataPanorama] = [[], [], []];
            [urlName, urlFilter, urlPanorama] = [[], [], []];
            [wildfireName, wildfireFilter, wildfirePanorama] = [[], [], []];

        }
    }

    if (Object.prototype.toString.call(logFwd) === '[object Array]') {
        for (let array in logFwd) {
            if (typeof(logFwd[array]['$'].name) !== 'undefined')
                name = logFwd[array]['$'].name;
            if (debug_logForward) console.log(`name = ${name}`);
            if (typeof(logFwd[array]['match-list']) !== 'undefined' && Object.prototype.toString.call(logFwd[array]['match-list']) !== '[object String]') {
                if (typeof(logFwd[array]['match-list'].entry) !== 'undefined')
                    if (Object.prototype.toString.call(logFwd[array]['match-list'].entry) === '[object Array]') {
                        for (let arr in logFwd[array]['match-list'].entry) {
                            if (debug_logForward) console.log(logFwd[array]['match-list'].entry[arr]);
                            if (logFwd[array]['match-list'].entry[arr]['log-type'] === 'traffic') {
                                trafficName.push(logFwd[array]['match-list'].entry[arr]['$'].name);
                                trafficFilter.push(logFwd[array]['match-list'].entry[arr].filter);
                                trafficPanorama.push((logFwd[array]['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                            }
                            if (logFwd[array]['match-list'].entry[arr]['log-type'] === 'threat') {
                                threatName.push(logFwd[array]['match-list'].entry[arr]['$'].name);
                                threatFilter.push(logFwd[array]['match-list'].entry[arr].filter);
                                threatPanorama.push((logFwd[array]['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                                if (typeof(logFwd[array]['match-list'].entry[arr]['send-syslog']) !== 'undefined') {
                                    if (typeof(logFwd[array]['match-list'].entry[arr]['send-syslog'].member) !== 'undefined')
                                        threatSyslog.push(logFwd[array]['match-list'].entry[arr]['send-syslog'].member);
                                } else {
                                    threatSyslog.push('');
                                }
                            }
                            if (logFwd[array]['match-list'].entry[arr]['log-type'] === 'data') {
                                dataName.push(logFwd[array]['match-list'].entry[arr]['$'].name);
                                dataFilter.push(logFwd[array]['match-list'].entry[arr].filter);
                                dataPanorama.push((logFwd[array]['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                            }
                            if (logFwd[array]['match-list'].entry[arr]['log-type'] === 'url') {
                                urlName.push(logFwd[array]['match-list'].entry[arr]['$'].name);
                                urlFilter.push(logFwd[array]['match-list'].entry[arr].filter);
                                urlPanorama.push((logFwd[array]['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                            }
                            if (logFwd[array]['match-list'].entry[arr]['log-type'] === 'wildfire') {
                                wildfireName.push(logFwd[array]['match-list'].entry[arr]['$'].name);
                                wildfireFilter.push(logFwd[array]['match-list'].entry[arr].filter);
                                wildfirePanorama.push((logFwd[array]['match-list'].entry[arr]['send-to-panorama'] == "yes"));
                            }
                        }
                    } else if (Object.prototype.toString.call(logFwd[array]['match-list'].entry) === '[object Object]') {
                        if (debug_logForward) console.log(logFwd[array]['match-list'].entry);
                        if (logFwd[array]['match-list'].entry['log-type'] === 'traffic') {
                            trafficName.push(logFwd[array]['match-list'].entry['$'].name);
                            trafficFilter.push(logFwd[array]['match-list'].entry.filter);
                            trafficPanorama.push((logFwd[array]['match-list'].entry['send-to-panorama'] == "yes"));
                        }
                        if (logFwd[array]['match-list'].entry['log-type'] === 'threat') {
                            threatName.push(logFwd[array]['match-list'].entry['$'].name);
                            threatFilter.push(logFwd[array]['match-list'].entry.filter);
                            threatPanorama.push((logFwd[array]['match-list'].entry['send-to-panorama'] == "yes"));
                            if (typeof(logFwd[array]['match-list'].entry[arr]['send-syslog']) !== 'undefined') {
                                if (typeof(logFwd[array]['match-list'].entry[arr]['send-syslog'].member) !== 'undefined')
                                    threatSyslog.push(logFwd[array]['match-list'].entry['send-syslog'].member);
                            } else {
                                threatSyslog.push('');
                            }
                        }
                        if (logFwd[array]['match-list'].entry['log-type'] === 'data') {
                            dataName.push(logFwd[array]['match-list'].entry['$'].name);
                            dataFilter.push(logFwd[array]['match-list'].entry.filter);
                            dataPanorama.push((logFwd[array]['match-list'].entry['send-to-panorama'] == "yes"));
                        }
                        if (logFwd[array]['match-list'].entry['log-type'] === 'url') {
                            urlName.push(logFwd[array]['match-list'].entry['$'].name);
                            urlFilter.push(logFwd[array]['match-list'].entry.filter);
                            urlPanorama.push((logFwd[array]['match-list'].entry['send-to-panorama'] == "yes"));
                        }
                        if (logFwd[array]['match-list'].entry['log-type'] === 'wildfire') {
                            wildfireName.push(logFwd[array]['match-list'].entry['$'].name);
                            wildfireFilter.push(logFwd[array]['match-list'].entry.filter);
                            wildfirePanorama.push((logFwd[array]['match-list'].entry['send-to-panorama'] == "yes"));
                        }
                    }

                scenario.scoringLogForwarding(trafficName, trafficFilter, trafficPanorama, threatName, threatFilter, threatPanorama, threatSyslog, dataName, dataFilter, dataPanorama, urlName, urlFilter, urlPanorama, wildfireName, wildfireFilter, wildfirePanorama, name, fwName);

                // clear arrays
                [trafficName, trafficFilter, trafficPanorama] = [[], [], []];
                [threatName, threatFilter, threatPanorama, threatSyslog] = [[], [], [], []];
                [dataName, dataFilter, dataPanorama] = [[], [], []];
                [urlName, urlFilter, urlPanorama] = [[], [], []];
                [wildfireName, wildfireFilter, wildfirePanorama] = [[], [], []];

            } else if (Object.prototype.toString.call(logFwd[array]['match-list']) === '[object String]') {
                console.log(`No profiles configured in logging profile.`);
            }
        }
    }

    if (debug_logForward) {
        console.log(`\n**************************************`);
        console.log(`END Function: processLogFwd`);
        console.log(`**************************************\n`);
    }

}

//***********************************************************************************************
// Virtual Routers
//***********************************************************************************************
function processVirtualRouters(vrConfig, fwName) {

    let interfaces = [];
    let resultArr = [];

    if (debug_vr) {
        console.log(`\n**************************************`);
        console.log(`Function: processVirtualRouters`);
        console.log(`**************************************\n`);
        console.log(`vrConfig = `);
        console.log(vrConfig);
        console.log(``);
    }

    if (Object.prototype.toString.call(vrConfig) === '[object Array]') {
        for (i in vrConfig) {
            if (typeof(vrConfig[i]) !== 'undefined')
                if (typeof(vrConfig[i].interface) !== 'undefined') 
                    if (Object.prototype.toString.call(vrConfig[i].interface.member) === '[object String]')
                        resultArr.push( { vr: [vrConfig[i]['$'].name], interfaces: [vrConfig[i].interface.member] } );
                    else
                        resultArr.push( { vr: [vrConfig[i]['$'].name], interfaces: vrConfig[i].interface.member } ); //vrConfig[i].interface.member is an array already
        }
    } else {
        if (typeof(vrConfig) !== 'undefined') 
            if (typeof(vrConfig.interface) !== 'undefined') 
                if (Object.prototype.toString.call(vrConfig.interface.member) === '[object String]') 
                    resultArr = [ { vr: [vrConfig['$'].name], interfaces: [vrConfig.interface.member] } ];
                else
                    resultArr = [ { vr: [vrConfig['$'].name], interfaces: vrConfig.interface.member } ];
    }


    if (debug_vr) {
        console.log(`resultArr = `);
        console.log(resultArr);
        console.log(`\n**************************************`);
        console.log(`END Function: processVirtualRouters`);
        console.log(`**************************************\n`);

    }

    scenario.scoringVirtualRouters(resultArr, fwName);

    interfaces = [];
    resultArr = [];
}

//***********************************************************************************************
// Physical Interface
//***********************************************************************************************
function processPhysicalInterface(physIntConfig, fwName) {

    let interfaces = [];
    let resultArr = [];

    if (debug_physInt) {
        console.log(`\n**************************************`);
        console.log(`Function: processPhysicalInterface`);
        console.log(`**************************************\n`);
        console.log(`physIntConfig = `);
        console.log(physIntConfig);
        console.log(``);
    }

    if (Object.prototype.toString.call(physIntConfig) === '[object Object]') {
        if (typeof(physIntConfig.layer3) !== 'undefined') {
            if (typeof(physIntConfig.layer3.ip) !== 'undefined') {
                resultArr.push( { interfaceName: physIntConfig['$'].name, ip: physIntConfig.layer3.ip.entry['$'].name } );
            } else if (Object.keys(physIntConfig.layer3).includes(`dhcp-client`)) {
                resultArr.push( { interfaceName: physIntConfig['$'].name, ip: 'dhcp' } );
            }
        }
    } else {
        for (i in physIntConfig) {
            if (typeof(physIntConfig[i].layer3) !== 'undefined') {
                if (typeof(physIntConfig[i].layer3.ip) !== 'undefined') {
                    resultArr.push( { interfaceName: physIntConfig[i]['$'].name, ip: physIntConfig[i].layer3.ip.entry['$'].name } );
                } else if (Object.keys(physIntConfig[i].layer3).includes(`dhcp-client`)) {
                    resultArr.push( { interfaceName: physIntConfig[i]['$'].name, ip: 'dhcp' } );
                }
            }
        }
    }

    if (debug_physInt) {
        console.log(`resultArr going for scoring = `);
        console.log(resultArr);
        console.log(`\n**************************************`);
        console.log(`END Function: processPhysicalInterface`);
        console.log(`**************************************\n`);

    }

    scenario.scoringPhysicalInterfaces(resultArr, fwName);

    interfaces = [];
    resultArr = [];
}

//***********************************************************************************************
// Schedule Processing
//***********************************************************************************************
function processScheduleConfig(scheduleObj, fwName) {

    let schedRecur = false; // can either be recurring or non-recurring
    let recurDaily = false; // can either be daily or weekly
    let recur; // holds the actual recurrance value regardless if non-recurring or recurring value
    let schedName;

    if (debug_scheduleObject) {
        console.log(`\n**************************************`);
        console.log(`Function: processScheduleConfig`);
        console.log(`**************************************\n`);
        console.log(`scheduleObj = `);
        console.log(scheduleObj);
        console.log(``);
    }

    if (Object.prototype.toString.call(scheduleObj) === '[object Object]') { 
        //console.log(`it is an object`);
        //console.log(scheduleObj);
        schedName = scheduleObj['$'].name;
        //console.log(`name = ${schedName}`);
        if (typeof(scheduleObj['schedule-type']) !== 'undefined')
            if (Object.keys(scheduleObj['schedule-type']).includes(`recurring`)) {
                schedRecur = true;
                if (Object.keys(scheduleObj['schedule-type'].recurring).includes(`daily`)) {
                    recurDaily = true;
                    recur = scheduleObj['schedule-type'].recurring.daily.member;
                } else 
                    recurDaily = false;
            } else {
                //console.log(`non-recurring obj`);
                //console.log(scheduleObj['schedule-type']['non-recurring']);
                schedRecur = false;
                recur = scheduleObj['schedule-type']['non-recurring'].member;
            }
        scenario.scoringSchedule(schedRecur, recurDaily, recur, schedName);
    }

    if (Object.prototype.toString.call(scheduleObj) === '[object Array]') {
        //console.log(`it is an array`);
        for (let obj in scheduleObj) {
            schedName = scheduleObj[obj]['$'].name;
            //console.log(`name = ${schedName}`);
            if (typeof(scheduleObj[obj]['schedule-type']) !== 'undefined')
                if (Object.keys(scheduleObj[obj]['schedule-type']).includes(`recurring`)) {
                    schedRecur = true;
                    if (Object.keys(scheduleObj[obj]['schedule-type'].recurring).includes(`daily`)) {
                        recurDaily = true;
                        recur = scheduleObj[obj]['schedule-type'].recurring.daily.member;
                    } else 
                        recurDaily = false;
                } else {
                    //console.log(`non-recurring arr`);
                    //console.log(scheduleObj[obj]['schedule-type']['non-recurring']);
                    schedRecur = false;
                    recur = scheduleObj[obj]['schedule-type']['non-recurring'].member;
                }
            scenario.scoringSchedule(schedRecur, recurDaily, recur, schedName);
        }
    }

    if (debug_scheduleObject) {
        console.log(`\n**************************************`);
        console.log(`END Function: processScheduleConfig`);
        console.log(`**************************************\n`);
    }
}

//***********************************************************************************************
// Process HTTP log forward from firewall and make API call to get full config
//***********************************************************************************************
const server = http.createServer((req, res) => {
    let output = '';
    let message = "";
    const pathName = url.parse(req.url, true).pathname; 
    const query = url.parse(req.url, true).query;  // comment out
    const id = url.parse(req.url, true).query.id; 
    function getConfig(fwName) { // make https request to FW to download file blocking config
        try {
            output = '';
            playerNum = fwName.match(/[0-9]{1,}$/);
            playerNum = Number(playerNum);
            let apikey = eval("player" + playerNum + ".apikey");
            var optionsgetmsg = {            
                host: fwName, // here only the domain name
                port: 443,
                path: `/api/?type=op&cmd=%3Cshow%3E%3Cconfig%3E%3Crunning%3E%3C%2Frunning%3E%3C%2Fconfig%3E%3C%2Fshow%3E&key=${apikey}`,
                method : 'GET' // do GET
            };

            var reqGet = https.request(optionsgetmsg, function(resp) { // make API call      
                resp.on('data', function(d) {
                    output += d.toString(); //take all the output 
                }); // receive data in chunks
                resp.on('end', function() { // process output

                    parseString(output, { explicitArray : false }, function(err,result){ // parse XML config from firewall

                        if (result.response['$'].status === 'success') { // make sure API call was successful

                            scenario.init(); // reset some variables so they get reprocessed again

                            if (debug_securityProfileGroup) console.log(result.response.result.config.devices.entry.vsys.entry['profile-group']);

                            //***********************************************************************************************
                            //Virtual Router
                            //***********************************************************************************************
                            let virtualRouterConfig = result.response.result.config.devices.entry.network['virtual-router'];
                            if (typeof(virtualRouterConfig) !== 'undefined')
                                processVirtualRouters(virtualRouterConfig.entry, fwName);

                            //***********************************************************************************************
                            //Physical Interfaces
                            //***********************************************************************************************
                            let physicalInterfaceConfig = result.response.result.config.devices.entry.network.interface.ethernet;
                            if (typeof(physicalInterfaceConfig) !== 'undefined')
                                processPhysicalInterface(physicalInterfaceConfig.entry, fwName);

                            //***********************************************************************************************
                            //Network Profile Config Section
                            //***********************************************************************************************
                            let networkProfileConfig = result.response.result.config.devices.entry.network.profiles['zone-protection-profile'];
                            if (typeof(networkProfileConfig) !== 'undefined')
                                processNetworkProfileConfig(networkProfileConfig.entry, fwName);

                            //***********************************************************************************************
                            //Zone Config Section
                            //***********************************************************************************************
                            let zoneConfig = result.response.result.config.devices.entry.vsys.entry.zone;
                            if (typeof(zoneConfig) !== 'undefined')
                                processZoneConfig(zoneConfig.entry, fwName);

                            //***********************************************************************************************
                            //QoS Profile Config Section
                            //***********************************************************************************************
                            let qosProfileConfig = result.response.result.config.devices.entry.network.qos.profile;
                            if (typeof(qosProfileConfig) !== 'undefined')
                                processQoSProfileConfig(qosProfileConfig.entry, fwName);

                            let qosInterfaceConfig = result.response.result.config.devices.entry.network.qos.interface;
                            if (typeof(qosInterfaceConfig) !== 'undefined')
                                processQoSInterfaceConfig(qosInterfaceConfig.entry, fwName); 

                            //***********************************************************************************************
                            //Schedule Config Section
                            //***********************************************************************************************
                            let scheduleConfig = result.response.result.config.devices.entry.vsys.entry.schedule;
                            if (typeof(scheduleConfig) !== 'undefined')
                                processScheduleConfig(scheduleConfig.entry, fwName);

                            //***********************************************************************************************
                            //Entry Settings Section (forward decrypted content)
                            //***********************************************************************************************
                            let forwardDecryptConfig = result.response.result.config.devices.entry.vsys.entry.setting;
                            if (typeof(forwardDecryptConfig) !== 'undefined')
                                processEntrySettingsConfig(forwardDecryptConfig, fwName);

                            //***********************************************************************************************
                            //Shared Config Section
                            //***********************************************************************************************
                            let sharedConfig = result.response.result.config.shared;
                            if (typeof(sharedConfig) !== 'undefined')
                                processSharedConfig(sharedConfig, fwName);

                            //***********************************************************************************************
                            //Device Config Section
                            //***********************************************************************************************
                            let deviceConfig = result.response.result.config.devices.entry.deviceconfig;
                            if (typeof(deviceConfig) !== 'undefined') // check if it exists
                                processDeviceConfig(deviceConfig, fwName);
                            //  console.log(deviceConfig);

                            //***********************************************************************************************
                            //Management Config Section
                            //***********************************************************************************************
                            let managementConfig = result.response.result.config['mgt-config'];
                            if (typeof(managementConfig) !== 'undefined') // check if it exists
                                processManagementConfig(managementConfig, fwName);

                            //***********************************************************************************************
                            //Custom Vuln Object
                            //***********************************************************************************************
                            let customVulnConfig = result.response.result.config.devices.entry.vsys.entry.threats;
                            if (typeof(customVulnConfig) !== 'undefined') { // check if it exists 
                                processVulnCustomProfile(customVulnConfig, fwName);
                            }

                            //***********************************************************************************************
                            //Security Profiles Section
                            //***********************************************************************************************
                            // XML Path: Config > Device > Security Profiles
                            //console.log(result.response.result.config.devices.entry.vsys.entry.profiles);
                            let profileConfig = result.response.result.config.devices.entry.vsys.entry.profiles;  

                            if (typeof(profileConfig) !== 'undefined') {

                                // antivirus profile
                                if (typeof(profileConfig['virus']) !== 'undefined') // check if it exists
                                    processSecurityProfileConfig(profileConfig['virus'], `antivirus`, fwName);

                                // spyware profile
                                if (typeof(profileConfig['spyware']) !== 'undefined') 
                                    processSecurityProfileConfig(profileConfig['spyware'], `spyware`, fwName);

                                // vulnerability profile
                                if (typeof(profileConfig['vulnerability']) !== 'undefined')
                                    processSecurityProfileConfig(profileConfig['vulnerability'], `vulnerability`, fwName);

                                // file-blocking profile
                                if (typeof(profileConfig['file-blocking']) !== 'undefined')
                                    processSecurityProfileConfig(profileConfig['file-blocking'], `fileblocking`, fwName);

                                // dos profile
                                if (typeof(profileConfig['dos-protection']) !== 'undefined')
                                    processSecurityProfileConfig(profileConfig['dos-protection'], `dosProtection`, fwName);

                                // Custom URL profile
                                if (typeof(profileConfig['custom-url-category']) !== 'undefined')
                                    processSecurityProfileConfig(profileConfig['custom-url-category'], `customUrl`, fwName);

                                // Custom Data Pattern
                                if (typeof(profileConfig['data-objects']) !== 'undefined') 
                                    processSecurityProfileConfig(profileConfig['data-objects'], `customData`, fwName);

                                // URL profile
                                if (typeof(profileConfig['url-filtering']) !== 'undefined') 
                                    processSecurityProfileConfig(profileConfig['url-filtering'], `url`, fwName);

                                // data-filtering profile
                                if (typeof(profileConfig['data-filtering']) !== 'undefined')
                                    processSecurityProfileConfig(profileConfig['data-filtering'], `datafiltering`, fwName);
                            }

                            // XML Path: Config > Device > Security Profile Groups
                            // Security Profile Groups
                            let securityProfileGroupConfig = result.response.result.config.devices.entry.vsys.entry['profile-group'];

                            if (typeof(securityProfileGroupConfig) !== 'undefined')
                                processSecurityProfileGroupConfig(securityProfileGroupConfig, fwName);

                            //***********************************************************************************************
                            //Objects Section
                            //***********************************************************************************************
                            let objectsConfig = result.response.result.config.devices.entry.vsys.entry.address;
                            if (typeof(objectsConfig) !== 'undefined')
                                processAddressObjects(objectsConfig.entry, fwName);

                            let appFilterConfig = result.response.result.config.devices.entry.vsys.entry['application-filter'];
                            if (typeof(appFilterConfig) !== 'undefined')
                                processAppFilterObjects(appFilterConfig.entry, fwName);

                            let logFwdConfig = result.response.result.config.devices.entry.vsys.entry['log-settings'];
                            if (typeof(logFwdConfig) !== 'undefined')
                                processLogFwd(logFwdConfig.profiles.entry, fwName);

                            //***********************************************************************************************
                            //User-ID
                            //***********************************************************************************************
                            let userIDConfig = result.response.result.config.devices.entry.vsys.entry['user-id-collector'];
                            if (typeof(userIDConfig) !== 'undefined')
                                processUserID(userIDConfig, fwName);

                            //***********************************************************************************************
                            //Policies Config Section
                            //***********************************************************************************************
                            // need to check the below if it is in the default config
                            let policiesConfig = result.response.result.config.devices.entry.vsys.entry.rulebase;
                            if (typeof(policiesConfig.security) !== 'undefined')
                                if (typeof(policiesConfig.security.rules) !== 'undefined')
                                    if (typeof(policiesConfig.security.rules.entry) !== 'undefined')
                                        processSecurityPolicies(policiesConfig.security.rules.entry, fwName);
                            if (typeof(policiesConfig.qos) !== 'undefined')
                                processQoSPolicies(policiesConfig.qos.rules.entry, fwName);
                            if (typeof(policiesConfig.decryption) !== 'undefined')
                                processDecryptPolicies(policiesConfig.decryption.rules.entry, fwName);
                            if (typeof(policiesConfig.dos) !== 'undefined')
                                processDoSPolicies(policiesConfig.dos.rules.entry, fwName);

                            //***********************************************************************************************
                            //Completion Logic Checks
                            //***********************************************************************************************
                            let completionResult = scenario.completionLogic(fwName);
                            //messages += completionResult[0]; 
                            //console.log(Object.prototype.toString.call(completionResult));
                            //console.log(`completionResult[1] = `);
                            //console.log(completionResult[1]);
                            if (typeof(completionResult) !== 'undefined') // make sure there is a value here
                                updateScoreBoard(completionResult, fwName);

                            //***********************************************************************************************

                        } else {
                            console.log(`API called failed: ${fwName}`);
                        }
                    });
                    if (httpGetDebug) console.log(output);
                    //console.log('End of data was triggered');
                });
            });       
            reqGet.end();
            reqGet.on('error', function(e) {
                console.error(`Error: ${e}`);
            }); 
        }
        catch(error) {
            console.log(error);
        }
    }

    if (pathName === '/system') { // receive HTTPS log forward messages from FW
        req.on('data', function (chunk) { // data is received in chunks from FW and assembled in 'message'
            message += chunk;
        });
        req.on('end', function () { // once complete, go to 'end' stage
            //console.log('POSTed: ' + message); // print raw log
            res.writeHead(200, { 'Content-type': 'text/html' });
            //console.log(`****************************************************************************************`);
            var splitText = message.split(","); // convert CSV into array to get FW hostname field
            if (gameStart) {
                if (typeof(splitText[22]) !== 'undefined') {
                    let hostname = splitText[22]; // this is the position of the hostname of the firewall in CSV
                    console.log(`Commit completed by: ${hostname}`);
                    if(typeof(player0) !== 'undefined') // make sure players have been initizlied through admin page first
                        getConfig(hostname); // parse file blocking
                    else {
                        console.log(`Commit submitted, but no players have been initialized yet!`);
                    }
                } else {    
                    console.log(`hostname parsing failed`);
                }
            }
        });
    } else {
        res.writeHead(404, { 'Content-type': 'text/html' });
        res.end('<http><h1>wat! 404</h1>lolz<img src="http://i.imgur.com/iMtqa1d.jpeg" width="300"></http>');
    } 
});

//server.listen(1337, '192.168.1.133', () => {
server.listen(1337, '192.168.1.171', () => {
    //server.listen(1377, '172.16.250.20', () => {
    console.log('Listening for requests now');
});